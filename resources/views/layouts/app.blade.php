<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php  $conf = CityTips\Conf::all() ?>
    @include('layouts.scripts-and-styles.meta-tags')
    @yield('meta')
    <title>CityTips</title>
    @include('layouts.scripts-and-styles.header-scripts-and-styles')
</head>
<body id="app-layout">
<h1 class="too-small-screen"><i class="fa fa-expand">Use uma tela maior para visualizar o CityTips! ;)</i></h1>
<div id="fb-root"></div>

@include('layouts.nav.nav-bar')
@yield('content')
@include('layouts.scripts-and-styles.footer-scripts-and-styles')
</body>
</html>
