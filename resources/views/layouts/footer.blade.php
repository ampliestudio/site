<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h4>Adicionar</h4>
                <ul class="site-map">
                    <li>
                        <a href="{!! url('tips/suporte/0/6/0') !!}">Cidade<em class="hidden-xs hidden-sm"> .</em></a>
                    </li>
                    <li>
                        <a href="{!! url('user/add-local') !!}">Estabelecimento<em class="hidden-xs hidden-sm"> .</em></a>
                    </li>
                    <li>
                        <a href="{!! url('user/criar-evento') !!}">Evento</a>
                    </li>

                </ul>
            </div>
            {{--<hr class="visible-xs">--}}
            <div class="col-sm-4">
                <h4>Informações</h4>
                <ul class="site-map">
                    <li>
                        <a href="{!! url('tips/sobre') !!}">Sobre FAQ<em class="hidden-xs hidden-sm"> .</em></a>
                    </li>
                    <li>
                        <a href="{!! url('tips/termos') !!}">Termos<em class="hidden-xs hidden-sm"> .</em></a>
                    </li>
                    <li>
                        <a href="{!! url('tips/legal') !!}">Licenças<em class="hidden-xs hidden-sm"> .</em></a>
                    </li>
                    {{--<li>--}}
                    {{--<a href="mailto:contato@citytips.com.br">Contato</a>--}}
                    {{--</li>--}}
                    <li>
                        <a target="_blank" href="https://m.me/1398707946825318">Ajuda<em class="hidden-xs hidden-sm"> .</em></a>
                    </li>
                    <li>
                        <a href="{!! url('tips/suporte/0/4/0') !!}">Reportar violação</a>
                    </li>
                </ul>
            </div>
            {{--<hr class="visible-xs">--}}
            <div class="col-sm-4">
                <h4>Seguir</h4>
                <ul class="site-map">
                    <li>
                        <a href="https://www.facebook.com/citytipsbr/" target="_blank"><i class="fa fa-2x fa-facebook-square"></i></a>&nbsp;
                        <a href="https://www.instagram.com/citytipsbr/" target="_blank"><i class="fa fa-2x fa-instagram"></i></a>&nbsp;
                        <a href="https://twitter.com/citytipsbr" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a>&nbsp;
                    </li>
                </ul>

            </div>
            <div class="col-sm-12 sobre">
                <p class="text-center">
                    <small class="sobre">Utilizamos cookies <a href="{!! url('tips/cookies') !!}">veja mais.</a></small>
                </p>
                <p class="text-center footer-text">&copy; 2016 <a href="http://zafalon.com"
                                                                 target="_blank">Zafalon.com</a>
                     </p>
            </div>
        </div>
    </div>
</footer>