<li class="hidden-xs"><a href="{{ url('user/home') }}"> {{ Auth::user()->name }}</a></li>
<li class="visible-lg visible-md visible-sm"><a href="{!! url('user/home')!!}">
        <img style="margin-bottom: -10px !important; margin-top: -10px !important;"
             class="img-circle" src="{!! Auth::user()->foto !!}" alt="" width="40px">
    </a>
</li>


<li class="dropdown visible-lg visible-md visible-sm">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        <span class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
        {{--<li><a href="{!! url('user/add-local') !!}"><i class="fa fa-glass"></i>&nbsp; Adicionar local</a></li>--}}
        <li><a href="{{ url('admin/home') }}"><i class="fa fa-btn fa-wrench"></i>&nbsp; Painel de controle</a></li>
        <li><a target="_blank" href="https://m.me/1398707946825318"><i class="fa fa-question"></i>&nbsp; Ajuda</a></li>
        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp; Sair</a></li>

    </ul>
</li>


{{--menu xs--}}
<li class="visible-xs"><a href="{{ url('user/home') }}"><i class="fa fa-btn fa-user"></i>&nbsp; {{ Auth::user()->name }} </a></li>
{{--<li class="visible-xs"><a href="{!! url('user/add-local') !!}"><i class="fa fa-glass"></i>&nbsp; Adicionar local</a></li>--}}
</li>
<li class="visible-xs"><a href="{{ url('admin/home') }}"><i class="fa fa-btn fa-wrench"></i>&nbsp; Painel de controle</a></li>
<li class="visible-xs"><a target="_blank" href="https://m.me/1398707946825318"><i class="fa fa-question"></i>&nbsp; Ajuda</a></li>
<li class="visible-xs"><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>&nbsp; Sair</a></li>


