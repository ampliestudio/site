<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="img-logo" src="{!! asset('images/logo.png') !!}" alt="logo citytips">
            </a>
            @if(isset($cidade))
                <a type="button" class="clickable navbar-brand" data-toggle="modal" data-target="#myModal"><?echo($cidade->nome); ?></a>
            @endif
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @yield('menu-left')
            </ul>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    @include('layouts.nav.menu-right-visitor')
                @else
                    @if(Auth::id()==4)
                        @include('layouts.nav.menu-right-admin')
                    @else
                        @include('layouts.nav.menu-right-user')
                    @endif
                @endif
            </ul>
        </div>
    </div>
</nav>