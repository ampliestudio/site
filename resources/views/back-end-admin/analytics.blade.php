@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">

            @include('back-end-admin.menu-interno')

            <div class="well" style="background-color: white">
                Visitas nos últimos 30 dias: <span class="badge">{!! $mes !!}</span>
                <br>
                <br>
                Hoje
                <div class="progress">
                    <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{!! $hojec !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $hojec !!}%">
                        <span class="">{!! $hoje !!}</span>
                    </div>
                </div>
                Últimos 7 dias
                <div class="progress">
                    <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{!! $semanac !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $semanac !!}%">
                        <span class="">{!! $semana !!}</span>
                    </div>
                </div>
            </div>

            @foreach($itens as $item)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse{!! $item->id !!}" aria-expanded="false" aria-controls="collapseOne">
                                {!! $item->username !!} - {!! $item->created_at !!} - {!! $item->url !!}
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{!! $item->id !!}" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading{!! $item->id !!}">
                        <div class="panel-body">
                            <div class="row">

                                <table class="table" style="background-color: #0f253c">
                                    <tbody>
                                    <tr>
                                        <td>URL</td>
                                        <td style="text-align: inherit;white-space: pre-wrap;">{!! $item->url !!}</td>
                                    </tr>
                                    <tr>
                                        <td>request</td>
                                        <td style="text-align: inherit;white-space: pre-wrap;">{!! $item->request !!}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
@endsection
