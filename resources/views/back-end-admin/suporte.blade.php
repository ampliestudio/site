@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">

            @include('back-end-admin.menu-interno')


            @foreach($itens as $item)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse{!! $item->id !!}" aria-expanded="false" aria-controls="collapseOne">
                                {!! $item->info !!} - {!! $item->created_at !!}
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{!! $item->id !!}" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading{!! $item->id !!}">
                        <div class="panel-body">
                            <div class="row">

                                <table class="table" style="background-color: #0f253c">
                                    <tbody>
                                    <tr>
                                        <td>Protocolo</td>
                                        <td>2017{!! $item->id !!} - {!! $item->created_at !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Nome</td>
                                        <td>{!! $item->nome !!}</td>
                                    </tr>
                                    <tr>
                                        <td>ID</td>
                                        <td>{!! $item->user_id !!}</td>
                                    </tr>
                                    <tr>
                                        <td>E-mail</td>
                                        <td>{!! $item->email !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Mensagem</td>
                                        <td style="text-align: inherit;white-space: pre-wrap;">{!! $item->texto !!}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <a class="btn-danger btn btn-xs" href="{!! url('admin/drop-suporte/'.$item->id) !!}">Excluir</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
@endsection
