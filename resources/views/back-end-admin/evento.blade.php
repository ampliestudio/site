@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">

            @include('back-end-admin.menu-interno')

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                @foreach($eventos as $item)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse{!! $item->id !!}" aria-expanded="false" aria-controls="collapseOne">
                                    {!! $item->nome !!} - update: {!!  date('d/m/y',strtotime($item->updated_at)) !!} - status: {!! $item->status !!} - inicio: {!!  date('d/m/y h:i',strtotime($item->inicio)) !!}
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{!! $item->id !!}" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="heading{!! $item->id !!}">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-2">

                                    </div>
                                    <div class="col-xs-10">


                                        <form id="form{!! $item->id !!}" method="post"
                                              action="{!! url('admin/update-evento') !!}">
                                            <div class="col-sm-6">
                                                <input class="form-control" type="hidden" name="_token"
                                                       value="{!! csrf_token() !!}">
                                                <p><strong>Status</strong></p>
                                                <input class="form-control" type="text" name="status"
                                                       value="{!! $item->status !!}"><br>
                                                <p>Nome</p>
                                                <input class="form-control" type="text" name="nome"
                                                       value="{!! $item->nome !!}"><br>
                                                <p>Local</p>
                                                <input class="form-control" type="text" name="local"
                                                       value="{!! $item->local !!}"><br>
                                                <p>Horário</p>
                                                <input class="form-control" type="text" name="inicio"
                                                       value="{!! $item->inicio !!}"><br>
                                                <p>Cidade ID</p>
                                                <input class="form-control" disabled type="text" name="cidade_id"
                                                       value="{!! $item->cidade_id !!}"><br>
                                                <p>Slug ID</p>
                                                <input class="form-control" type="text" name="slug_id"
                                                       value="{!! $item->slug_id !!}"><br>
                                                <p>link fb</p>
                                                <input class="form-control" type="text" name="link_fb"
                                                       value="{!! $item->link_fb !!}"><br>

                                            </div>
                                            <div class="col-sm-6">

                                                <p>User ID</p>
                                                <input class="form-control" disabled type="text" name="user_id"
                                                       value="{!! $item->user_id !!}"><br>
                                                <p>Score</p>
                                                <input class="form-control" type="text" name="score_count"
                                                       value="{!! $item->score_count !!}" disabled><br>
                                                <p>Latitude</p>
                                                <input class="form-control" type="text" name="lat"
                                                       value="{!! $item->lat !!}"><br>
                                                <p>Longitude</p>
                                                <input class="form-control" type="text" name="lon"
                                                       value="{!! $item->lon !!}"><br>
                                                <p>Desc</p>
                                                <textarea class="form-control" type="text"
                                                          name="descricao">{!! $item->descricao !!}</textarea><br>
                                                <input type="hidden" name="id" value="{!! $item->id !!}">
                                                <input class="btn btn-primary" type="submit" name="action"
                                                       value="Atualizar"><br>
                                            </div>
                                        </form>
                                        <a style="cursor: pointer;"
                                           onclick="myNavFunc({!! $item->lat !!},{!! $item->lon !!})"
                                           class="btn btn-menu-rest btn-default"><i class="fa fa-location-arrow"></i>
                                            Direções até o
                                            local</a>
                                        {{--<a href="" class="btn btn-danger btn-xs">Excluir</a>--}}


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                    {{ $eventos->links() }}
                <br>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <table class="table-bordered table-striped">
                        <thead>
                        <tr>
                            <td>Código</td>
                            <td>Significado</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Esperando aprovação</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Aprovado</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Reprovado</td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                </div>
            </div>
        </div>
@endsection
