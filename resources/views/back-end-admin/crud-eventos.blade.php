@extends('layouts.app')

@section('content')

    <div class="container">
        @include('back-end-admin.menu-interno')

        <div class="col-sm-6">
            <p style="color: white;">Novo</p>
            <form action="{!! url('user/crud-eventos') !!}" method="post">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <input type="text" class="form-control" placeholder="nome" name="nome">
                <input type="text" class="form-control" placeholder="id" name="link">
                <select name="comercio" class="form-control">
                    @foreach($comercios as $item)
                        <option value="{!! $item->id !!}">{!! $item->slug !!}</option>
                    @endforeach
                </select>
                <input type="submit" value="enviar" class="btn btn-default">
            </form>
        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">

            <table class="table table-bordered table-responsive">
                <tr>
                    <td>Nome</td>
                    <td>ID</td>
                    <td colspan="2">Comercio</td>
                </tr>
                @foreach($paginas as $pagina)
                    <tr>
                        <td>{!! $pagina->nome !!}</td>
                        <td>{!! $pagina->link !!}</td>
                        <td>{!! $pagina->comercio !!}</td>
                        <td>
                            <a href="{!! url('user/crud-eventos-del/'.$pagina->id) !!}" class="btn btn-danger"><i
                                        class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection