@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">

            @include('back-end-admin.menu-interno')


            @foreach($itens as $item)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse{!! $item->id !!}" aria-expanded="false" aria-controls="collapseOne">
                                {!! $item->com_nome !!} / {!! $item->nome !!} /
                                Status {!! $item->status !!} <? if ($item->status == 1) {
                                    echo '<a href="' . url('admin/aprovar-item/' . $item->id) . '" class="btn btn-success btn-xs">Aprovar</a>';
                                }?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{!! $item->id !!}" class="panel-collapse collapse" role="tabpanel"
                         aria-labelledby="heading{!! $item->id !!}">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img class="img-responsive img-thumbnail"
                                         src="{!! asset('uploads/'.$item->foto) !!}" alt="">
                                </div>
                                <div class="col-xs-10">

                                    <form id="form{!! $item->id !!}" method="post"
                                          action="{!! url('admin/update-item') !!}">
                                        <input class="form-control" type="hidden" name="_token"
                                               value="{!! csrf_token() !!}">
                                        <p><strong>Status</strong></p>
                                        <input class="form-control" type="text" name="status"
                                               value="{!! $item->status !!}"><br>
                                        <p>Nome</p>
                                        <input class="form-control" type="text" name="nome" value="{!! $item->nome !!}"><br>
                                        <p>Slug</p>
                                        <input class="form-control" type="text" name="slug" value="{!! $item->slug !!}"><br>
                                        <p>Descrição</p>
                                        <input class="form-control" type="text" name="descricao"
                                               value="{!! $item->descricao !!}"><br>
                                        <p>Preço</p>
                                        <input class="form-control" type="text" name="preco"
                                               value="{!! $item->preco !!}"><br>
                                        <p>Comercio ID</p>
                                        <input class="form-control" disabled type="text" name="comercio_id"
                                               value="{!! $item->comercio_id !!}"><br>
                                        <p>Categoria ID</p>
                                        <input class="form-control" disabled type="text" name="categoria_id"
                                               value="{!! $item->categoria_id !!}"><br>


                                        <input type="hidden" name="id" value="{!! $item->id !!}">
                                        <input class="btn btn-primary" type="submit" name="action"
                                               value="Atualizar"><br>
                                    </form>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            {{ $itens->links() }}

            <table class="table-bordered table-striped">
                <thead>
                <tr>
                    <td>Código</td>
                    <td>Categoria</td>
                </tr>
                </thead>
                <tbody>
                @foreach($cat as $c)
                    <tr>
                        <td>{!! $c->id !!}</td>
                        <td>{!! $c->nome !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <br>
            <table class="table-bordered">
                <thead>
                <tr>
                    <td>Código</td>
                    <td>Significado</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Esperando aprovação</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Aprovado</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Reprovado</td>
                </tr>
                </tbody>
            </table>
            <br>
        </div>
    </div>
@endsection
