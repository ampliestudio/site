@extends('layouts.app')
@section('content')
    <div class="container">
        @if(Session::has('flash_message'))
            <div class="alert alert-info"><span
                        class="glyphicon glyphicon-ok"></span> {!! print_r(session('flash_message')) !!}<a
                        href="#"
                        class="close"
                        data-dismiss="alert"
                        aria-label="close">&times;</a>
            </div>
        @endif
        <div class="row">
            @include('back-end-admin.menu-interno')
            <table class="table">
                <thead>
                <tr>
                    <td colspan="2">Nome <span class="badge">{!! count($itens) !!}</span></td>
                    <td>email</td>
                    <td>telefone</td>
                    <td>papel</td>
                    <td>email status</td>
                    <td><a href="{!! url('admin/users') !!}">criação</a></td>
                    <td><a href="{!! url('admin/users-by-mod') !!}">ultimo login</a></td>
                    <td>exclusão</td>
                    <td>ultima cidade</td>
                    <td>enviar email</td>
                </tr>
                </thead>
                @foreach($itens as $item)
                    <tbody>
                    <tr>
                        <td><img src="{!! $item->foto !!}" height="40"></td>
                        <td><a href="{!! url('admin/user-city-history/'.$item->id) !!}" style="color: white;">{!! $item->name !!}</a><?= $item->nome_original ? '('.$item->nome_original.')':'' ?></td>
                        <td>{!! $item->email !!}</td>
                        <td>{!! $item->telefone !!}</td>
                        <td>{!! $item->role !!}</td>
                        <td>{!! $item->email_status !!}</td>
                        <td>{!! $item->created_at !!}</td>
                        <td>{!! $item->updated_at !!}</td>
                        <td>{!! $item->deleted_at !!}</td>
                        <td>{!! $item->ultima_cidade !!}</td>
                        <td>
                            <a href="{!! url('admin/email-mkt/'.$item->id) !!}" class="btn btn-default"> <i class="fa fa-envelope-o"></i></a>
                        </td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
        </div>
        <br>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <table class="table">
                <thead>
                <tr>
                    <td>Papel &nbsp;</td>
                    <td>Significado</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Verificado</td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>Padrão</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Suspenso - Abuso</td>
                </tr>
                </tbody>
            </table>
            <br>
        </div>
    </div>
@endsection
