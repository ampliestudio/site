@extends('layouts.app')
@section('content')
    <div class="container">
        @if(Session::has('flash_message'))
            <div class="alert alert-info"><span
                        class="glyphicon glyphicon-ok"></span> {!! print_r(session('flash_message')) !!}<a
                        href="#"
                        class="close"
                        data-dismiss="alert"
                        aria-label="close">&times;</a>
            </div>
        @endif
        <div class="row">
            @include('back-end-admin.menu-interno')
            <table class="table">
                <thead>
                <tr>
                    <td colspan="2">{!! $user->name !!}</td>
                </tr>
                <tr>
                    <td>Cidade</td>
                    <td>Data</td>
                </tr>
                </thead>
                @foreach($itens as $item)
                    <tbody>
                    <tr>
                        <td>{!! $item->cidadenome !!}</td>
                        <td>{!! $item->created_at !!}</td>
                    </tr>
                    </tbody>
                @endforeach
            </table>
        </div>
    </div>
@endsection
