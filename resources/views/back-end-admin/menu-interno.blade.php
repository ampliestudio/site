<ol class="breadcrumb" style="background-color: white">
    <li><a href="{!! url('admin/home') !!}"><i class="fa fa-home"></i> Locais e Cidades</a></li>
    <li><a href="{!! url('admin/evento') !!}"><i class="fa fa-calendar"></i> Eventos</a></li>
    {{--<li><a href="{!! url('admin/itens') !!}" style="text-decoration: line-through"><i class="fa fa-list"></i> Itens (depreciado)</a></li>--}}
    <li><a href="{!! url('admin/suporte') !!}"><i class="fa fa-cog"></i> Suporte</a></li>
    <li><a href="{!! url('admin/users') !!}"><i class="fa fa-user"></i> Usuários</a></li>
    <li><a href="{!! url('admin/analytics') !!}"><i class="fa fa-pie-chart"></i> Analytics</a></li>
    <li><a href="{!! url('admin/crud-eventos') !!}"><i class="fa fa-facebook-square"></i> Facebook sync</a></li>
</ol>
<style>
    td {
        color: white;
    }
</style>