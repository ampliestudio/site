@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @if(Session::has('flash_message'))
                <div class="alert alert-info"><span
                            class="glyphicon glyphicon-ok"></span> {!! print_r(session('flash_message')) !!}<a
                            href="#"
                            class="close"
                            data-dismiss="alert"
                            aria-label="close">&times;</a>
                </div>
            @endif
            @include('back-end-admin.menu-interno')
            <a href="{!! url('admin/importa-local-do-facebook') !!}" style="color: white">Importar local do facebook</a>
            <hr>
            <table class="table">
                <thead>
                <tr>
                    <td colspan="2">Cidade</td>
                    <td>Locais</td>
                </tr>
                </thead>
                <tbody>
                @foreach($cidades as $cidade)
                    <tr>
                        <? $eventos = \CityTips\Eventos::where('cidade_id',$cidade->id)->whereDate('inicio','>=',Carbon\Carbon::now())->get() ?>
                        <td><input id="checkbox-{!! $cidade->id !!}" onclick="checkboxFunc({!! $cidade->id !!})"
                                   type="checkbox"></td>
                        <td><a style="color: white"
                               href="{!! url('admin/gerenciar-cidade/'.$cidade->id) !!}">{!! $cidade->nome !!}</a> &nbsp; ({!! count($eventos) !!})</td>
                        <td>{!! count(\CityTips\Comercios::where('cidade_id',$cidade->id)->get()) !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <script>
                function checkboxFunc(id) {
                    if (document.getElementById('checkbox-'+id).checked) {
                        $('#input-cidades-update').val($('#input-cidades-update').val()+','+id);
                        $('#botao-cidades-update').prop('disabled', false);
                    } else {
                        var replace = $('#input-cidades-update').val();
                        replace = replace.replace(','+id,'');
                        $('#input-cidades-update').val(replace);
                        if($('#input-cidades-update').val()==""){
                            $('#botao-cidades-update').prop('disabled', true);
                        }
                    }
                }

            </script>
            <form action="{!! url('user/update-eventos-massa') !!}" method="post">
                <input type="submit" class="btn btn-danger" id="botao-cidades-update" disabled value="Atulaziar Eventos">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <input type="hidden" id="input-cidades-update" name="id" value="">
            </form>
                <a href="{!! url('admin/update-covers') !!}" class="btn btn-action">Update Covers</a>
            <br>
            <h3 style="color: white">Pendentes:</h3>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                @foreach($comerciospendentes as $item)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse{!! $item->id !!}" aria-expanded="false" aria-controls="collapseOne">
                                    {!! $item->nome !!} / {!! $item->cidade_nome !!} / Status: {!! $item->status !!}
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{!! $item->id !!}" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="heading{!! $item->id !!}">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <img class="img-responsive img-thumbnail"
                                             src="{!! asset('uploads/'.$item->foto) !!}" alt="">
                                    </div>
                                    <div class="col-xs-10">


                                        <form id="form{!! $item->id !!}" method="post"
                                              action="{!! url('admin/update') !!}">
                                            <div class="col-sm-6">
                                                <input class="form-control" type="hidden" name="_token"
                                                       value="{!! csrf_token() !!}">
                                                <p><strong>Status</strong></p>
                                                <input class="form-control" type="text" name="status"
                                                       value="{!! $item->status !!}"><br>
                                                <p>Nome</p>
                                                <input class="form-control" type="text" name="nome"
                                                       value="{!! $item->nome !!}"><br>
                                                <p>Endereço</p>
                                                <input class="form-control" type="text" name="endereco"
                                                       value="{!! $item->endereco !!}"><br>
                                                <p>Entrega</p>
                                                <input class="form-control" type="text" name="entrega"
                                                       value="{!! $item->entrega !!}"><br>
                                                <p>Horário</p>
                                                <input class="form-control" type="text" name="horario"
                                                       value="{!! $item->horario !!}"><br>
                                                <p>Telefone</p>
                                                <input class="form-control" type="text" name="telefone"
                                                       value="{!! $item->telefone !!}"><br>
                                                <p>Cidade ID</p>
                                                <input class="form-control" type="text" name="cidade_id"
                                                       value="{!! $item->cidade_id !!}"><br>
                                                <p>Slug</p>
                                                <input class="form-control" type="text" name="slug"
                                                       value="{!! $item->slug !!}"><br>
                                                <p>tipo</p>
                                                <input class="form-control" type="text" name="tipo"
                                                       value="{!! $item->tipo !!}"><br>

                                            </div>
                                            <div class="col-sm-6">
                                                <p>Legal</p>
                                                <input class="form-control" type="text" name="legal_info"
                                                       value="{!! $item->legal_info !!}"><br>
                                                <p>User ID</p>
                                                <input class="form-control" type="text" name="user_id"
                                                       value="{!! $item->user_id !!}"><br>
                                                <p>descricao</p>
                                                <textarea class="form-control" type="text" name="descricao">
                                                    {!! $item->descricao !!}
                                                </textarea><br>
                                                <p>Rate</p>
                                                <input class="form-control" type="text" name="rate_count"
                                                       value="{!! $item->rate_count !!}" disabled><br>
                                                <p>Oficial</p>
                                                <input class="form-control" type="text" name="oficial"
                                                       value="{!! $item->oficial !!}"><br>
                                                <p>Latitude</p>
                                                <input class="form-control" type="text" name="lat"
                                                       value="{!! $item->lat !!}"><br>
                                                <p>Longitude</p>
                                                <input class="form-control" type="text" name="lon"
                                                       value="{!! $item->lon !!}"><br>
                                                <p>Foto</p>
                                                <input class="form-control" type="text" name="foto"
                                                       value="{!! $item->foto !!}" disabled><br>
                                                <p>Site</p>
                                                <small>helper</small>
                                                <textarea><a href="" target="_blank"></a></textarea>
                                                <textarea class="form-control" type="text"
                                                          name="site">{!! $item->site !!}</textarea><br>
                                                <input type="hidden" name="id" value="{!! $item->id !!}">
                                                <input class="btn btn-primary" type="submit" name="action"
                                                       value="Atualizar"><br>
                                            </div>
                                        </form>
                                        <a style="cursor: pointer;"
                                           onclick="myNavFunc({!! $item->lat !!},{!! $item->lon !!})"
                                           class="btn btn-menu-rest btn-default"><i class="fa fa-location-arrow"></i>
                                            Direções até o
                                            local</a>
                                        {{--<a href="" class="btn btn-danger btn-xs">Excluir</a>--}}


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{ $comerciospendentes->links() }}

                <br>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <table class="table-bordered">
                        <thead>
                        <tr>
                            <td>Código</td>
                            <td>Significado</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Esperando aprovação</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Aprovado</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Reprovado</td>
                        </tr>
                        </tbody>
                    </table>
                    <br>
                </div>
            </div>
        </div>
@endsection
