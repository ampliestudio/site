@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Importa local do facebook</div>
                    <div class="panel-body">


                        <script type="text/javascript"
                                src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&key=AIzaSyBd49Kf65vAG2amO_d6nl37AiRyYfJUqHk"></script>

                        <small class="pull-right">*Obrigatório</small>
                        <form action="{!! url('/admin/importa-local-do-facebook') !!}" method="post" enctype='multipart/form-data'>
                            <input type='hidden' name='_token' value="{!! csrf_token() !!}">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="image">Logo do estabelecimento*</label>
                                        <p>
                                            <small>-A imagem deve ser <strong>quadrada</strong> e não ultrapassar 200kb</small>
                                        </p>
                                        <input type="file" name="image">
                                        <div class='text-danger'>{{$errors->first('image')}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="site">ID do <i class="fa fa-facebook-official"></i></label>
                                        <input class="form-control" type="text" name="site">
                                        <div class='text-danger'>{{$errors->first('site')}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="entrega">Faz entrega?*</label>
                                        <div class='text-danger'>{{$errors->first('entrega')}}</div>
                                        <br>
                                        <input type="radio" name="entrega" value="1"> Sim
                                        <input type="radio" name="entrega" value="0"> Não
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="sel1">Cidade*</label>
                                        <select name="cidade" class="form-control" id="sel1">
                                            @foreach($cidades as $item)
                                                <option value="{!! $item->id !!}">{!! $item->nome !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="sel1">Categoria*</label>
                                        <select name="tipo" class="form-control" id="sel1">
                                            <option value="1">Refeição</option>
                                            <option value="2">Happy-Hour</option>
                                            <option value="3">Diversão(Balada)</option>
                                            <option disabled>──────────</option>
                                            <option value="12">Refeição e Happy-Hour</option>
                                            <option value="23">Diversão(Balada) e Happy-Hour</option>
                                            <option value="13">Diversão(Balada) e Refeição</option>
                                            <option disabled>──────────</option>
                                            <option value="123">Todas as Categorias</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn-add-comercio btn btn-success">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
