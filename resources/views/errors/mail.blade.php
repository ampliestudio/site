@extends('layouts.app')



@section('content')
    {{--Modal--}}
    <br><br>
    <div class="container-fluid">
        <div class="col-xs-12">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Ops... <i class="fa fa-asterisk"></i></h1>
                <br><br>
                <p class="text-center" style="color: white"><strong>
                        Parece que você não autorizou compartilhar seu e-mail conosco e, isso é extremamente importante para criarmos seu cadastro, você deve entrar no seu facebook e nas configurações de aplicativos autorizar o uso do seu email no app CityTips como na imagem abaixo
                        </strong></p>
                <img src="{!! asset('images/help.png') !!}" alt="">
                <p class="text-center" style="color: white"><strong>Caso não tenha email cadastrado no facebook tente isso: <a href="https://pt-br.facebook.com/help/162801153783275?helpref=faq_content" style="color: white">https://pt-br.facebook.com/help/162801153783275?helpref=faq_content</a></strong></p>

                <h6 class="text-center"><a href="mailto:contato@citytips.com.br?subject=Erro%20404%20CityTips&body=Encontrei%20um%20erro%20na%20url%20{!! Request::url() !!}">Tenho uma dúvida</a></h6>
            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection
