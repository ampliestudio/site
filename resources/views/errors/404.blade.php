@extends('layouts.app')

@section('menu-left')
    {{--incluir modal em content--}}
    @include('layouts.nav.menu-left-pagina-cidade')
@endsection

@section('content')
    {{--Modal--}}
@include('sessoes.modal.modal-mudar-cidade')
    <br><br>
    <div class="container-fluid">
        <div class="col-md-offset-1 col-md-10">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Não há nada aqui <i class="fa fa-life-ring"></i></h1>
                <br><br>
                <p class="text-center" style="color: white"><strong>
                        O que quer que você estava procurando não existe atualmente neste endereço. A menos que você
                        estava procurando essa
                        página de erro, se for o caso: Parabéns! Você a encontrou.</strong></p>
                {{--<h6 class="text-center"><a href="mailto:contato@citytips.com.br?subject=Erro%20404%20CityTips&body=Encontrei%20um%20erro%20na%20url%20{!! Request::url() !!}">Reportar erro</a> #404</h6>--}}
            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection
