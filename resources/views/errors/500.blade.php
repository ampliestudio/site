@extends('layouts.app')

{{--@section('menu-left')--}}
{{--incluir modal em content--}}
{{--@include('layouts.nav.menu-left-pagina-cidade')--}}
{{--@endsection--}}

@section('content')
    {{--Modal--}}
    {{--@include('sessoes.modal.modal-mudar-cidade')--}}

    <br><br>
    <div class="container-fluid">
        <div class="col-md-offset-2 col-md-8">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Algo de errado não está certo... <i class="fa fa-meh-o"></i></h1>
                <br><br>
                <p class="text-center" style="color: white">
                        Você pode ter encontrado um erro, mas fique calmo! Já estamos trabalhando para resolver isso.</p>
                <p class="text-center" style="color: white">
                    <a  href="https://twitter.com/citytipsbr" target="_blank" style="color: whitesmoke">Status do Sistema</a></p>
                {{--<h6 class="text-center"><a href="mailto:contato@citytips.com.br?subject=Erro%20500%20CityTips&body=Encontrei%20um%20erro%20na%20url%20{!! Request::url() !!}">Reportar erro</a> #500</h6>--}}
            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection
