@extends('layouts.app')



@section('content')
    {{--Modal--}}
    <br><br>
    <div class="container-fluid">
        <div class="col-xs-12">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Ops... <i class="fa fa-asterisk"></i></h1>
                <br><br>
                <p class="text-center" style="color: white"><strong>
                        No momento estamos com um problema para conectar ao Facebook, estamos trabalhando nisso e esperamos voltar logo!
                        </strong></p>
            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection
