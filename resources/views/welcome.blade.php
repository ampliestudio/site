@extends('layouts.app')
@section('meta')
    <meta name="CityTips.com.br">
    <meta name="description" content="Descubra e compartilhe o seu rolê com CityTips">
    <meta name="keyowrds" content="city tips, bar, pub, balada, eventos,">
    <meta name="robots" content="index, follow, nosnippet">
    <meta property="og:url" content="{!! Request::url() !!}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="O que tem pra hj?!"/>
    <meta property="og:description"
          content="Descubra e compartilhe o seu rolê com CityTips"/>
    <meta property="og:image" content="{!! asset('images/wall-2.png') !!}"/>
    <meta property="fb:app_id" content="{!! Config::get('constants.FBAPPID') !!}"/>
@endsection
@section('menu-left')
    {{--incluir modal em content--}}
    @include('layouts.nav.menu-left-pagina-cidade-usuario-home')
@endsection
@section('content')
    <div class="arrow bounce">
    </div>
    {{--<h4 class="title-home-xs clickable" data-toggle="modal" data-target="#myModal">O que tem pra hoje?</h4>--}}
    <img id="image" class="visible-xs image-rel" src="{!! asset('images/wall.png') !!}" alt="...">
    <video id="video" class="hidden-xs image-rel" autoplay muted loop poster="{!! asset('images/wall.png') !!}">
        <source src="{!! asset('images/wall.mp4') !!}" type="video/mp4">
    </video>

    <div id="citytips-home">
        <div class="container entre-home">
            <div class="row">

                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <a href="{!! url('auth/facebook') !!}"
                       class="btn btn-lg btn-social btn-block btn-facebook">
                        <span class="fa fa-facebook"></span> Entre com o Facebook!
                    </a>
                </div>
            </div>
            <br>
            <p class="text-center">
                <small style="color: #b8b8b8;">Ao me registrar em CityTips aceito os <a
                            href="{!! url('tips/termos') !!}">termos</a>
                    impostos pelo site
                </small>
            </p>
        </div>
    </div>
    <div class="row cidades-welcome">
        <h4 class="text-center" style="color: white">Escolha sua cidade</h4>
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            @foreach($cidades as $atual)
                <div class="col-sm-6">
                    <div class="bs-callout-city lista-cidades"
                         style="background-image: url('https://citytips.com.br/images/{!! $atual->slug !!}.jpg') ;background-size: cover; border-color: #f8f8f8;border-width: 3px;">
                        <p class="text-center"><a href="{!! url('/'.$atual->slug) !!}">{!! $atual->nome !!}</a></p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @include('sessoes.modal.modal-mudar-cidade')

    @include('layouts.footer')
    <script>
        var ypos, image, video;
        function paralax() {
            ypos = window.pageYOffset;
            image = document.getElementById('image');
            image.style.top = ypos * .4 - 50 + 'px';
            video = document.getElementById('video');
            video.style.top = ypos * .4 - 50 + 'px';
        }
        window.addEventListener('scroll', paralax);

    </script>
@endsection
