<div id="comercios">
    <div class="row">
        {{--<div class="col-sm-6">--}}
        {{--<h4 class="tab-titulo text-center"> &nbsp;<i class="fa fa-glass"></i></h4>--}}
        {{--</div>--}}
        <div class="col-xs-12">
            <div class="col-sm-6">
                {{--<p>&nbsp;Filtrar:</p>--}}
                <div id="filters" role="group" class="button-group btn-group filter-button-group">
                    <div class="visible-xs">
                        <button id="botao-tudo" class="m-filter btn botao-filtro btn-success is-checked"
                                data-filter="*">Tudo
                        </button>
                        <button class="btn m-filter botao-filtro btn-success"
                                data-filter=".tipo-1, .tipo-123, .tipo-12, .tipo-13">
                            <i class="fa fa-cutlery m-filter">&nbsp;Refeição</i>
                        </button>
                        <button class="btn m-filter botao-filtro btn-success"
                                data-filter=".tipo-3, .tipo-123, .tipo-23, .tipo-13">
                            <i class="fa fa-music m-filter">&nbsp;Diversão</i>
                        </button>
                        <button class="btn m-filter botao-filtro btn-success"
                                data-filter=".tipo-2, .tipo-123, .tipo-12, .tipo23">
                            <i class="fa fa-glass m-filter">&nbsp;Happy-Hour</i>
                        </button>
                    </div>
                    <div class="visible-lg visible-sm visible-md">
                        <button class="btn botao-filtro btn-success is-checked" data-filter="*">Tudo</button>
                        <button class="btn botao-filtro btn-success"
                                data-filter=".tipo-1, .tipo-123, .tipo-12, .tipo-13">
                            <i class="fa fa-cutlery m-filter">&nbsp;Refeição</i>
                        </button>
                        <button class="btn botao-filtro btn-success"
                                data-filter=".tipo-3, .tipo-123, .tipo-23, .tipo-13">
                            <i class="fa fa-music m-filter">&nbsp;Diversão</i>
                        </button>
                        <button class="btn botao-filtro btn-success"
                                data-filter=".tipo-2, .tipo-123, .tipo-12, .tipo23">
                            <i class="fa fa-glass m-filter">&nbsp;Happy-Hour</i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <br class="visible-xs">
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                    <input type="text" id="quicksearch" class="form-control" placeholder="Buscar"/>
                </div>
            </div>
        </div>
    </div>
    @if(rand(0,15)==1)
        <br>
        <div class="col-xs12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Dica!</strong> Quanto mais recomendações um local tem, mais acima na página ele fica.
            </div>
        </div>
    @elseif(rand(0,10)==1)
        <br>
        <div class="col-xs12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Dica!</strong> Crie um evento para anúnciar um show, uma festa ou um prato do seu restaurante
                favorito daqui de {!! $cidade->nome !!}.
            </div>
        </div>
    @endif
    {{--<div class="fix-titulo visible-xs"></div>--}}
    <div class="grid row">
        @include('sessoes.sub-comercio.comercios-box')
    </div>
    @if(count($comercios)==0)
        {{--no cards to display--}}
        <div class="comercios col-xs-12">
            <div class="bs-callout bs-callout-info">
                <div class="row">
                    <div class="col-sm-8">
                        <p><i class="fa fa-exclamation fa-4x"></i> &nbsp Não há nada aqui, começe você mesmo &nbsp <a
                                    class="btn btn-default" href="{!! url('user/add-local') !!}">Criar novo local</a>
                        </p>
                    </div>
                </div>


                {{--<div id="googleMap" style="width:500px;height:380px;"></div>--}}
            </div>
        </div>
        <div class="pull-down"></div>
    @else
        <div class="col-xs-12">
            <br>
            <form action="{!! url('tips/falta-algo') !!}" method="post">
                <div class="row">
                    <div class="col-sm-6">
                        <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                        <input type="text" id="texto-falta" placeholder="Falta algum lugar aqui?" name="texto"
                               class="form-control">
                    </div>
                    <div id="aparecer-focus" class="col-sm-4 sr-only">
                        <input type="text" id="texto-falta" placeholder="E-mail"
                               value="<?= \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->email : '' ?>"
                               name="email"
                               class="form-control">
                    </div>
                    <br class="visible-xs">
                    <div class="col-sm-2">
                        <input type="submit" value="Enviar" class="btn btn-success">
                    </div>
                </div>
            </form>
        </div>
    @endif
</div>
<script>
    jQuery(document).ready(function () {
        setTimeout(function () {
            document.getElementById("botao-tudo").click();
        }, 500);
    });

    jQuery('#button-com-click').click(function () {
        setTimeout(function () {
            document.getElementById("botao-tudo").click();
        }, 200);
    });

    $('#texto-falta').bind('focus', function () {
        $('#aparecer-focus').removeClass('sr-only');
    });
</script>

