<div class="row">
    {{--<div class="col-xs-12">--}}
    {{--<h4 class="tab-titulo text-center"> &nbsp;<i class="fa fa-calendar-o"></i></h4>--}}
    {{--<a href="{!! url('eventos-antigos/'.$cidade->slug) !!}">Eventos passados</a>--}}
    {{--</div>--}}

    <br><br>
    <div class="col-sm-12 col-md-12">
        @if(Auth::check())
            @include('sessoes.sub-evento.caixa-status')
        @endif
    </div>
    <?
    //o código a seguir separa os eventos seguintes por data
    $ultima_data = 0;
    $exibiu_passados = 0;
    $ultimo_local = 0;
    $ultimo_dia = 0;
    $ultimo_local_slug = 0;
    $ultimo_local_nome = 0;
    $mesmo_local_count = 0;
    ?>

    @if(count($eventos) != 0)
        @foreach($eventos as $evento)
            <?php
            //verifica eventos repetidos do mesmo lugar
            $dia_cru = strtotime($evento->inicio);
            $dia = date('d', $dia_cru);
            if ($ultimo_dia == $dia) {
                if ($ultimo_local == $evento->id_local) {
                    $mesmo_local_count++;
                    $ultimo_dia = $dia;
                    $ultimo_local = $evento->id_local;
                    $ultimo_local_slug = $evento->local_slug;
                    $ultimo_local_nome = $evento->local_nome;
                    continue;
                }
            }
            if ($mesmo_local_count > 0) {
                if ($ultimo_local != $evento->id_local || $ultimo_dia != $dia) {
                    echo '<div class="row"> <div class="col-sm-4 col-xs-11 col-xs-offset-1  col-sm-offset-4"><a href="' . url("place/" . $ultimo_local_slug) . '" style="color:white">Ver mais ' . $mesmo_local_count . ' de ' . $ultimo_local_nome . '...</a></div></div>';
                    $mesmo_local_count = 0;
                }
            }
            $dia_cru = strtotime($evento->inicio);
            $dia = date('d', $dia_cru);
            $ultimo_dia = $dia;
            $ultimo_local = $evento->id_local;

            //continuação do cod que separa os eventos seguintes por data

            date_default_timezone_set('America/Sao_Paulo');
            Carbon\Carbon::setLocale('pt_BR');
            $data = $evento->inicio;
            $data = strtotime($data);
            $dia = date('d', $data);
            $mes = date('m', $data);
            $diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado');
            $diasemana_numero = date('w', $data);
            $hoje = \Carbon\Carbon::today();
            $data_limite = $hoje->addDays(7);
            $hoje = \Carbon\Carbon::today();
            if ($ultima_data != $dia) {
                $ultima_data = $dia;
                if ($evento->inicio >= $hoje && $evento->inicio < $data_limite) {
                    echo '    <div class="col-xs-10 col-xs-offset-1">
<p class="data-divisao">' . $diasemana[$diasemana_numero] . '</p></div>';
                } elseif ($evento->inicio < $hoje && $exibiu_passados == 0) {
                    $exibiu_passados = 1;
                    echo '    <div class="col-xs-10 col-xs-offset-1">
<p class="data-divisao">Eventos passados</p></div>';
                } elseif ($evento->inicio >= $data_limite) {
                    echo '    <div class="col-xs-10 col-xs-offset-1">
<p class="data-divisao">' . $dia . '/' . $mes . '</p></div>';
                }
            }
            ?>
            @include('sessoes.sub-evento.evento-bloco')
        @endforeach
        <div class="append">

        </div>
    @endif
    @if(count($eventos)==0 )
        {{--no cards to display--}}
        <div class="comercios col-sm-12 col-md-8 col-md-offset-2">
            <div class="bs-callout bs-callout-info">
                <div class="row">
                    <div class="col-sm-8">
                        <p><i class="fa fa-exclamation fa-4x"></i> &nbsp Não há nada aqui, começe você mesmo &nbsp <a
                                    class="btn btn-default" href="{!! url('user/criar-evento') !!}">Criar evento</a>
                        </p>
                    </div>
                </div>


                {{--<div id="googleMap" style="width:500px;height:380px;"></div>--}}
            </div>
        </div>
        <div class="pull-down"></div>
    @else
        <div class="load-more col-xs-12">
            {{--<a href="{!! url('user/criar-evento') !!}" class="btn-add-comercio btn btn-success">Criar evento</a>--}}
            <button onclick="getnewpage()" class="btn-add-comercio botao-carregar-mais btn btn-action">Carregar Mais
            </button>
            <br><br>
        </div>
    @endif

</div>
<script>
    //    call infinite scroll

    //        setTimeout(function () {
    //            $(window).scroll(function () {
    //                if ($(window).scrollTop() + $(window).height() == $(document).height()) {
    //                    getnewpage();
    //                }
    //            });
    //        }, 5000);


    //infinite scroll
    var pageNumber = 2;

    function getnewpage() {

        var articles;
        $.ajax({
            type: 'GET',
            url: '/user/city/{!! $cidade->id !!}?page=' + pageNumber,
            success: function (data) {
                pageNumber += 1;
                if (data.length == 0) {
                    $(".botao-carregar-mais").fadeOut(function () {
                        $(this).text("Isso é tudo").fadeIn();
                    });
                } else {

                    $(".append").append(data);
                }
            }, error: function (data) {
                $(this).text("Erro de conexão").fadeIn();

            },
        })
    }
</script>

