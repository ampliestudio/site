@extends('layouts.app')
@section('menu-left')
    {{--incluir modal em content--}}
    @include('layouts.nav.menu-left-pagina-cidade-usuario-home')
@endsection

@section('content')
    {{--Modal--}}
    @include('sessoes.modal.modal-mudar-cidade')
    <div class="row">
        <h4 class="text-center" style="color: white">Escolha sua cidade</h4>
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            @foreach($cidades as $atual)
                <div class="col-sm-6">
                    <div class="bs-callout-city lista-cidades"
                         style="background-image: url('https://citytips.com.br/images/{!! $atual->slug !!}.jpg') ;background-size: cover; border-color: #f8f8f8;border-width: 3px;">
                        <p class="text-center"><a href="{!! url('/'.$atual->slug) !!}">{!! $atual->nome !!}</a></p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
