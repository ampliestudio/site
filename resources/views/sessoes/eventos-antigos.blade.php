@extends('layouts.app')

@section('menu-left')
    {{--incluir modal em content--}}
    @include('layouts.nav.menu-left-pagina-cidade')
@endsection

@section('content')
    {{--Modal--}}
    @include('sessoes.modal.modal-mudar-cidade')
    <div class="row">
        <div class=" col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

            &nbsp;<a href="{!! url($cidade->slug) !!}"><i class="fa fa-chevron-circle-left"> &nbsp;Voltar</i></a>
        </div>
    </div>
    @if(count($eventos)==0)
        <div class="row">
            <div class=" col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <div class="bs-callout bs-callout-info">
                    <p><i class="fa fa-exclamation fa-4x"></i> &nbsp Não há nada aqui, começe você mesmo &nbsp <a
                                class="btn btn-default" href="{!! url('user/criar-evento') !!}">Criar evento</a>
                    </p>
                </div>
            </div>
        </div>
        <br><br><br><br>

    @endif
        <div class="col-sm-8 col-sm-offset-2">
            <table id="eventos" class="table">
                <tbody>
                <tr><td colspan="3"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <!-- citytips -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-1291546981046674"
                             data-ad-slot="9879717207"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script></td></tr><tr class="data-divisao"><td colspan="3">
                        <p class="text-center">Eventos passados</p></td></tr>
                @if(count($eventos)==0)
                    <tr class="data-divisao">
                        <td colspan="3">
                            <p class="text-center">Poxa, nenhum evento..</p>
                        </td>
                    </tr>
                    <tr class="data-divisao">
                        <td colspan="3">
                            <p class="text-center">Mas você pode criar um!</p>
                        </td>
                    </tr>
                    <tr class="data-divisao">
                        <td colspan="3">
                            <p class="text-center"><a href="{!! url('user/criar-evento-novo/'.$cidade->id) !!}"><i class="fa fa-chevron-right"></i> Criar evento</a></p>
                        </td>
                    </tr>

                @else
                    @foreach($eventos as $evento)
                        <?
                        //continuação do cod que separa os eventos seguintes por data

                        date_default_timezone_set('America/Sao_Paulo');
                        Carbon\Carbon::setLocale('pt_BR');
                        $data = $evento->inicio;
                        $data = strtotime($data);
                        $dia = date('d', $data);
                        $mes = date('m', $data);
                        $diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado');
                        $diasemana_numero = date('w', $data);
                        $hoje = \Carbon\Carbon::today();
                        $data_limite = $hoje->addDays(7);
                        $hoje = \Carbon\Carbon::today();
                        date_default_timezone_set('America/Sao_Paulo');
                        $data = $evento->inicio;
                        $data = strtotime($data);
                        $dia = date('d', $data);
                        $mes = date('m', $data);
                        $hora = date('H:i', $data);
                        ?>
                        <tr>
                            <td>
                                @if(isset($evento->local_foto))
                                    <img src="{!! asset('uploads/'. $evento->local_foto) !!}"
                                         class="local-foto"
                                         alt="{!! $evento->local_nome !!}">
                                @else
                                    <img src="{!! asset('uploads/alias-image.png') !!}"
                                         class="local-foto"
                                         alt="{!! $evento->local_nome !!}">
                                @endif
                            </td>
                            <td>
                                <p>
                                    &nbsp; <a href="{!! url('evento/'.$evento->slug_id) !!}">{!! $evento->nome !!}</a>
                                </p>
                                <p>
                                    &nbsp; <a href="{!! url('place/'.$evento->local_slug) !!}">{!! $evento->local_nome !!}</a>
                                    <span class="pull-right">{!! $hora !!}h</span>
                                </p>
                            </td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
        </div>

    <div class="row">
        <div class=" col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            {{ $eventos->links() }}
        </div>
    </div>




    @include('layouts.footer')
@endsection
