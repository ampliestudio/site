@foreach($comercios as $comercio)
    <?
    //rate comercio

    $soma_rate_atual = 0;
    $sou_eu = 0;
    ?>
    @foreach($rate as $rate_atual)
        @if($rate_atual->comercio_id == $comercio->id)
            <?
            //opa! temos mais um positivo!
            $soma_rate_atual++;
            //opa! a pessoa logada que deu esse positivo!
            if (\Illuminate\Support\Facades\Auth::id() == $rate_atual->user_id) {
                $sou_eu = 1;
            }
            ?>
        @endif
    @endforeach
    <div class="col-sm-6 col-xs-12 grid-sizer"></div>
    <div class="comercios grid-item tipo-{!! $comercio->tipo !!} col-xs-12 col-sm-6"
         id="com-{!! $comercio->id !!}">
        {{--<div class="ribbon"><span class="fa fa-thumbs-o-up"> {!! $comercio->rate_count !!}</span></div>--}}
        <div class="bs-callout bs-callout-info">
            {{--header card--}}
            <h3>
                <a href="{!! url('/place/'.$comercio->slug) !!}">{!! $comercio->nome !!}</a><?= $comercio->oficial ? ' <small><span class="oficial-icon fa fa-check-square-o" data-toggle="tooltip" data-placement="top" title="Lugares com o simbolo de verificado são atualizados pelo próprio estabelecimento"></span></small>' : '' ?>
            </h3>
                    <span class="pull-right badge badge-titulo badge-success recomendado-icone"><i
                                class="fa fa-thumbs-o-up"> <span
                                    id="badge-place-{!! $comercio->id !!}">{!! $comercio->rate_count !!}</span></i></span>
            {{--body card--}}
            <div class="row">
                {{--image card--}}
                <div class="col-xs-4 fix-image-card">
                    <div class="row">
                        <img src="{!! asset('uploads/'.$comercio->foto) !!}"
                             class="img-responsive img-thumbnail"
                             alt="Responsive image">
                    </div>
                </div>
                {{--info card--}}
                <div class="col-xs-8 fix-info-card">
                    <?= $comercio->telefone ? '<p class="card-body-item"><i class="fa fa-phone"></i> ' . $comercio->telefone . '</p>' : '<p><i class="fa fa-phone"></i>&nbsp; <a href="' . url("tips/suporte/1/5/.$comercio->id") . '">Adicionar telefone</a></p>' ?>
                    <?= $comercio->site ? '<p class="card-body-item"><i class="fa fa-chevron-right"></i> ' . $comercio->site . '</p>' : '<p><i class="fa fa-chevron-right"></i>&nbsp; <a href="' . url("tips/suporte/1/5/.$comercio->id") . '">Adicionar site</a></p>' ?>


                    <? $eventos = count(\CityTips\Eventos::where('id_local', $comercio->id)->whereDate('inicio', '>=', \Carbon\Carbon::today()->toDateString())->get()); ?>
                    <? if ($eventos > 0) {
                        echo '<a class="btn btn-menu-rest btn-default" href="' . url('/place/' . $comercio->slug) . '"><i class="fa fa-calendar"></i> Eventos <span class="badge-danger badge">' . $eventos . '</span></a>';
                    } else {
                        echo '<a class="btn btn-menu-rest btn-default" href="' . url('/place/' . $comercio->slug) . '"><i class="fa fa-calendar"></i> Eventos </a>';
                    }  ?>

                    {{--GPS--}}
                    @if($comercio->lat != 0)
                        <a style="cursor: pointer;"
                           onclick="myNavFunc({!! $comercio->lat !!},{!! $comercio->lon !!})"
                           class="btn btn-menu-rest btn-default"><i class="fa fa-location-arrow"></i> Direções até o
                            local</a>
                    @endif

                    <p class="bottom-card-autor">
                        Autor: {!! $comercio->user_name .' - ' !!}<?= $comercio->legal_info ? '&copy; ' . $comercio->legal_info . '' : '&copy; ' . $comercio->nome ?></p>
                </div>

            </div>
            <br>
            {{--footer card--}}
            <div class="row">
                <div class="col-xs-10">
                    @if(Auth::guest())
                        <div class="btn-group">

                            <a href="{!! url('login') !!}">
                                Recomendar <span
                                        id="comercio-rate-thumb-{!! $comercio->id !!}"
                                        class="fa fa-thumbs-o-up">&nbsp;</span>
                            </a>
                        </div>
                    @else
                        <div class="btn-group">

                            <a onclick="upplace({!! $comercio->id !!})">
                                <?= $sou_eu ? '<span id="comercio-rate-text-' . $comercio->id . '" class="up-up">Recomendado</span>' : '<span id="comercio-rate-text-' . $comercio->id . '" class="">Recomendar</span>' ?>
                                <span
                                        id="comercio-rate-thumb-{!! $comercio->id !!}"
                                        class="fa <?= $sou_eu ? 'fa-thumbs-up up-up' : 'fa-thumbs-o-up' ?>">&nbsp;</span>
                            </a>
                        </div>
                    @endif
                    &nbsp
                    <div class="btn-group">
                        <a target="_blank"
                           href="javascript:myPopup('http%3A%2F%2Fwww.facebook.com%2Fshare.php%3Fu%3D{!! url('place/'.$comercio->slug) !!}')">
                            Compartilhar <span class="fa fa-share"></span>
                        </a>
                    </div>

                </div>
                <div class="col-xs-2">
                    <div class="btn-group pull-right">
                        <a class="clickable" type="button" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <span class="glyphicon glyphicon-flag"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="{!! url('tips/suporte/1/5/'.$comercio->id) !!}"><span
                                            class="fa fa-pencil"></span> Editar</a>
                            </li>
                            <li>
                                <a href="{!! url('tips/suporte/1/4/'.$comercio->id) !!}"><span
                                            class="fa fa-flag"></span> Solicitar remoção </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            {{--<div id="googleMap" style="width:500px;height:380px;"></div>--}}
        </div>
    </div>
{{--    @include('sessoes.modal.modal-ver-cardaprio')--}}
@endforeach