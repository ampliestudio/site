@if(count($eventos)!=0)
    <hr>
    <p>Próximos eventos:</p>
        @foreach($eventos as $evento)
            <div class="row">
            <?
            $data = $evento->inicio;
            $data = strtotime($data);
            $dia = date('d', $data);
            $mes = date('m', $data);
            switch ($mes) {
                case 1:
                    $mes = "Janeiro";
                    break;
                case 2:
                    $mes = "Fevereiro";
                    break;
                case 3:
                    $mes = "Março";
                    break;
                case 4:
                    $mes = "Abril";
                    break;
                case 5:
                    $mes = "Maio";
                    break;
                case 6:
                    $mes = "Junho";
                    break;
                case 7:
                    $mes = "Julho";
                    break;
                case 8:
                    $mes = "Agosto";
                    break;
                case 9:
                    $mes = "Setembro";
                    break;
                case 10:
                    $mes = "Outubro";
                    break;
                case 11:
                    $mes = "Novembro";
                    break;
                case 12:
                    $mes = "Dezembro";
                    break;
            }
            $hora = date('H:i', $data);
            ?>
            <div class="col-xs-3">
                <h3 class="text-center">{!! $dia !!}</h3>
                <h5 class="text-center mes">{!! $mes !!}</h5>
                <h4 class="text-center">{!! $hora !!}h</h4>
            </div>
            <div class="col-xs-9">
                <br>
                <p><a href="{!! url('evento/'.$evento->slug_id) !!}">{!! $evento->nome !!}</a></p>
            </div>
    </div>
            <hr>
    @endforeach
@endif