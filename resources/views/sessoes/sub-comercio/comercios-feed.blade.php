@if(count($eventos)!=0)
    @foreach($eventos as $evento)
        <?
        $soma_rate_atual = 0;
        $sou_eu = 0;
        ?>
        @foreach($score as $rate_atual)
            @if($rate_atual->evento_id == $evento->id)
                <?
                //opa! temos mais um positivo!
                $soma_rate_atual++;
                //opa! a pessoa logada que deu esse positivo!
                if (\Illuminate\Support\Facades\Auth::id() == $rate_atual->user_id) {
                    $sou_eu = 1;
                }
                ?>
            @endif
        @endforeach
        <?
        //tradução dos meses do PHP

        $data = $evento->inicio;
        $data = strtotime($data);
        $dia = date('d', $data);
        $mes = date('m', $data);
        switch ($mes) {
        case 1:
        $mes = "Janeiro";
        break;
        case 2:
        $mes = "Fevereiro";
        break;
        case 3:
        $mes = "Março";
        break;
        case 4:
        $mes = "Abril";
        break;
        case 5:
        $mes = "Maio";
        break;
        case 6:
        $mes = "Junho";
        break;
        case 7:
        $mes = "Julho";
        break;
        case 8:
        $mes = "Agosto";
        break;
        case 9:
        $mes = "Setembro";
        break;
        case 10:
        $mes = "Outubro";
        break;
        case 11:
        $mes = "Novembro";
        break;
        case 12:
        $mes = "Dezembro";
        break;
        }
        $hora = date('H:i', $data);
        ?>
        <div class="row">
            <div class="col-xs-12 col-sm-3" id="ev-{!! $evento->slug !!}">
                <br>
                <img src="@if($evento->tipo == 2){!! $evento->user_image !!}@elseif($evento->id_local!=0){!! asset('uploads/'.$comercio->foto) !!}@else{!! asset('uploads/alias-image.png') !!}@endif"
                     class="image-bubble img-circle img-thumbnail"
                     alt="Responsive image">
            </div>
            <div class="eventos col-xs-12 col-sm-6">
                <div class=" ev-{!! $evento->id !!} bubble">

                    <h3><a href="{!! url('evento/'.$evento->slug_id) !!}">{!! $evento->nome !!}</a></h3>
                    <p><i class="fa fa-clock-o"></i> {!! $dia !!} de {!! $mes !!} às {!! $hora !!}</p>

                    {{--GPS--}}
                    {{--@if($evento->lat != 0 && $evento->lat != -23.4087)--}}
                        {{--<a style="cursor: pointer;" onclick="myNavFunc({!! $evento->lat !!},{!! $evento->lon !!})"--}}
                           {{--class="btn btn-menu-rest btn-default"><i class="fa fa-location-arrow"></i> Direções até o--}}
                            {{--local</a>--}}
                    {{--@endif--}}
                    <div class="evento-footer">
                        <a class="clickable" type="button" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <span class="glyphicon glyphicon-flag"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{!! url('tips/suporte/3/5/'.$evento->id) !!}"><span
                                            class="fa fa-pencil"></span> Editar</a>
                            </li>

                            {{-- TODO: ver se é o dono e se for status colocar remover e o editar como link para editar o post--}}
                            <li>
                                <a href="{!! url('tips/suporte/3/4/'.$evento->id) !!}"><span
                                            class="fa fa-flag"></span> Reportar </a>
                            </li>
                        </ul>
                        <div class="btn-group pull-right">
                            @if (Auth::guest())
                                <a href="{!! url('login') !!}">
                        <span id="badge-{!! $evento->id !!}"
                              class="badge badge-wait">{!! $evento->score_count !!}</span>
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            @else
                                <a class="<?= $sou_eu ? 'st-st' : '' ?>" onclick="upthis({!! $evento->id !!})">

                        <span id="badge-{!! $evento->id !!}"
                              class="badge badge-wait">{!! $evento->score_count !!}</span>

                                    <?= $sou_eu ? '<span id="marcar-' . $evento->id . '"></span>' : '<span id="marcar-' . $evento->id . '"></span>' ?>
                                    <i id="star-{!! $evento->id !!}"
                                       class="fa clickable <?= $sou_eu ? 'fa-chevron-down st-st' : 'fa-chevron-up' ?>"></i>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endforeach
    <div class="append">

    </div>
<div class="row">
    <div class="load-more col-xs-12">
        {{--<a href="{!! url('user/criar-evento') !!}" class="btn-add-comercio btn btn-success">Criar evento</a>--}}
        <button onclick="getnewpage()" class="btn-add-comercio botao-carregar-mais btn btn-success">Carregar Mais</button>
        <br><br>
    </div>
</div>

@endif
<script>
    //    call infinite scroll

    //        setTimeout(function () {
    //            $(window).scroll(function () {
    //                if ($(window).scrollTop() + $(window).height() == $(document).height()) {
    //                    getnewpage();
    //                }
    //            });
    //        }, 5000);


    //infinite scroll
    var pageNumber = 2;

    function getnewpage() {

        var articles;
        $.ajax({
            type: 'GET',
            url: '/place/{!! $comercio->id !!}/edit?page=' + pageNumber,
            success: function (data) {
                pageNumber += 1;
                if (data.length == 0) {
                    $(".botao-carregar-mais").fadeOut(function () {
                        $(this).text("Isso é tudo").fadeIn();
                    });
                } else {

                    $(".append").append(data);
                }
            }, error: function (data) {
                $(this).text("Erro de conexão").fadeIn();

            },
        })
    }
</script>