<div class="row">
    <div class=" col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
        @if(Auth::check())
            <div class="bs-callout bs-callout-info">
                <div class="media">
                    <form method="post" action="{!! url('user/comentar/') !!}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <input type="hidden" name="id" value="{!! $comercio->id !!}">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-circle" src="{!! Auth::user()->foto !!}"
                                     height="40">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="input-group">
                                <input type="text" class="form-control"
                                       placeholder="Comentar" name="avaliacao">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="fa fa-chevron-down"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @else
            <div class="bs-callout bs-callout-info">
                <div class="media">

                    <div class="media-body">
                        <div class="row">

                            <div class="col-md-10 col-md-offset-1 col-sm-12">
                                <a href="{!! url('login') !!}"
                                   class="btn btn-lg btn-social btn-block btn-facebook">
                                    <span class="fa fa-facebook"></span> Entre para comentar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @foreach($avaliacoes as $avaliacao)
            <div class="bs-callout bs-callout-info">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object img-circle" src="{!! $avaliacao->userimg !!}"
                                 height="40">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            @if($avaliacao->nota == 1)
                                Ótimo <i class="fa fa-star"></i>
                            @elseif($avaliacao->nota == 2)
                                Regular <i class="fa fa-star-half-full"></i>
                            @else
                                Péssimo <i class="fa fa-star-o"></i>
                            @endif
                            <small>{!! $avaliacao->username !!}</small>
                            @if(Auth::id() == $avaliacao->user_id)
                                <a href="{!! url('user/excluir-comentario/'.$comercio->id) !!}"
                                   class="pull-right"><i class="fa fa-trash">&nbsp;</i></a>
                            @else
                                <a href="{!! url('user/abuso-avaliacao/'.$avaliacao->id) !!}"
                                   class="pull-right"><i class="fa fa-flag" data-toggle="tooltip"
                                                         data-placement="left"
                                                         title="Reportar impróprio"></i></a>
                            @endif
                        </h4>
                        <div>
                            {!! $avaliacao->avaliacao !!}
                            <br>
                            <?
                            $data = $avaliacao->updated_at;
                            $data = strtotime($data);
                            $data = date('d/m/Y', $data);
                            ?>
                            <small style="color: gray">{!! $data !!}</small>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        {{ $avaliacoes->links() }}
        @if(count($avaliacoes) == 0)

        @endif
    </div>
</div>