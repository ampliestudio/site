@extends('layouts.app')
@section('meta')
    <meta name="description" content="Descubra e compartilhe o seu rolê em {!! $cidade->nome !!} com CityTips">
    <meta name="keyowrds"
          content="{!! $cidade->nome !!}, city tips, citytips, bar, pub, balada, eventos, eventos em {!! $cidade->nome !!}, balada em {!! $cidade->nome !!}, festa em {!! $cidade->nome !!}">
    <meta name="robots" content="index, follow, nosnippet">
    <meta property="og:url" content="{!! Request::url() !!}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="O que tem pra hj em {!! $cidade->nome !!}?!"/>
    <meta property="og:description"
          content="Descubra e compartilhe o seu rolê com CityTips"/>
    <meta property="og:image" content="{!! asset('images/wall-2.png') !!}"/>
    <meta property="fb:app_id" content="{!! Config::get('constants.FBAPPID') !!}"/>
    {{--script que verifica o login--}}
    <script src="{{ asset('js/home-cidade.js') }}"></script>

@endsection

@section('menu-left')
    {{--incluir modal em content -- modal selecionar cidade--}}
    @include('layouts.nav.menu-left-pagina-cidade')
@endsection

@section('content')
    {{--modal de menu-left -- modal selecionar cidade--}}
    @include('sessoes.modal.modal-mudar-cidade')
    @include('sessoes.modal.modal-fazer-login')

    <div class="container">

        <ul class="nav nav-tabs" role="tablist">
            <li><a class="btn btn-success" href="{!! url($cidade->slug) !!}">Ver eventos</a></li>
            {{--<li><a href="{!! url('user/criar-evento-novo/'.$cidade->id) !!}">Criar evento</a></li>--}}
            <li><a>Filtrar por:</a></li>
            <br class="visible-xs">
            <li><a onclick="filtrodetipo(1)" class="btn btn-success btn-sm"><i class="fa fa-cutlery"><span class="hidden-xs">&nbsp; Refeição</span></i></a></li>
            <li><a onclick="filtrodetipo(2)" class="btn btn-success btn-sm"><i class="fa fa-glass"><span class="hidden-xs">&nbsp; Happy Hour</span></i></a></li>
            <li><a onclick="filtrodetipo(3)" class="btn btn-success btn-sm"><i class="fa fa-music"><span class="hidden-xs">&nbsp; Diversão</span></i></a></li>
            <li><a onclick="filtrodetipo(4)" class="btn btn-success btn-sm"><i class="fa fa-transgender-alt"><span class="hidden-xs">&nbsp; LGBT Friendly</span></i></a></li>
            <li><a onclick="filtrodetipo(0)" class="btn btn-success btn-sm"><i class="fa fa-remove" data-toggle="tooltip" data-placement="bottom"
                                                                               title="Remover filtro"></i></a></li>
            <div id="filter-style">

            </div>
        </ul>
        <br>
        {{--lista de eventos da cidade--}}
        @include('sessoes.sub-cidade-home.comercios')

    </div>

    {{--icone para voltar ao topo--}}
    <a href="#0" class="cd-top">Top</a>

    <script>


        @if(Auth::guest())
        $(document).ready(function () {

            setTimeout(function () {
                $('#modalLogin').modal('show');
                $('#modalLogin').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            }, 5000);

        });
        @endif


    </script>

    @include('layouts.footer')
@endsection
