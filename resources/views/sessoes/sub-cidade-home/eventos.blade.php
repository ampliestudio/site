<?
//o código a seguir separa os eventos seguintes por data
$ultima_data = 0;
$exibiu_passados = 0;
$ultimo_local = 0;
$ultimo_dia = 0;
$ultimo_local_slug = 0;
$ultimo_local_nome = 0;
$mesmo_local_count = 0;
?>

<div class="col-sm-8 col-sm-offset-2">
{{--    @include('sessoes.sub-cidade-home.carousel')--}}
    <table id="eventos" class="table">
        <tbody>
        {{--<tr>--}}
            {{--<td colspan="3"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
                {{--<!-- citytips -->--}}
                {{--<ins class="adsbygoogle"--}}
                     {{--style="display:block"--}}
                     {{--data-ad-client="ca-pub-1291546981046674"--}}
                     {{--data-ad-slot="9879717207"--}}
                     {{--data-ad-format="auto"></ins>--}}
                {{--<script>--}}
                    {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
                {{--</script>--}}
            {{--</td>--}}
        {{--</tr>--}}
        @if(count($eventos)==0)
            <tr class="data-divisao">
                <td colspan="3">
                    <p class="text-center">Poxa, nenhum evento..</p>
                </td>
            </tr>
            <tr class="data-divisao">
                <td colspan="3">
                    <p class="text-center">Mas você pode criar um!</p>
                </td>
            </tr>
            <tr class="data-divisao">
                <td colspan="3">
                    <p class="text-center"><a href="{!! url('user/criar-evento-novo/'.$cidade->id) !!}"><i
                                    class="fa fa-chevron-right"></i> Criar evento</a></p>
                </td>
            </tr>

        @else
            @foreach($eventos as $evento)
                <?
                $soma_rate_atual = 0;
                $sou_eu = 0;
                ?>
                @foreach($score as $rate_atual)
                    @if($rate_atual->evento_id == $evento->id)
                        <?
                        //opa! temos mais um positivo!
                        $soma_rate_atual++;
                        //opa! a pessoa logada que deu esse positivo!
                        if (\Illuminate\Support\Facades\Auth::id() == $rate_atual->user_id) {
                            $sou_eu = 1;
                        }
                        ?>
                    @endif
                @endforeach

                <?
                //continuação do cod que separa os eventos seguintes por data

                date_default_timezone_set('America/Sao_Paulo');
                Carbon\Carbon::setLocale('pt_BR');
                $data = $evento->inicio;
                $data = strtotime($data);
                $dia = date('d', $data);
                $mes = date('m', $data);
                $diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado');
                $diasemana_numero = date('w', $data);
                $hoje = \Carbon\Carbon::today();
                $data_limite = $hoje->addDays(7);
                $hoje = \Carbon\Carbon::today();
                if ($ultima_data != $dia) {
                    $ultima_data = $dia;
                    if ($evento->inicio >= $hoje && $evento->inicio < $data_limite) {
                        echo '<tr class="data-divisao"><td colspan="3">
<p class="text-center">' . $diasemana[$diasemana_numero] . '</p></td></tr>';
                    } elseif ($evento->inicio < $hoje && $exibiu_passados == 0) {
                        $exibiu_passados = 1;
                        echo '
        <tr>
            <td colspan="3"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- citytips -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-1291546981046674"
                     data-ad-slot="9879717207"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </td>
        </tr>

<tr class="data-divisao"><td colspan="3">
<p class="text-center">Eventos passados</p></td></tr>';
                    } elseif ($evento->inicio >= $data_limite) {
                        echo '<tr class="data-divisao"><td colspan="3">
<p class="text-center">' . $diasemana[$diasemana_numero] . " " . $dia . '/' . $mes . '</td></tr>';
                    }
                }
                date_default_timezone_set('America/Sao_Paulo');
                $data = $evento->inicio;
                $data = strtotime($data);
                $dia = date('d', $data);
                $mes = date('m', $data);
                $hora = date('H:i', $data);
                ?>
                <tr>
                    <td>
                        @if(isset($evento->local_foto))
                            <img src="{!! asset('uploads/'. $evento->local_foto) !!}"
                                 class="local-foto"
                                 alt="{!! $evento->local_nome !!}">
                        @else
                            <img src="{!! asset('uploads/alias-image.png') !!}"
                                 class="local-foto"
                                 alt="{!! $evento->local_nome !!}">
                        @endif
                    </td>
                    <td>
                        <p>
                            &nbsp; <a href="{!! url('evento/'.$evento->slug_id) !!}">{!! $evento->nome !!}</a>
                        </p>
                        <p>
                            @if($evento->local_slug )
                            &nbsp; <a class="btn-success btn btn-xs" href="{!! url('place/'.$evento->local_slug) !!}">{!! $evento->local_nome !!}</a>
                            @endif
                            &nbsp; <a class="btn-success btn btn-xs" href="{!! url('evento/'.$evento->slug_id.'#comentarios') !!}"><span class="fb-comments-count" data-href="{!! url('evento/'.$evento->slug_id) !!}">&nbsp; </span> <i class="fa fa-comment"> </i></a>
                            <span class="pull-right">{!! $hora !!}h</span>

                        </p>
                    </td>
                    <td>
                        <div class="btn-group up-lista-evento">
                            @if (Auth::guest())
                                <a href="{!! url('auth/facebook') !!}">
                        <span id="badge-{!! $evento->id !!}"
                              class="badge clickable badge-score">{!! $evento->score_count !!}</span>
                                    <i class="st-st-chevron fa st-st-up clickable fa-chevron-up"></i>
                                </a>
                            @else
                                <a class="<?= $sou_eu ? 'st-st-down' : 'st-st-up' ?>"
                                   onclick="upthis({!! $evento->id !!})">

                        <span id="badge-{!! $evento->id !!}"
                              class="badge clickable <?= $sou_eu ? 'badge-score-up' : 'badge-score' ?>">{!! $evento->score_count !!}</span>

                                    <?= $sou_eu ? '<span id="marcar-' . $evento->id . '"></span>' : '<span id="marcar-' . $evento->id . '"></span>' ?>
                                    <i id="star-{!! $evento->id !!}"
                                       class="st-st-chevron fa clickable <?= $sou_eu ? 'fa-chevron-down st-st-down' : 'fa-chevron-up' ?>"></i>
                                </a>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        <tr>
            <td colspan="3">
                <a href="{!! url('eventos-antigos/'.$cidade->slug) !!}">Ver mais eventos antigos</a>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="col-xs-12">
        <form action="{!! url('tips/falta-algo') !!}" method="post">
            <div class="row">
                <div class="col-sm-6">
                    <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                    <input type="text" id="texto-falta" placeholder="Falta algo aqui? Algo está errado?" name="texto"
                           class="form-control">
                </div>
                <br class="visible-xs">
                <div id="aparecer-focus" class="col-sm-4 sr-only">
                    <input type="text" id="texto-falta" placeholder="E-mail"
                           value="<?= \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->email : '' ?>"
                           name="email"
                           class="form-control">
                </div>
                <br class="visible-xs">
                <div class="col-sm-2">
                    <input type="submit" value="Enviar" class="btn btn-success">
                </div>
            </div>
        </form>
        <br>
    </div>
</div>
