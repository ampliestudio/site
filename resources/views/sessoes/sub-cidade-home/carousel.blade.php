<div id="carousel-posts-de-locais" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-posts-de-locais" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="1"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="2"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="3"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="4"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="5"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="6"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="7"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="8"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="9"></li>
        <li data-target="#carousel-posts-de-locais" data-slide-to="10"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <? $count = 0 ?>
        @foreach($carousel as $item_carousel)
            <div class="item @if($count == 0) active @endif">
                <div class="carousel-img" style="background-image: url('{!! $item_carousel->full_picture !!}')"></div>
{{--                <img src="{!! $item_carousel->full_picture !!}" alt="...">--}}
                <div class="carousel-caption">
                    <p><a class="carousel-link" href="{!! url('place/'.$item_carousel->slug) !!}">{!! $item_carousel->nome !!}</a></p>
                </div>
            </div>
            <? $count++ ?>
        @endforeach
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-posts-de-locais" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-posts-de-locais" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>