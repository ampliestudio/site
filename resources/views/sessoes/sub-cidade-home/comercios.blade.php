<div class="col-sm-8 col-sm-offset-2">
    <table id="eventos" class="table">
        <tbody>
        <tr>
            <td colspan="3">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- citytips -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-1291546981046674"
                     data-ad-slot="9879717207"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </td>
        </tr>
        @foreach($comercios as $comercio)
            <tr class="tipo-{!! $comercio->tipo !!}">
                <td>
                    <img src="{!! asset('uploads/'.$comercio->foto) !!}"
                         class="local-foto"
                         alt="{!! $comercio->nome !!}">

                </td>
                <td>
                    <p>
                        &nbsp; <a href="{!! url('/place/'.$comercio->slug) !!}">{!! $comercio->nome !!}</a>
                    </p>
                    <p>
                        @if($comercio->lat != 0)&nbsp; <a style="cursor: pointer;" class="btn btn-xs btn-success"
                                                          onclick="myNavFunc({!! $comercio->lat !!},{!! $comercio->lon !!})"><i
                                    class="fa fa-location-arrow"></i> Direções</a>
                        @endif
                        @if(count(\CityTips\Eventos::where('id_local',$comercio->id)->whereDate('inicio','>=',Carbon\Carbon::today())->get()) > 0)
                            <a class="btn-success btn btn-xs hidden-xs"
                               href="{!! url('/place/'.$comercio->slug) !!}">{!! count(\CityTips\Eventos::where('id_local',$comercio->id)->whereDate('inicio','>=',Carbon\Carbon::today())->get()) !!}
                                eventos</a>
                        @endif
                        <a class="btn-success btn btn-xs hidden-xs" href="{!! url('/place/'.$comercio->slug) !!}">Ver
                            mais</a>
                        <span class="pull-right">
                                <? $tipos = str_split($comercio->tipo) ?>
                            @foreach($tipos as $tipo)
                                @if($tipo == 1)
                                    <i class="fa fa-cutlery" data-toggle="tooltip" data-placement="top"
                                       title="Serve Refeições"></i>&nbsp;
                                @elseif($tipo == 2)
                                    <i class="fa fa-glass" data-toggle="tooltip" data-placement="top"
                                       title="Happy-Hour"></i>&nbsp;
                                @elseif($tipo == 3)
                                    <i class="fa fa-music" data-toggle="tooltip" data-placement="top"
                                       title="Local com pista de dança / Balada"></i>&nbsp;
                                @elseif($tipo == 4)
                                    <i class="fa fa-transgender-alt" data-toggle="tooltip" data-placement="top"
                                       title="LGBT Friendly"></i>&nbsp;
                                @endif
                            @endforeach
                        </span>
                    </p>
                </td>
                <td>
                    <div class="btn-group up-lista-evento">
                        @if (Auth::guest())
                            <a href="{!! url('auth/facebook') !!}">
                        <span id="badge-place-{!! $comercio->id !!}"
                              class="badge clickable badge-score">{!! $comercio->rate_count !!}</span>
                                <i class="st-st-chevron fa st-st-up clickable fa-chevron-up"></i>
                            </a>
                        @else
                            <? $sou_eu = 0;
                            $select = \CityTips\ComerciosRate::where('comercio_id', $comercio->id)->where('user_id', Auth::id())->first();
                            if (count($select) != 0) {
                                $sou_eu = $select->recomendado;
                            }?>
                            <a class="<?= $sou_eu ? 'st-st-down' : 'st-st-up' ?>"
                               onclick="upthiscomercio({!! $comercio->id !!})">

                        <span id="badge-place-{!! $comercio->id !!}"
                              class="badge clickable <?= $sou_eu ? 'badge-score-up' : 'badge-score' ?>">{!! $comercio->rate_count !!}</span>

                                <?= $sou_eu ? '<span id="marcar-' . $comercio->id . '"></span>' : '<span id="marcar-' . $comercio->id . '"></span>' ?>
                                <i id="star-{!! $comercio->id !!}"
                                   class="st-st-chevron fa clickable <?= $sou_eu ? 'fa-chevron-down st-st-down' : 'fa-chevron-up' ?>"></i>
                            </a>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
        <tr>
            <td colspan="3">
                <i class="fa fa-cutlery" style="color: white"> = Serve Refeições</i>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <i class="fa fa-glass" style="color: white"> = Happy-Hour</i>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <i class="fa fa-music" style="color: white"> = Local com pista de dança / Balada</i>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <i class="fa fa-transgender-alt" style="color: white"> = LGBT Friendly</i>&nbsp;
            </td>
        </tr>
        </tbody>
    </table>
    <div class="col-xs-12">
        <form action="{!! url('tips/falta-algo') !!}" method="post">
            <div class="row">
                <div class="col-sm-6">
                    <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                    <input type="text" id="texto-falta" placeholder="Falta aqui? Algo está errado?" name="texto"
                           class="form-control">
                </div>
                <br class="visible-xs">
                <div id="aparecer-focus" class="col-sm-4 sr-only">
                    <input type="text" id="texto-falta" placeholder="E-mail"
                           value="<?= \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->email : '' ?>"
                           name="email"
                           class="form-control">
                </div>
                <br class="visible-xs">
                <div class="col-sm-2">
                    <input type="submit" value="Enviar" class="btn btn-success">
                </div>
            </div>
        </form>
        <br>
    </div>
</div>


