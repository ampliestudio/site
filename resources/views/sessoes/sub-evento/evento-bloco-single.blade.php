<?
$soma_rate_atual = 0;
$sou_eu = 0;
?>
@foreach($scorelist as $rate_atual)
    @if($rate_atual->evento_id == $evento->id)
        <?
        //opa! temos mais um positivo!
        $soma_rate_atual++;
        //opa! a pessoa logada que deu esse positivo!
        if (\Illuminate\Support\Facades\Auth::id() == $rate_atual->user_id) {
            $sou_eu = 1;
        }
        ?>
    @endif
@endforeach


<?
//tradução dos meses do PHP

$data = $item->inicio;
$data = strtotime($data);
$dia = date('d', $data);
$mes = date('m', $data);
        $diasemana = array('Domingo','Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');
        $diasemana_numero = date('w', $data);
switch ($mes) {
    case 1:
        $mes = "Janeiro";
        break;
    case 2:
        $mes = "Fevereiro";
        break;
    case 3:
        $mes = "Março";
        break;
    case 4:
        $mes = "Abril";
        break;
    case 5:
        $mes = "Maio";
        break;
    case 6:
        $mes = "Junho";
        break;
    case 7:
        $mes = "Julho";
        break;
    case 8:
        $mes = "Agosto";
        break;
    case 9:
        $mes = "Setembro";
        break;
    case 10:
        $mes = "Outubro";
        break;
    case 11:
        $mes = "Novembro";
        break;
    case 12:
        $mes = "Dezembro";
        break;
}
$hora = date('H:i', $data);
?>
<div class="row">
    <div class="col-xs-12 col-sm-3">
        <br>
        <img src="@if($item->tipo == 2){!! $item->user_image !!}@elseif($item->id_local!=0){!! asset('uploads/'.$item->local_foto) !!}@else{!! asset('uploads/alias-image.png') !!}@endif"
             class="image-bubble img-circle img-thumbnail"
             alt="Responsive image">
    </div>
    <div class="eventos col-xs-12 col-sm-7">
        <div class=" ev-{!! $item->id !!} bubble">

            <h3><a href="{!! url('evento/'.$item->slug_id) !!}">{!! $item->nome !!}</a>@if($item->tipo == 2) <small>{!! $item->user_name !!}</small> @endif</h3>
            @if($evento->id_local != 0)
                <p class="card-body-item"><i
                            class="fa fa-map-marker"></i> <a
                            href="{!! url('/place/'.$item->local_slug) !!}">{!! $item->local_nome !!}</a></p>
            @else
                <p class="card-body-item"><i
                            class="fa fa-map-marker"></i> {!! $item->local !!}</p>
            @endif
            <p><i class="fa fa-clock-o"></i> <span style="color: #ff9252; font-weight: 600">{!! $diasemana[$diasemana_numero] !!}</span>, {!! $dia !!} de {!! $mes !!} às {!! $hora !!}</p>

            {{--GPS--}}
            @if($item->lat != 0 && $item->lat != -23.4087)
                <a style="cursor: pointer;" onclick="myNavFunc({!! $item->lat !!},{!! $item->lon !!})"
                   class="btn btn-menu-rest btn-default"><i class="fa fa-location-arrow"></i> Direções até o
                    local</a>
            @endif
            <div class="evento-footer">
                <a class="clickable" type="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <span class="glyphicon glyphicon-option-horizontal"></span>
                </a>
                <ul class="dropdown-menu">
                    @if(Auth::id() == $item->user_id)
                        <li>
                            <a class="clickable" type="button" data-toggle="modal" data-target="#modal-del{!! $item->id !!}"><span
                                        class="fa fa-times"></span> Excluir</a>
                        </li>
                    @else
                        <li>
                            <a class="clickable" href="{!! url('tips/suporte/3/5/'.$item->id) !!}"><span
                                        class="fa fa-pencil"></span> Editar</a>
                        </li>
                    @endif

                    <li>
                        <a href="{!! url('tips/suporte/3/4/'.$item->id) !!}"><span
                                    class="fa fa-flag"></span> Reportar </a>
                    </li>
                </ul>
                @include('sessoes.modal.modal-excluir-status')

                <div class="btn-group pull-right">
                    @if (Auth::guest())
                        <a href="{!! url('login') !!}">
                        <span id="badge-{!! $item->id !!}"
                              class="badge badge-wait">{!! $item->score_count !!}</span>
                             <i class="fa fa-chevron-up"></i>
                        </a>
                    @else
                        <a class="<?= $sou_eu ? 'st-st' : '' ?>" onclick="upthis({!! $item->id !!})">

                        <span id="badge-{!! $item->id !!}"
                              class="badge badge-wait">{!! $item->score_count !!}</span>

                            <?= $sou_eu ? '<span id="marcar-' . $item->id . '"></span>' : '<span id="marcar-' . $item->id . '"></span>' ?>
                            <i id="star-{!! $item->id !!}"
                               class="fa clickable <?= $sou_eu ? 'fa-chevron-down st-st' : 'fa-chevron-up' ?>"></i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>

