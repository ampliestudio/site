@if(Session::has('flash_message'))
    <div class="alert alert-warning"><span
                class="glyphicon glyphicon-alert"></span> {!! session('flash_message') !!}<a href="#"
                                                                                             class="close"
                                                                                             data-dismiss="alert"
                                                                                             aria-label="close">&times;</a>
    </div>
@endif
<div class="row">
    <div class="col-xs-12 col-sm-3">

        <img class="image-bubble status-box-image img-circle img-thumbnail" src="{!! Auth::user()->foto !!}"
             height="40">
    </div>
    <div class="col-xs-12 col-sm-7">
        <div class="bubble status-box">

            <form method="post" action="{!! url('user/status-novo/') !!}">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <input type="hidden" name="id" value="">

                <div class="input-box">
                                        <textarea class="form-control"
                                                  placeholder="O que tem pra hoje? Compartilhe aqui suas dicas pra {!! $cidade->nome !!} e ajude alguém encontrar um lugar para sair!" name="status"
                                                  id="statusbox"
                                                  cols="3">@if((session()->has('status'))){!! session('status') !!}@endif</textarea>
                    <div class="local-hid">
                        <input type="text" autocomplete="off" id="autocomplete" name="local" placeholder="Onde?"
                               class="local-status-box">
                    </div>
                    <input type="hidden" class="local-id" value="0" name="localid">
                    <input type="hidden" class="datetimetrue" value="0" name="datetimetrue">
                    <input type="hidden" value="{!! $cidade->id !!}" name="cidade">
                    <div id="vinculo-val"></div>

                </div>
                <button type="button" onclick="datadestatus()" class="btn btn-default btn-sm"
                        data-toggle="tooltip"
                        data-placement="bottom" title="Adicionar data e hora"><i class="fa fa-calendar-o"></i>
                </button>

                {{--llllllllll--}}
                <div class="form-group status-date">
                    <div class='input-group date' id='datetimepicker1' data-action="DateTimePicker">
                        <input placeholder="25/09/2016 18:00" type='hidden' class="form-control" name="inicio"/>
                        {{--<span class="input-group-addon">--}}
                        {{--<span class="glyphicon glyphicon-calendar"></span>--}}
                        {{--</span>--}}
                    </div>
                    {{--lllllll--}}

                </div>

                <input type="submit" class="btn btn-sm btn-action status-send-button" value="Publicar">
                <div id="input-vinc-st">
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        var locais = [
                @foreach($lugares as $item)
            {
                value: "{!! $item->nome !!}", data: "{!! $item->id !!}"
            },
            @endforeach
        ];

        $('#autocomplete').autocomplete({
            lookup: locais,
            onSelect: function (suggestion) {
                $('.local-id').val(suggestion.data);
                $('#vinculo-val').append('<input style="width: 80%" disabled value="' + suggestion.value + '" > <button type="button" class="btn-xs btn btn-default" onclick="removervinc()"><i class="fa fa-remove"></i></button> </input>');
                $('#input-vinc-st').empty();
                $('#input-vinc-st').append('<style> #autocomplete{display: none;}.local-hid{display: none}</style>');
            }
        });
    });


    function removervinc() {
        $('#input-vinc-st').empty();
        $('#input-vinc-st').append('<style> #autocomplete{display: block;}.cidade-hid{display: block}</style>');
        $('#vinculo-val').empty();
        $('#autocomplete').val('');
        $('.local-id').val('0');
    }


    //date time piker
    $(document).ready(function () {
        $(document).ready(function () {
            $('#datetimepicker1').datetimepicker({
                inline: true,
                sideBySide: true,
                locale: 'pt-br',
                format: 'YYYY-MM-DD HH:mm'
            });
        });
    });
    $(document).ready(function () {
        $(".status-date").addClass('sr-only')
    });
    var isactive = false;


    function datadestatus() {
        if (isactive == true) {
            $(".status-date").addClass('sr-only');
            $(".datetimetrue").val(0);
            isactive = false;
        } else {
            $(".status-date").removeClass('sr-only')
            $(".datetimetrue").val(1);
            isactive = true;
        }

    }


</script>