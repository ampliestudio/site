<?
//o código a seguir separa os eventos seguintes por data
$ultima_data = 0;
$exibiu_passados = 0;
$ultimo_local = 0;
$ultimo_dia = 0;
$ultimo_local_slug = 0;
$ultimo_local_nome = 0;
$mesmo_local_count = 0;
?>
@foreach($eventos as $evento)
    <?php
    //verifica eventos repetidos do mesmo lugar
    $dia_cru = strtotime($evento->inicio);
    $dia = date('d', $dia_cru);
    if ($ultimo_dia == $dia) {
        if ($ultimo_local == $evento->id_local) {
            $mesmo_local_count++;
            $ultimo_dia = $dia;
            $ultimo_local = $evento->id_local;
            $ultimo_local_slug = $evento->local_slug;
            $ultimo_local_nome = $evento->local_nome;
            continue;
        }
    }
    if ($mesmo_local_count > 0) {
        if ($ultimo_local != $evento->id_local || $ultimo_dia != $dia) {
            echo '<div class="row"> <div class="col-sm-4 col-xs-11 col-xs-offset-1 col-sm-offset-4"><a href="' . url("place/" . $ultimo_local_slug) . '" style="color:white">Ver mais ' . $mesmo_local_count . ' de ' . $ultimo_local_nome . '...</a></div></div>';
            $mesmo_local_count = 0;
        }
    }
    $dia_cru = strtotime($evento->inicio);
    $dia = date('d', $dia_cru);
    $ultimo_dia = $dia;
    $ultimo_local = $evento->id_local;

    //continuação do cod que separa os eventos seguintes por data

    date_default_timezone_set('America/Sao_Paulo');
    Carbon\Carbon::setLocale('pt_BR');
    $data = $evento->inicio;
    $data = strtotime($data);
    $dia = date('d', $data);
    $mes = date('m', $data);
    $diasemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');
    $diasemana_numero = date('w', $data);
    $hoje = \Carbon\Carbon::today();
    $data_limite = $hoje->addDays(7);
    $hoje = \Carbon\Carbon::today();
    if($ultima_data != $dia){
        echo $ultima_data;
        $ultima_data = $dia;
        echo $ultima_data;
        if($evento->inicio >= $hoje && $evento->inicio < $data_limite){
            echo '    <div class="col-xs-10 col-xs-offset-1">
<p class="data-divisao">'.$diasemana[$diasemana_numero].'</p></div>';
        }elseif($evento->inicio < $hoje && $exibiu_passados == 0){
            $exibiu_passados = 1;
            echo '    <div class="col-xs-10 col-xs-offset-1">
<p class="data-divisao">Eventos passados</p></div>';
        }
        elseif($evento->inicio >= $data_limite){
            echo '    <div class="col-xs-10 col-xs-offset-1">
<p class="data-divisao">'.$dia.'/'.$mes.'</p></div>';
        }
    }
    ?>
    <?
    $soma_rate_atual = 0;
    $sou_eu = 0;
    ?>
    @foreach($score as $rate_atual)
        @if($rate_atual->evento_id == $evento->id)
            <?
            //opa! temos mais um positivo!
            $soma_rate_atual++;
            //opa! a pessoa logada que deu esse positivo!
            if (\Illuminate\Support\Facades\Auth::id() == $rate_atual->user_id) {
                $sou_eu = 1;
            }
            ?>
        @endif
    @endforeach
    <?
    //tradução dos meses do PHP

    $data = $evento->inicio;
    $data = strtotime($data);
    $dia = date('d', $data);
    $mes = date('m', $data);
    $diasemana = array('Domingo','Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado');
    $diasemana_numero = date('w', $data);
    switch ($mes) {
        case 1:
            $mes = "Janeiro";
            break;
        case 2:
            $mes = "Fevereiro";
            break;
        case 3:
            $mes = "Março";
            break;
        case 4:
            $mes = "Abril";
            break;
        case 5:
            $mes = "Maio";
            break;
        case 6:
            $mes = "Junho";
            break;
        case 7:
            $mes = "Julho";
            break;
        case 8:
            $mes = "Agosto";
            break;
        case 9:
            $mes = "Setembro";
            break;
        case 10:
            $mes = "Outubro";
            break;
        case 11:
            $mes = "Novembro";
            break;
        case 12:
            $mes = "Dezembro";
            break;
    }
    $hora = date('H:i', $data);
    ?>

    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <br>
            <img src="@if($evento->tipo == 2){!! $evento->user_image !!}@elseif($evento->id_local!=0){!! asset('uploads/'.$evento->local_foto) !!}@else{!! asset('uploads/alias-image.png') !!}@endif"
                 class="image-bubble img-circle img-thumbnail"
                 alt="Responsive image">
        </div>
        <div class="eventos col-xs-12 col-sm-7">
            <div class=" ev-{!! $evento->id !!} bubble">

                <h3><a href="{!! url('evento/'.$evento->slug_id) !!}">{!! $evento->nome !!}</a>@if($evento->tipo == 2) <small>{!! $evento->user_name !!}</small> @endif</h3>
                @if($evento->id_local != 0)
                    <p class="card-body-item"><i
                                class="fa fa-map-marker"></i> <a
                                href="{!! url('/place/'.$evento->local_slug) !!}">{!! $evento->local_nome !!}</a></p>
                @else
                    <p class="card-body-item"><i
                                class="fa fa-map-marker"></i> {!! $evento->local !!}</p>
                @endif
                <p><i class="fa fa-clock-o"></i> <span style="color: #ff9252; font-weight: 600">{!! $diasemana[$diasemana_numero] !!}</span>, {!! $dia !!} de {!! $mes !!} às {!! $hora !!}</p>

                {{--GPS--}}
                @if($evento->lat != 0 && $evento->lat != -23.4087)
                    <a style="cursor: pointer;" onclick="myNavFunc({!! $evento->lat !!},{!! $evento->lon !!})"
                       class="btn btn-menu-rest btn-default"><i class="fa fa-location-arrow"></i> Direções até o
                        local</a>
                @endif
                <div class="evento-footer">
                    <a class="clickable" type="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <span class="glyphicon glyphicon-option-horizontal"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @if(Auth::id() == $evento->user_id)
                            <li>
                                <a class="clickable" type="button" data-toggle="modal" data-target="#modal-del{!! $evento->id !!}"><span
                                            class="fa fa-times"></span> Excluir</a>
                            </li>
                        @else
                            <li>
                                <a class="clickable" href="{!! url('tips/suporte/3/5/'.$evento->id) !!}"><span
                                            class="fa fa-pencil"></span> Editar</a>
                            </li>
                        @endif

                        <li>
                            <a href="{!! url('tips/suporte/3/4/'.$evento->id) !!}"><span
                                        class="fa fa-flag"></span> Reportar </a>
                        </li>
                    </ul>
                    @include('sessoes.modal.modal-excluir-status')
                    <div class="btn-group pull-right">
                        @if (Auth::guest())
                            <a href="{!! url('login') !!}">
                        <span id="badge-{!! $evento->id !!}"
                              class="badge badge-wait">{!! $evento->score_count !!}</span>
                                <span>Marcar</span> <i class="fa fa-star-o"></i>
                            </a>
                        @else
                            <a class="<?= $sou_eu ? 'st-st' : '' ?>" onclick="upthis({!! $evento->id !!})">

                        <span id="badge-{!! $evento->id !!}"
                              class="badge badge-wait">{!! $evento->score_count !!}</span>

                                <?= $sou_eu ? '<span id="marcar-' . $evento->id . '"></span>' : '<span id="marcar-' . $evento->id . '"></span>' ?>
                                <i id="star-{!! $evento->id !!}"
                                   class="fa clickable <?= $sou_eu ? 'fa-chevron-down st-st' : 'fa-chevron-up' ?>"></i>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach


