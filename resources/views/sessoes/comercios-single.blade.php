@extends('layouts.app')
@section('meta')
    <meta name="{!! $comercio->nome !!} - CityTips! ">
    <meta about="Um site que reune o melhor do lazer de cada cidade, descubra restaurantes, bares, clubes, entre outros lugares de uma forma simples e atualizada">
    <meta property="og:url" content="{!! Request::url() !!}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{!! $comercio->nome !!} - CityTips! "/>
    <meta property="og:description"
          content="Reunindo o melhor do lazer de cada cidade, descubra restaurantes, bares, clubes, entre outros lugares de uma forma simples e atualizada"/>
    <meta property="og:image" content="{!! asset('uploads/'.$comercio->foto) !!}"/>
@endsection

@section('menu-left')
    {{--incluir modal em content--}}
    @include('layouts.nav.menu-left-pagina-cidade')
@endsection

@section('content')
    {{--Modal--}}
    @include('sessoes.modal.modal-mudar-cidade')

    <?$sou_eu = 0;?>
    @foreach($rate as $rate_atual)
        <?//opa! a pessoa logada que deu esse positivo!
        if (\Illuminate\Support\Facades\Auth::id() == $rate_atual->user_id) {
            $sou_eu = 1;
        }
        ?>
    @endforeach
    <div class="container-fluid">
        @if(Session::has('flash_message'))
            <div class="row">
                <div class=" col-sm-8 col-sm-offset-3">
                    <div class="alert alert-danger"><span
                                class="glyphicon glyphicon-exclamation-sign"></span> {!! session('flash_message') !!}<a
                                href="#"
                                class="close"
                                data-dismiss="alert"
                                aria-label="close">&times;</a>
                    </div>
                </div>
            </div>
        @endif
        @if(Session::has('flash_message_update'))
            <div class="row">
                <div class=" col-sm-8 col-sm-offset-3">
                    <div class="alert alert-success"><span
                                class="glyphicon glyphicon-ok"></span> {!! session('flash_message_update') !!}<a
                                href="#"
                                class="close"
                                data-dismiss="alert"
                                aria-label="close">&times;</a>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 ">

                &nbsp;<a href="{!! url('places/'.$cidade->slug) !!}" style="color: white"><i class="fa fa-chevron-circle-left"> &nbsp;Ver todos
                        de {!! $cidade->nome !!}</i></a>
            </div>
        </div>
        <div class="row">
            <div class="comercios col-sm-8 col-sm-offset-2 "
                 id="com-{!! $comercio->id !!}">
                {{--<div class="ribbon"><span class="fa fa-thumbs-o-up"> {!! $comercio->rate_count !!}</span></div>--}}
                <div class="bs-callout bs-callout-info">
                    {{--header card--}}
                    @if(isset($capa))
                        {{--<div class="capa-comercio-single" style="background-image: url('{!! $capa !!}')">--}}
                        {{--</div>--}}
                        <img src="{!! $capa !!}" class="img-responsive capa-comercio-single" alt="">
                    @endif
                    <h3>
                        <a href="{!! url('/place/'.$comercio->slug) !!}">{!! $comercio->nome !!}</a><?= $comercio->oficial ? ' <small><span class="oficial-icon fa fa-check-square-o" data-toggle="tooltip" data-placement="top" title="Lugares com o simbolo de verificado são atualizados pelo próprio estabelecimento"></span></small>' : '' ?>
                    </h3>
                    <span class="pull-right badge badge-titulo badge-success recomendado-icone"><i
                                class="fa fa-thumbs-o-up"> <span
                                    id="badge-place-{!! $comercio->id !!}">{!! $comercio->rate_count !!}</span></i></span>
                    {{--body card--}}
                    <div class="row">
                        {{--image card--}}
                        <div class="col-xs-4 fix-image-card">
                            <div class="row">
                                <img src="{!! asset('uploads/'.$comercio->foto) !!}"
                                     class="img-responsive img-thumbnail"
                                     alt="Responsive image">
                            </div>
                            {{--<div class="row">--}}
                            {{--<button class="btn btn-success btn-menu-rest"--}}
                            {{--onclick="loadDoc({!! $comercio->id !!})"--}}
                            {{--data-toggle="modal" data-target="#myModal-1-{!! $comercio->id !!}"--}}
                            {{--data-backdrop="static" data-keyboard="false">--}}
                            {{--<i class="fa fa-info"></i>&nbsp;Menu--}}
                            {{--</button>--}}
                            {{--</div>--}}
                            <div class="row">
                                {{--<button class="btn btn-default btn-menu-rest badge-text">Avaliacões <span--}}
                                {{--class="badge">42</span></button>--}}
                            </div>
                        </div>
                        {{--info card--}}
                        <div class="col-xs-7 col-xs-offset-1 fix-info-card-single">
                            <div class="texto-evento-single">
                                {!! $comercio->descricao !!}
                            </div>
                            <?= $comercio->telefone ? '<p class="card-body-item"><i class="fa fa-phone"></i> ' . $comercio->telefone . '</p>' : '<p><i class="fa fa-phone"></i>&nbsp; <a href="' . url("tips/suporte/1/5/.$comercio->id") . '">Adicionar telefone</a></p>' ?>
                            <?= $comercio->horario ? '<p class="card-body-item"><i class="fa fa-clock-o"></i> ' . $comercio->horario . '</p>' : '<p><i class="fa fa-clock-o"></i>&nbsp; <a href="' . url("tips/suporte/1/5/.$comercio->id") . '">Adicionar horário de funcionamento</a></p>' ?>
                            <?= $comercio->entrega ? '<p class="card-body-item"><i class="fa fa-motorcycle"></i> Delivery</p>' : '' ?>
                            <?= $comercio->endereco ? '<p class="card-body-item"><i class="fa fa-map-marker"></i> ' . $comercio->endereco . '</p>' : '<p><i class="fa fa-map-marker"></i>&nbsp; <a href="' . url("tips/suporte/1/5/.$comercio->id") . '">Adicionar endereço</a></p>' ?>
                            <?= $comercio->site ? '<p class="card-body-item"><i class="fa fa-chevron-right"></i> ' . $comercio->site . '</p>' : '<p><i class="fa fa-chevron-right"></i>&nbsp; <a href="' . url("tips/suporte/1/5/.$comercio->id") . '">Adicionar site</a></p>' ?>

                            <span>
                                <? $tipos = str_split($comercio->tipo) ?>
                                @foreach($tipos as $tipo)
                                    @if($tipo == 1)
                                        <i class="fa fa-cutlery" data-toggle="tooltip" data-placement="top"
                                           title="Serve Refeições"></i>&nbsp;Refeição
                                    @elseif($tipo == 2)
                                        <i class="fa fa-glass" data-toggle="tooltip" data-placement="top"
                                           title="Happy-Hour"></i>&nbsp;Happy-Hour
                                    @elseif($tipo == 3)
                                        <i class="fa fa-music" data-toggle="tooltip" data-placement="top"
                                           title="Local com pista de dança / Balada"></i>&nbsp;Diversão
                                    @elseif($tipo == 4)
                                        <i class="fa fa-transgender-alt" data-toggle="tooltip" data-placement="top"
                                           title="LGBT Friendly"></i>&nbsp;LGBT Friendly
                                    @endif
                                @endforeach
                        </span>

                            {{--GPS--}}
                            @if($comercio->lat != 0)
                                <a style="cursor: pointer;"
                                   onclick="myNavFunc({!! $comercio->lat !!},{!! $comercio->lon !!})"
                                   class="btn btn-menu-rest btn-default"><i class="fa fa-location-arrow"></i> Direções
                                    até o
                                    local</a>
                            @endif
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            {{--                            @include('sessoes.sub-comercio.evento')--}}

                            <p class="bottom-card-autor">Autor: {!! $comercio->user_name !!}</p>
                            <?= $comercio->legal_info ? '<p class="bottom-card-info">&copy; ' . $comercio->legal_info . '</p>' : '<p class="bottom-card-info">&copy; ' . $comercio->nome ?>
                        </div>
                    </div>
                    <br>
                    {{--footer card--}}
                    <div class="row">
                        <div class="col-xs-10">

                            {{--<div class="btn-group">--}}
                            {{--<div class="fb-like" data-href="{!! url('c/'.$comercio->slug) !!}/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>--}}
                            {{--</div>--}}
                            @if(Auth::guest())
                                <div class="btn-group">

                                    <a href="{!! url('login') !!}">
                                        Recomendar <span
                                                id="comercio-rate-thumb-{!! $comercio->id !!}"
                                                class="fa fa-thumbs-o-up">&nbsp;</span>
                                    </a>
                                </div>
                            @else
                                <div class="btn-group">

                                    <a onclick="upplace({!! $comercio->id !!})">
                                        <?= $sou_eu ? '<span id="comercio-rate-text-' . $comercio->id . '" class="up-up">Recomendado</span>' : '<span id="comercio-rate-text-' . $comercio->id . '" class="">Recomendar</span>' ?>
                                        <span
                                                id="comercio-rate-thumb-{!! $comercio->id !!}"
                                                class="fa <?= $sou_eu ? 'fa-thumbs-up up-up' : 'fa-thumbs-o-up' ?>">&nbsp;</span>
                                    </a>
                                </div>
                            @endif

                            <div class="btn-group">
                                {{--<a href="">--}}
                                {{--Compartilhar <span class="fa fa-share"></span>--}}
                                {{--</a>--}}
                                <a target="_blank"
                                   href="javascript:myPopup('http%3A%2F%2Fwww.facebook.com%2Fshare.php%3Fu%3D{!! url('place/'.$comercio->slug) !!}')">
                                    Compartilhar <span class="fa fa-share"></span>
                                </a>


                            </div>
                        </div>
                        <div class="col-xs-2">
                            <div class="btn-group pull-right">
                                <a class="clickable" type="button" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false">
                                    <span class="glyphicon glyphicon-flag"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{!! url('tips/suporte/1/5/'.$comercio->id) !!}"><span
                                                    class="fa fa-pencil"></span> Editar</a>
                                    </li>
                                    <li>
                                        <a href="{!! url('tips/suporte/1/4/'.$comercio->id) !!}"><span
                                                    class="fa fa-flag"></span> Solicitar remoção </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    {{--facebook comments não usado mais--}}

                    {{--<div id="comentarios" class="fb-comments" data-href="http://citytips.com.br/c/{!! $comercio->slug !!}" data-numposts="10"></div>                </div>--}}
                    {{--<style>.fb-comments, .fb-comments iframe[style], .fb-like-box, .fb-like-box iframe[style] {width: 100% !important;}--}}
                    {{--.fb-comments span, .fb-comments iframe span[style], .fb-like-box span, .fb-like-box iframe span[style] {width: 100% !important;}--}}
                    {{--</style>--}}

                </div>
                {{--                @include('sessoes.modal.modal-ver-cardaprio')--}}
            </div>
        </div>
        {{--        @include('sessoes.sub-comercio.comentarios')--}}
        {{--        @include('sessoes.sub-comercio.comentarios')--}}

    </div>
    @include('sessoes.sub-comercio.comercios-feed')

    @include('layouts.footer')
@endsection
