
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content mudar-cidade-modal ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel">Selecione a cidade</h2>
            </div>
            <div class="modal-body">
                <div class="row row-list-city-modal">
                @foreach($cidades as $atual)
                    {{--@if($atual->id == 2 || $atual->id == 4 || $atual->id == 5)--}}
                        {{--@continue--}}
                    {{--@endif--}}
                    <div class="col-sm-6">
                        <div class="bs-callout-city lista-cidades"
                             style="background-image: url('https://citytips.com.br/images/{!! $atual->slug !!}.jpg') ;background-size: cover; border-color: #f8f8f8;border-width: 3px;">
                            <p class="text-center"><a href="{!! url('/'.$atual->slug) !!}">{!! $atual->nome !!}</a></p>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>

        </div>
    </div>
</div>