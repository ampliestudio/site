<div class="modal fade" id="myModal-1-{!! $comercio->id !!}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button onclick="clearDiv({!! $comercio->id !!})" type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{!! $comercio->nome !!}</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="{!! $comercio->id !!}" id="com-id{!! $comercio->id !!}" class="sr-only">
                <div id="value{!! $comercio->id !!}">
                    Carregando...
                    {{--aqui se monta o cardaprio--}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="clearDiv({!! $comercio->id !!})" class="btn btn-default"
                        data-dismiss="modal">Fechar
                </button>
                <a href="{!! url('user/criar-item-cardaprio/'.\Illuminate\Support\Facades\Crypt::encrypt($comercio->id)) !!}"
                   class="btn btn-success btn-add-comercio">Adicionar item</a>
            </div>
        </div>
    </div>
</div>

