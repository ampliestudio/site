<!-- Button trigger modal -->
<button type="button" class="btn btn-block text-center btn-social btn-facebook" data-toggle="modal"
        data-target="#myModal">
    <span class="fa fa-facebook"></span>&nbsp;Importar evento do Facebook
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Importar evento do facebook</h4>
            </div>
            <div class="modal-body">
                <form action="{!! url('user/facebook-import') !!}" method="post">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="form-group">
                        <label for="link">Cole o link do evento</label>
                        <input type="text" id="link-fb" onblur="linkColou()" onmousemove="linkColou()"
                               onclick="linkColou()" class="form-control" name="link">
                    </div>
                    <div class="form-group cidade-hid">
                        <label for="sel1">Em qual cidade será?</label>
                        <select name="cidade" class="form-control" id="sel1">
                            @foreach($cidades as $item)
                                <option value="{!! $item->id !!}">{!! $item->nome !!}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="local">Vincule a um local existente no CityTips
                            <small>(Opcional)</small>
                        </label>
                        <div id="input-vinc">
                            <input type="text" class="form-control" autocomplete="off"  placeholder="Digite o nome do local e selecione a sugestão" name="local" id="autocomplete">
                        </div>
                        <input type="hidden" class="local-id" value="0" name="localid">
                        <div id="vinculo-val"></div>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-default" id="link-btn" disabled type="submit" value="Importar">
                    </div>
                </form>
                <small>O evento deve ser público</small>
                <div id="input-vinc-st">

                </div>

            </div>
            {{--<div class="modal-footer">--}}
            {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
            {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
            {{--</div>--}}
        </div>
    </div>
</div>

<script>

    function linkColou() {
        var link = $('#link-fb').val();
        var oi = isURL(link);
        if (oi == true) {
            $('#link-btn').attr('disabled', false);
        } else {
            $('#link-btn').attr('disabled', true);
        }
    }

    function isURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return pattern.test(str);
    }

    $(document).ready(function () {

        var locais = [
                @foreach($lugares as $item)
            {
                value: "{!! $item->nome !!}", data: "{!! $item->id !!}"
            },
            @endforeach
        ];

        $('#autocomplete').autocomplete({
            lookup: locais,
            onSelect: function (suggestion) {
                $('.local-id').val(suggestion.data);
                $('#vinculo-val').append('<p>Vinculado: '+suggestion.value+' <button type="button" onclick="removervinc()"><i class="fa fa-trash"></i></button> </p>');
                $('#input-vinc-st').empty();
                $('#input-vinc-st').append('<style> #autocomplete{display: none;}.cidade-hid{display: none}</style>');
            }
        });
    });


    function removervinc(){
        $('#input-vinc-st').empty();
        $('#input-vinc-st').append('<style> #autocomplete{display: block;}.cidade-hid{display: block}</style>');
        $('#vinculo-val').empty();
        $('#autocomplete').val('');
        $('.local-id').val('0');
    }
</script>


