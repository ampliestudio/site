<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLogin" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Entre para continuar!</h4>
            </div>
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        <a href="{!! url('auth/facebook') !!}"
                           class="btn btn-lg btn-social btn-block btn-facebook">
                            <span class="fa fa-facebook"></span> Acessar com Facebook
                        </a>
                    </div>
                </div>
                <br>
                <p class="text-center">
                    <small>Ao me registrar em CityTips aceito os <a href="{!! url('tips/termos') !!}">termos</a>
                        impostos pelo site
                    </small>
                </p>
                <div class="row">
                    <hr>
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        <a class="btn btn-default btn-block" type="button" data-dismiss="modal" aria-label="Close">Agora não</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

