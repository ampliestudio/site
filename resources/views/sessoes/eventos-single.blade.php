@extends('layouts.app')
@section('meta')
    <meta name="{!! $evento->nome !!} - CityTips! ">
    <meta about="{!! $evento->descricao !!}">
    <meta property="og:url" content="{!! Request::url() !!}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="{!! $evento->nome !!} - CityTips! "/>
    <meta property="og:description"
          content="{!! $evento->descricao !!}"/>
    <meta property="og:image" content="{!! asset('uploads/'. $evento->local_foto) !!}"/>
    <meta property="fb:app_id" content="{!! Config::get('constants.FBAPPID') !!}"/>
@endsection

@section('menu-left')
    {{--incluir modal em content--}}
    @include('layouts.nav.menu-left-pagina-cidade')
@endsection

@section('content')
    {{--Modal--}}
    @include('sessoes.modal.modal-mudar-cidade')
    <div class="row">
        <div class=" col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">

            &nbsp;<a href="{!! url($cidade->slug) !!}" style="color: white"><i class="fa fa-chevron-circle-left"> &nbsp;Ver
                    todos
                    de {!! $cidade->nome !!}</i></a>
        </div>
    </div>

    <?
    $soma_rate_atual = 0;
    $sou_eu = 0;
    ?>
    @foreach($score as $rate_atual)
        @if($rate_atual->evento_id == $evento->id)
            <?
            //opa! temos mais um positivo!
            $soma_rate_atual++;
            //opa! a pessoa logada que deu esse positivo!
            if (\Illuminate\Support\Facades\Auth::id() == $rate_atual->user_id) {
                $sou_eu = 1;
            }
            ?>
        @endif
    @endforeach
    <?
    date_default_timezone_set('America/Sao_Paulo');

    //                            dd($evento->inicio)

    $data = $evento->inicio;
    $data = strtotime($data);
    $dia = date('d', $data);
    $mes = date('m', $data);
    switch ($mes) {
        case 1:
            $mes = "Janeiro";
            break;
        case 2:
            $mes = "Fevereiro";
            break;
        case 3:
            $mes = "Março";
            break;
        case 4:
            $mes = "Abril";
            break;
        case 5:
            $mes = "Maio";
            break;
        case 6:
            $mes = "Junho";
            break;
        case 7:
            $mes = "Julho";
            break;
        case 8:
            $mes = "Agosto";
            break;
        case 9:
            $mes = "Setembro";
            break;
        case 10:
            $mes = "Outubro";
            break;
        case 11:
            $mes = "Novembro";
            break;
        case 12:
            $mes = "Dezembro";
            break;
    }
    $hora = date('H:i', $data);
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <br>
            @if(isset($evento->local_foto))
                <img src="{!! asset('uploads/'. $evento->local_foto) !!}"
                     class="img-thumbnail image-bubble img-circle"
                     alt="{!! $evento->local_nome !!}">
            @else
                <img src="{!! asset('uploads/alias-image.png') !!}"
                     class="img-thumbnail image-bubble img-circle"
                     alt="{!! $evento->local_nome !!}">
            @endif
        </div>
        <div class="eventos col-xs-12 col-sm-7">
            <div class=" ev-{!! $evento->id !!} bubble">


                <h3>{!! $evento->nome !!}@if($evento->tipo == 2) -
                    <small>{!! $evento->username !!}</small>@endif</h3>

                {{--celular--}}
                <div class="col-xs-12">
                    @if($evento->descricao != null)
                        <div class="texto-evento-single">
                            {!! $evento->descricao !!}
                        </div>
                    @endif
                    @if($evento->id_local != 0)
                        <p><i class="fa fa-map-marker">&nbsp; <a
                                        href="{!! url('/place/'.$evento->local_slug) !!}">{!! $evento->local !!}</a></i>
                        </p>
                    @else
                        <p><i class="fa fa-map-marker">&nbsp; {!! $evento->local !!}</i></p>
                    @endif
                    <p class="xs-date"><i class="fa fa-calendar-o">&nbsp;{!! $dia !!} de {!! $mes !!}</i>
                    </p>
                    <p class="xs-date"><i class="fa fa-clock-o">&nbsp; ás {!! $hora !!}h</i></p>
                </div>
                {{--GPS--}}
                @if($evento->lat != 0 && $evento->lat != -23.4087)
                    <a style="cursor: pointer;"
                       onclick="myNavFunc({!! $evento->lat !!},{!! $evento->lon !!})"
                       class="btn btn-menu-rest btn-default"><i class="fa fa-location-arrow"></i> Direções
                        até o
                        local</a>
                @endif

                <br><br>
                {{--<a href="{!! url('user/score/'.$id_crypt) !!}">--}}
                <div class="row">
                    <div class="col-xs-12">
                        <a class="clickable" type="button" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <span class="glyphicon glyphicon-option-horizontal"></span>
                        </a>
                        <ul class="dropdown-menu">
                            @if(Auth::id() == $evento->user_id)
                                <li>
                                    <a class="clickable" type="button" data-toggle="modal"
                                       data-target="#modal-del{!! $evento->id !!}"><span
                                                class="fa fa-times"></span> Excluir</a>
                                </li>
                            @else
                                <li>
                                    <a href="{!! url('tips/suporte/3/5/'.$evento->id) !!}"><span
                                                class="fa fa-pencil"></span> Editar</a>
                                </li>
                            @endif

                            <li>
                                <a href="{!! url('tips/suporte/3/4/'.$evento->id) !!}"><span
                                            class="fa fa-flag"></span> Reportar </a>
                            </li>
                        </ul>
                        @include('sessoes.modal.modal-excluir-status')
                        <div class="btn-group pull-right">
                            @if (Auth::guest())
                                <a href="{!! url('login') !!}">
                                <span id="badge-{!! $evento->id !!}"
                                      class="badge badge-wait">{!! $evento->score_count !!}</span>
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            @else
                                <a class="clicable <?= $sou_eu ? 'st-st' : '' ?>" onclick="upthis({!! $evento->id !!})">

                                <span id="badge-{!! $evento->id !!}"
                                      class="badge badge-wait">{!! $evento->score_count !!}</span>

                                    <i id="star-{!! $evento->id !!}"
                                       class="fa <?= $sou_eu ? 'fa-chevron-down st-st' : 'fa-chevron-up' ?>"></i>
                                </a>
                            @endif
                        </div>
                    </div>

                </div>


            </div>
        </div>


    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <br>
            {{--@if(isset($evento->local_foto))--}}
            {{--<img src="{!! asset('uploads/alias-image.png') !!}"--}}
            {{--class="img-thumbnail image-bubble img-circle"--}}
            {{--alt="{!! $evento->local_nome !!}">--}}
            {{--@else--}}
            {{--<img src="{!! asset('uploads/alias-image.png') !!}"--}}
            {{--class="img-thumbnail image-bubble img-circle"--}}
            {{--alt="{!! $evento->local_nome !!}">--}}
            {{--@endif--}}
        </div>
        <div id="comentarios" class="eventos col-xs-12 col-sm-7">
            <div class="ev-comments">
                <div class="fb-comments" data-width="100%" data-href="{!! url('evento/'.$evento->slug_id) !!}"
                     data-numposts="5"></div>
            </div>
        </div>
    </div>

    <div class="row ev-related">
        <br>
        <h4 class="text-center" style="color: white">Eventos que você talvez curta:</h4>


        @foreach($eventos as $item)
            <?
            if ($item->id == $evento->id) {
                continue;
            }
            ?>
            @include('sessoes.sub-evento.evento-bloco-single')
        @endforeach

    </div>
    <div class="row">
        <div class=" col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <br>
            &nbsp;<a href="{!! url($cidade->slug) !!}" style="color: white"><i class="fa fa-chevron-circle-left">
                    &nbsp;Ver
                    todos
                    de {!! $cidade->nome !!}</i></a>
            <br><br>
        </div>
    </div>

    @include('layouts.footer')
@endsection
