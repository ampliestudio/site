@extends('layouts.app')
@section('meta')
    <meta name="description" content="Descubra e compartilhe o seu rolê em {!! $cidade->nome !!} com CityTips">
    <meta name="keyowrds"
          content="{!! $cidade->nome !!}, city tips, citytips, bar, pub, balada, eventos, eventos em {!! $cidade->nome !!}, balada em {!! $cidade->nome !!}, festa em {!! $cidade->nome !!}">
    <meta name="robots" content="index, follow, nosnippet">
    <meta property="og:url" content="{!! Request::url() !!}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="O que tem pra hj em {!! $cidade->nome !!}?!"/>
    <meta property="og:description"
          content="Descubra e compartilhe o seu rolê em {!! $cidade->nome !!} com CityTips"/>
    <meta property="og:image" content="{!! asset('images/wall-2.png') !!}"/>
    <meta property="fb:app_id" content="{!! Config::get('constants.FBAPPID') !!}"/>
    {{--script que verifica o login--}}

@endsection

@section('menu-left')
    {{--incluir modal em content -- modal selecionar cidade--}}
    @include('layouts.nav.menu-left-pagina-cidade')
@endsection

@section('content')
    {{--modal de menu-left -- modal selecionar cidade--}}
    @include('sessoes.modal.modal-mudar-cidade')
    @if(Auth::guest())
        @include('sessoes.modal.modal-fazer-login')
    @endif
    <div class="container">

        <ul class="nav nav-tabs" role="tablist">
            <li><a class="btn btn-success" href="{!! url('places/'.$cidade->slug) !!}">Conheça os locais</a></li>
            <li><a class="btn btn-success" href="{!! url('user/criar-evento-novo/'.$cidade->id) !!}">Criar evento</a></li>
        </ul>
        {{--lista de eventos da cidade--}}
        @include('sessoes.sub-cidade-home.eventos')

    </div>

    {{--icone para voltar ao topo--}}
    <a href="#0" class="cd-top">Top</a>

    <script>


        @if(Auth::guest())
        $(document).ready(function () {

            setTimeout(function () {
                $('#modalLogin').modal('show');
                $('#modalLogin').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            }, 5000);

        });
        @endif


    </script>
    @include('layouts.footer')
@endsection
