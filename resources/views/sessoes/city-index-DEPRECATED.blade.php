@extends('layouts.app')
@section('meta')
    <meta name="description" content="Descubra e compartilhe o seu rolê em {!! $cidade->nome !!} com CityTips">
    <meta name="keyowrds" content="{!! $cidade->nome !!}, city tips, citytips, bar, pub, balada, eventos, eventos em {!! $cidade->nome !!}, balada em {!! $cidade->nome !!}, festa em {!! $cidade->nome !!}">
    <meta name="robots" content="index, follow, nosnippet">
    <meta property="og:url" content="{!! Request::url() !!}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="O que tem pra hj em {!! $cidade->nome !!}?!"/>
    <meta property="og:description"
          content="Descubra e compartilhe o seu rolê com CityTips"/>
    <meta property="og:image" content="{!! asset('images/wall-2.png') !!}"/>
    <meta property="fb:app_id" content="{!! Config::get('constants.FBAPPID') !!}" />
@endsection

@section('menu-left')
    {{--incluir modal em content--}}
    @include('layouts.nav.menu-left-pagina-cidade')
@endsection

@section('content')
    {{--Modal--}}
    @include('sessoes.modal.modal-mudar-cidade')
    @include('sessoes.modal.modal-fazer-login')
    {{-- ˆˆˆˆ  desativado. fazendo verificação no middleware agora, não pode esntrar sem logar--}}
    <div class="container-fluid">

        {{--<img src="https://citytips.com.br/images/veja-aqui.png" id="veja-aqui">--}}
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a class="tab-style" href="#eventos" aria-controls="eventos" role="tab"
                                       data-toggle="tab">
                    {{--<i class="fa fa-calendar"></i>&nbsp;--}}Eventos
                    {{--<span class="badge badge-fix badge-danger">@if(count($eventoshj)){!! count($eventoshj) !!}@endif</span>--}}
                </a>
            </li>
            <li role="presentation"><a class="tab-style" id="button-com-click" href="#restaurantes"
                                                      aria-controls="restaurantes" role="tab" data-toggle="tab">
                    {{--<i class="fa fa-glass"></i>--}}Lugares
                </a>
            </li>
            <li><a href="{!! url('user/criar-evento-novo/'.$cidade->id) !!}">Criar evento</a></li>

        </ul>
        <script>
//            desativado. fazendo verificação no middleware agora, não pode esntrar sem logar
            $(document).ready(function () {

                @if(Auth::guest())
                $('#modalLogin').modal('show');
                $('#modalLogin').modal({
                    backdrop: 'static',
                    keyboard: false
                })
                @endif
            });
        </script>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="restaurantes">
                @include('sessoes.comercios')
            </div>
            <div role="tabpanel" class="tab-pane in active" id="eventos">
                @include('sessoes.eventos')
            </div>
        </div>
    </div>
    <a href="#0" class="cd-top">Top</a>

    @include('layouts.footer')
@endsection
