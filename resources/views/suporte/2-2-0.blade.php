@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-success">
                    <div class="panel-heading">Requisitar edição</div>
                    <div class="panel-body">
                        <small class="pull-right">*Obrigatório</small>

                        <form action="{!! url('tips/suporte') !!}" method="post">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <input type="hidden" name="info" value="Requisitar edição item menu id = {!! $item !!}">
                            <input type="hidden" name="id" value="<?= \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::id() : '0' ?>">

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="nome">Nome*</label>
                                        <div class='text-danger'>{{$errors->first('nome')}}</div>
                                        <input type="text" name="nome" value="<?= \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->name : '' ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="email">E-mail*</label>
                                        <div class='text-danger'>{{$errors->first('email')}}</div>
                                        <input type="text" name="email" value="<?= \Illuminate\Support\Facades\Auth::check() ? \Illuminate\Support\Facades\Auth::user()->email : '' ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="texto">Descreva as edições e motivo*</label>
                                        <div class='text-danger'>{{$errors->first('email')}}</div>
                                        <textarea name="texto" class="form-control" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    {!! Recaptcha::render() !!}
                                    <br>
                                    @if ($errors->has('recaptcha'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('recaptcha') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn-add-comercio btn btn-success">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
