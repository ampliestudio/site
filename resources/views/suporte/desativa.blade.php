@extends('layouts.app')

@section('content')

    <br><br>
    <div class="container-fluid">
        <div class="col-md-offset-1 col-md-10">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Sucesso! <i class="fa fa-smile-o"></i></h1>
                <br><br>
                <p class="text-center" style="color: white"><strong>
                        Deixaremos de enviar e-mails semanais para {!! $email !!}. Até logo!</strong><br>Para constinuar recebendo os informativos de eventos semanalmente ative os emails nas configurações de conta.</p>
                <p class="text-right" style="color: white"><small>Equipe City Tips.</small></p>
            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection
