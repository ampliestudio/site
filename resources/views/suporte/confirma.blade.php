@extends('layouts.app')

@section('content')

    <br><br>
    <div class="container-fluid">
        <div class="col-md-offset-1 col-md-10">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Sucesso! <i class="fa fa-smile-o"></i></h1>
                <br><br>
                <p class="text-center" style="color: white"><strong>
                        Sua solicitação foi recebida, iremos responder via e-mail. Protocolo: 2017{!! $id !!}</strong></p>
                <p class="text-right" style="color: white"><small>Equipe City Tips.</small></p>
                <h6 class="text-center"><a href="mailto:contato@citytips.com.br?subject=Contato%20CityTips&body=Oi!">Contato direto</a></h6>
            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection
