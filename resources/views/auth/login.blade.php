@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    {{--<div class="panel-heading">Login</div>--}}
                    <div class="panel-body">
                        <h2 style="color: #1B5E20" class="text-center"><i class="fa fa-hand-peace-o"></i>&nbsp; Bem Vindo ao #CityTips</h2>
                        {{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                        {{--<label for="email" class="col-md-4 control-label">E-Mail</label>--}}

                        {{--<div class="col-md-6">--}}
                        {{--<input id="email" type="email" class="form-control" name="email"--}}
                        {{--value="{{ old('email') }}">--}}

                        {{--@if ($errors->has('email'))--}}
                        {{--<span class="help-block">--}}
                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                        {{--</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                        {{--<label for="password" class="col-md-4 control-label">Senha</label>--}}

                        {{--<div class="col-md-6">--}}
                        {{--<input id="password" type="password" class="form-control" name="password">--}}

                        {{--@if ($errors->has('password'))--}}
                        {{--<span class="help-block">--}}
                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                        {{--</span>--}}
                        {{--@endif--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                        {{--<div class="col-md-6 col-md-offset-4">--}}
                        {{--<div class="checkbox">--}}
                        {{--<label>--}}
                        {{--<input type="checkbox" name="remember" checked>Manter logado <i class="fa fa-info-circle" data-toggle="tooltip-lembrar" data-placement="top" title="Sua conta ficará conectada até clicar em sair, mesmo que feche o site. Desmarque esta opção se estiver acessando de um computador público"></i>--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                        {{--<div class="col-md-6 col-md-offset-4">--}}
                        {{--<button type="submit" class="btn btn-primary">--}}
                        {{--<i class="fa fa-btn fa-sign-in"></i> Login--}}
                        {{--</button>--}}

                        {{--<a class="btn btn-link" href="{{ url('/password/reset') }}">Esqueci minha senha?</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</form>--}}

                        {{--<hr>--}}

                        <div class="row">

                            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                                <a href="{!! url('auth/facebook') !!}"
                                   class="btn btn-lg btn-social btn-block btn-facebook">
                                    <span class="fa fa-facebook"></span> Acessar com Facebook
                                </a>
                            </div>
                        </div>
                        <br>
                        <p class="text-center">
                            <small>Ao me registrar em CityTips aceito os <a href="{!! url('tips/termos') !!}">termos</a>
                                impostos pelo site
                            </small>
                        </p>
                        {{--<hr>--}}

                        {{--<div class="row">--}}
                        {{--<div class="col-md-6 col-md-offset-4">--}}
                        {{--<a href="{!! url('/register') !!}">Ainda não tenho uma conta</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="visible-md visible-lg visible-sm" style="margin-bottom: 200px"></div>

    @include('layouts.footer')
@endsection
