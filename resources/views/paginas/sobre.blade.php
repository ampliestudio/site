@extends('layouts.app')

@section('content')

    <br><br>
    <div class="container-fluid">
        <div class="col-md-offset-1 col-md-10">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Sobre <i class="fa fa-smile-o"></i></h1>
                <br><br>
                <p class="text-center" style="color: white">Um site que reune o melhor do lazer de cada cidade, descubra
                    restaurantes, bares, clubes, entre outros lugares de uma forma simples e atualizada. <br> Compartilhe com seus amigos qual vai ser o rolê ou a aventura de hoje, saiba o que está para acontecer na sua cidade e o que as pessoas estão comentando mais! Embreve muitas novidades por aqui
                    <br> Aguardem as futuras atualizações ;)</p>
                {{--<a href="{!! url('user/home') !!}" class="btn btn-success btn-lg">Começe agora!</a>--}}

                <br>
                <h2 class="text-center" style="color: white">FAQ</h2>
                <br>
                <h4 style="color: white" class="text-center">Beta?</h4>
                <p class="text-center" style="color: white">Sim! nós estamos apenas começando, então você pode ver
                    alguma mudança aqui ou ali, algumas coisas podem sumir ou mudar de formato, o que importa é que
                    estamos cheios de ideias, ao longo das próximas semanas estaremos soltando novidades e estudando quais fazem mais sucesso e moldando o site para ficar cada vez melhor e mais útil.</p>
                <br>
                <h4 style="color: white" class="text-center">Preciso pagar para anunciar aqui?</h4>
                <p class="text-center" style="color: white">Não, é tudo grátis mesmo e seu negócio aparece no site após
                    aprovarmos alguns requisitos como ser interessante ao público e possuir todas as informações
                    preenchidas corretamente (ex: logo, endereço, telefone...)</p>
                <br>
                <h4 style="color: white" class="text-center">Não vejo minha cidade ou balada favorita aqui, como faço
                    para incluir?</h4>
                <p class="text-center" style="color: white">CityTips é uma plataforma onde todos podem adicionar
                    conteúdo! Estes conteúdos estão sujeitos a aprovação antes de ir ao ar, então se acha que algo está
                    faltando, mão a obra! Se tiver algum problema ou não saber como fazer algo é só clicar em ajuda e
                    mandar seu textão para nós ;) </p>
                <br>
                <h4 style="color: white" class="text-center">Sou dono de um restaurante/bar/balada e não quero que
                    pessoas postem em meu nome, quero fazer tudo sozinho</h4>
                <p class="text-center" style="color: white">Seu pedido é uma ordem! Temos um tipo de conta especial para
                    donos de estabelecimentos, estas pessoas podem fazer alterações e publicar eventos sem necessidade
                    de aprovação e ainda tem um selo
                    <br> de <i>verificado<span class="oficial-icon fa fa-check-square-o"></span></i> em seu nome. Ah..
                    isso também é gratis, e para solicitar sua conta especial é só clicar em ajuda e dizer que tem
                    interesse </p>
                <br>
                <h4 style="color: white" class="text-center">"Tenho uma idéia!" Ou "Adoro CityTips <i
                            class="fa fa-heart-o"></i>"</h4>
                <p class="text-center" style="color: white">Queremos te ouvir! Clique em ajuda e nos diga agora mesmo
                    ;)</p>
                <br>
                <h4 style="color: white" class="text-center">Tem uma coisa neste site que não me agradou ou infringe
                    meus direitos legais ou da minha empresa</h4>
                <p class="text-center" style="color: white">Nossa! Isso é sério... Se este for o caso clique em Reportar
                    violação na parte inferior do site ou envie um e-mail direto para
                    <a href="mailto:staff@citytips.com.br" style="color: white; text-decoration: underline">staff@citytips.com.br</a> </p>


            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection
