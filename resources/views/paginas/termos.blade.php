@extends('layouts.app')

@section('content')

    <br><br>
    <div class="container-fluid">
        <div class="col-md-offset-1 col-md-10">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Termos <i class="fa fa-hand-o-down"></i></h1>
                <br><br>
                <p class="text-justify" style="color: white; font-size: 12pt">
                    <strong>Fontes:</strong> CityTips é um site que mantém seu conteúdo atualizado pela comunidade, todo
                    conteúdo é
                    baseado em informações disponíveis ao público na internet, não sendo permitido a postagem de
                    conteúdo privado ou não préviamente autorizado.<br>
                    <strong>Moderação:</strong> A moderação de todo conteúdo postado pela comunidade é feita pela equipe
                    do CityTips
                    sendo analisado o mínimo grau de veracidade E NÃO SE O CONTEÚDO É REALMENTE PÚBLICO, se caso o próprio estabelecimento se sentir prejudicado
                    pelas informações, pode solicitar a remoção em: <a
                            href="{!! url('tips/suporte/0/4/0') !!}">Reportar erro ou violação</a>. <br>
                    <strong>Direitos sobre conteúdo:</strong> A CityTips não se responsabiliza por conteúdo postado por
                    terceiros, é possível solicitar remoção dos conteúdos rapidamente em: <a
                            href="{!! url('tips/suporte/0/4/0') !!}">Reportar erro ou violação</a>, preenchendo o formulário corretamente e sinalizando os motivos pelos quais o conteúdo deve ser removido. <br>
                    <strong>Símbolo de estabelecimento verificado:</strong> Os estabelecimentos podem gerenciar seu próprio conteúdo.
                    Assim não será possível a comunidade alterar, estes estabelecimentos recebem o selo de autenticidade
                    azul junto ao seu nome, basta solicitar em <a href="mailto:contato@citytips.com">contato@citytips.com</a>. <br>
                    <strong>Dados dos usuários:</strong> CityTips pode repassar os dados dos usuários cadastrados a
                    terceiros com fins comerciais futuramente. <br>CityTips poderá enviar e-mail informativos sobre mudanças no site e nos termos, e-mails comerciais e anúncios sendo possível cancelamento.
                    Nós podemos coletar e processar os seguintes dados sobre você:
                    As informações que você fornecer através do preenchimento de formulários no site. Isso inclui informações fornecidas no momento do registro para usar o site, assinando o nosso serviço, publicando material ou solicitar outros serviços, quando você relatar um problema ou sugerir alguma alteração no o site.
                    Se nos contatar, poderemos manter um registro dessa correspondência.
                    Também pode pedir-lhe para preencher questionários que utilizamos para fins de pesquisa, embora você não é obrigado a fazê-lo.
                    Detalhes de suas visitas ao site, dados de tráfego, dados de localização, weblogs e outros dados de comunicação, se tal for necessário para nossos próprios fins ou não, e os recursos que você acessa.
                    Você tem o direito de nos pedir para não processar os seus dados pessoais para fins de marketing.
                    <br>
                    <strong>Cookies:</strong> Nosso site utiliza cookies para salvar algumas
                    informações. Caso não queira, você pode desabilitar os cookies do seu navegador ou usar uma janela anônima.
                    <a href="{!! url('tips/cookies') !!}">Ver mais.</a> <br>
                    <br>
                    <strong>Em caso de dúvida entre em contato conosco pelo e-mail: <a href="mailto:contato@citytips.com">contato@citytips.com</a></strong>


                </p>
            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection
