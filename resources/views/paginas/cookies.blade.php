@extends('layouts.app')
@section('content')

    <br><br>
    <div class="container-fluid">
        <div class="col-md-offset-1 col-md-10">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Cookies <i class="fa fa-paw"></i></h1>
                <br><br>
                <p class="text-justify" style="color: white; font-size: 12pt">
                    <strong>Cookies e spyware:</strong> Nunca usamos ou instalamos spyware no seu computador, nem usamos spyware para obter informações do seu computador.
                    <br>

                    Como muitos sites, nós usamos "cookies", que são arquivos salvos no seu computador pelo seu navegador e utilizados para recolher e armazenar determinadas informações sobre você.

                    A maioria dos navegadores aceita cookies automaticamente, mas permitem que você os desative. O menu de ajuda na maioria dos navegadores irá lhe mostrar como evitar que seu navegador aceite novos cookies, como fazer com que o navegador o notifique quando receber um novo cookie e como desativar todos os cookies. Você também pode desativar ou apagar os cookies você já aceitos se desejar.
                    <br>

                    Recomendamos que você deixe os cookies "ligado" para que nós possamos lhe oferecer uma melhor experiência no site. Se você configurar seu navegador para bloquear todos os cookies (incluindo cookies estritamente necessários), você pode não ser capaz de acessar partes do site.

                    Criamos alguns cookies no seu dispositivo e recomendamos que você concorde com seu uso. Se você não fizer isso, você ainda pode usar o site, mas certas partes do mesmo podem não funcionar ou não funcionar tão bem.
                    <br><br>
                    <strong>Em caso de dúvida entre em contato conosco pelo e-mail: <a href="mailto:contato@citytips.com">contato@citytips.com</a></strong>

                </p>
            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection

