@extends('layouts.app')

@section('content')

    <br><br>
    <div class="container-fluid">
        <div class="col-md-offset-1 col-md-10">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Licenças <i class="fa fa-book"></i></h1>
                <br><br>
                <p class="text-center" style="color: white; font-size: 15pt">Desenvolvido com códigos e frameworks de Laravel, TWITER &reg; Bootstrap, jQuery, GOOGLE &reg;, FACEBOOK &reg;, Font Awesome, Metafizzy, CLOUDFLARE &reg;, AJAX Autocomplete by Tomas Kirda, Moment.js, Isotope.js by Metafizzy
                    <br>
                    Sob as licenças: <a style="color: white; text-decoration: underline"
                            href="https://github.com/twbs/bootstrap/blob/master/LICENSE" target="_blank" rel="license">MIT</a>,
                    e <a href="https://creativecommons.org/licenses/by/3.0/" target="_blank" style="color: white; text-decoration: underline" rel="license"><i class="fa fa-creative-commons"> </i> Creative Commons BY 3.0
                        </a></p>
                <br>
                <p class="text-center" style="color: white; font-size: 9pt">
                    <i>
                        The MIT License (MIT)

                        Permission is hereby granted, free of charge, to any person obtaining a copy
                        of this software and associated documentation files (the "Software"), to deal
                        in the Software without restriction, including without limitation the rights
                        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
                        copies of the Software, and to permit persons to whom the Software is
                        furnished to do so, subject to the following conditions:

                        The above copyright notice and this permission notice shall be included in
                        all copies or substantial portions of the Software.

                        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
                        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
                        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
                        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
                        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
                        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
                        THE SOFTWARE.
                    </i>
                </p>
                <br>
                <p class="text-center" style="color: white; font-size: 9pt">TWITTER é marca registrada de TWITTER INC. Todos os direitos reservados</p>
                <br>
                <p class="text-center" style="color: white; font-size: 9pt">FACEBOOK é marca registrada de FACEBOOK INC. Todos os direitos reservados</p>
                <br>
                <p class="text-center" style="color: white; font-size: 9pt">GOOGLE é marca registrada de GOOGLE INC. Todos os direitos reservados</p>
            </div>
        </div>
    </div>
    <br><br><br>

    @include('layouts.footer')
@endsection
