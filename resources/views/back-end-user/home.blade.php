@extends('layouts.app')

@section('menu-left')
    {{--incluir modal em content--}}
    @include('layouts.nav.menu-left-pagina-cidade-usuario-home')
@endsection

@section('content')
    {{--Modal--}}
    @include('sessoes.modal.modal-mudar-cidade')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(Session::has('flash_message'))
                    <div class="alert alert-info"><span
                                class="glyphicon glyphicon-ok"></span> {!! print_r(session('flash_message')) !!}<a
                                href="#"
                                class="close"
                                data-dismiss="alert"
                                aria-label="close">&times;</a>
                    </div>
                @endif
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">{!! $user->name !!}<a class="pull-right"
                                                                      href="{{ url('user/conf') }}"><i
                                        class="fa fa-btn fa-cog">&nbsp;
                                    <small>Opções</small>
                                </i></a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-4 col-md-3 col-md-offset-0 col-lg-3 "
                                 align="center"><img class="img-responsive img-circle"
                                                     alt="User Pic"
                                                     src="{!! $user->foto !!}">
                                <br>


                            </div>

                            <div class="col-xs-12 visible-xs"
                                 align="center">

                            </div>

                            <div class="col-xs-12 col-md-9 col-lg-9 ">
                                {{--lugares--}}
                                <div class="row">
                                    <h4 class="text-center"><i class="fa fa-thumbs-o-up">&nbsp; Recomendados</i></h4>
                                    @if(count($recomendados) == 0)
                                        <div class="col-sm-12">
                                            <h4 class="text-center">Você ainda não recomendou nenhum lugar</h4>
                                            <h5 class="text-center">Ao clicar em "<i class="fa fa-thumbs-o-up">&nbsp;Recomendar</i>"
                                                em um restaurante por exemplo, ficará salvo aqui
                                            </h5>
                                        </div>
                                    @endif
                                    @foreach($recomendados as $atual)
                                        <div class="col-sm-6">
                                            <div class="well lista-comercios"
                                                 style="background-image: url('../uploads/{!! $atual->foto !!}') ;background-size: contain;background-repeat: no-repeat; border-color: #f8f8f8;border-width: 3px;">
                                                <p class="text-right"><a
                                                            href="{!! url('place/'.$atual->slug) !!}">{!! $atual->nome !!}</a>
                                                </p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <hr>
                                {{--eventos--}}
                                <div class="row">
                                    <h4 class="text-center"><i class="fa fa-star-o">&nbsp; Eventos marcados</i></h4>
                                    @if(count($marcados) == 0)
                                        <div class="col-sm-12">
                                            <h4 class="text-center">Você ainda não marcou nenhum evento</h4>
                                            <h5 class="text-center">Ao clicar em "<i class="fa fa-star-o">
                                                    &nbsp;Marcar</i>"
                                                um evento ele ficará salvo aqui
                                            </h5>
                                        </div>
                                    @endif
                                    @foreach($marcados as $atual)
                                        <?
                                        $data = $atual->inicio;
                                        $data = strtotime($data);
                                        $dia = date('d', $data);
                                        $mes = date('m', $data);
                                        ?>
                                        <div class="col-sm-6">
                                            <div class="well lista-comercios">
                                                <p>
                                                    <a href="{!! url('evento/'.$atual->slug_id) !!}">{!! $dia !!}
                                                        / {!! $mes !!} - {!! $atual->nome !!}</a>
                                                </p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <hr>
                                {{--temporário cidades--}}
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="text-center"><i class="fa fa-map-marker">&nbsp; Cidades</i></h4>
                                    @foreach($cidades as $atual)
                                            <div class="col-sm-6">
                                                <div class="bs-callout-city lista-cidades"
                                                     style="background-image: url('https://citytips.com.br/images/{!! $atual->slug !!}.jpg') ;background-size: cover; border-color: #f8f8f8;border-width: 3px;">
                                                    <p class="text-center"><a
                                                                href="{!! url('/'.$atual->slug) !!}">{!! $atual->nome !!}</a>
                                                    </p>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer list-options-user">
                        <div class="row">
                            {{--<div class="col-sm-3">--}}
                            {{--<a href="{!! url('user/gerenciador') !!}"><i--}}
                            {{--class="fa fa-puzzle-piece"></i>&nbsp;Minhas--}}
                            {{--contribuições</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-3">--}}
                            {{--<a href="{!! url('user/add-local') !!}"><i class="fa fa-glass"></i>&nbsp;Novo--}}
                            {{--Estabelecimento</a>--}}
                            {{--</div>--}}
                            {{--<div class="col-sm-3">--}}
                            {{--<a href="{!! url('user/criar-evento') !!}"><i class="fa fa-calendar"></i>&nbsp;Criar--}}
                            {{--Evento</a>--}}
                            {{--</div>--}}
                            <div class="col-xs-6">
                                <a href="{!! url('user/gerenciador') !!}"><i
                                            class="fa fa-puzzle-piece"></i>&nbsp;Minhas
                                    contribuições</a>
                            </div>
                            <div class="col-xs-6">
                                <a class="pull-right" target="_blank" href="http://m.me/1398707946825318"><i
                                            class="fa fa-question"></i>&nbsp;Ajuda</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
