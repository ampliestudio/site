@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Adicionando ao cardáprio de <strong>{!! $comercio->nome !!}</strong></div>
                    <div class="panel-body">
                        <small class="pull-right">*Obrigatório</small>

                        <form action="{!! url('user/criar-item-cardaprio') !!}" method="post">
                            <input type='hidden' name='_token' value="{!! csrf_token() !!}">
                            <input type='hidden' name='id' value="{!! \Illuminate\Support\Facades\Crypt::encrypt($comercio->id) !!}">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="nome">Nome*</label>
                                        <div class='text-danger'>{{$errors->first('nome')}}</div>
                                        <input type="text" name="nome" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="descricao">Descrição*</label>
                                        <div class='text-danger'>{{$errors->first('descricao')}}</div>
                                        <input type="text" name="descricao" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="preco">Preço</label>
                                        <div class='text-danger'>{{$errors->first('preco')}}</div>
                                        <input placeholder="R$ 15,50"  type="text" name="preco" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="sel1">Categoria*</label>
                                        <select name="categoria" class="form-control" id="sel1">
                                            @foreach($categorias as $item)
                                                <option value="{!! $item->id !!}">{!! $item->nome !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn-add-comercio btn btn-success">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
