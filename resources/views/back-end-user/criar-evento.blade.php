@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Criar evento</div>

                    <div class="panel-body">
                        @if(Session::has('flash_message'))
                            <div class="alert alert-danger"><span
                                        class="glyphicon glyphicon-exclamation-sign"></span> {!! session('flash_message') !!}<a href="#"
                                                                                                                  class="close"
                                                                                                                  data-dismiss="alert"
                                                                                                                  aria-label="close">&times;</a>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-sm-5">
                                @include('sessoes.modal.modal-importar-facebook')
                            </div>
                            <div class="text-center col-sm-2">
                                <strong>ou</strong>
                            </div>
                            <div class="col-sm-5">
                                <a href="{!! url('user/criar-evento-manual') !!}"
                                   class="btn btn-block btn-default text-center">Criar novo evento</a>
                            </div>
                        </div>
                        <br>
                        <div class="alert alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <strong>O que são eventos no CityTips?</strong>
                            <p>Já se perguntou o que tem pra fazer hoje na sua cidade mas não sabia onde procurar? Bom, a gente quer reunir os principais acontecimentos aqui! Os eventos criados ficam disponíveis na aba '<i class="fa fa-calendar"></i>' (icone de calendário) e podem ser vistos por todos que visitam o site, você pode criar um evento para divulgar o que vai acontecer de especial no seu negócio e atrair mais interesados. Os Eventos com mais recomendações aparecem primeiro.</p>
                        </div>
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <i class="fa fa-thumbs-up"></i>&nbsp;Exemplo de eventos legais para postar
                            <ul>
                                <li>"Promoção! Pratos brasileiros 40% de desconto"</li>
                                <li>"Festa sertaneja na casa de shows"</li>
                                <li>"Festival gastrônomico na praça"</li>
                                <li>"Show de artista famoso no estádio"</li>
                                <li>"Lançamento, sanduiche vegano (desconto pra quem printar este evento!)"</li>
                            </ul>
                        </div>
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <i class="fa fa-thumbs-down"></i>&nbsp;Exemplos de eventos esquisitos aqui
                            <ul>
                                <li>"Minha festa de aniversário"</li>
                                <li>"Jantar em casa"</li>
                                <li>"Estou vendendo meu carro usado"</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer')
@endsection
