@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if(Session::has('flash_message'))
                    <div class="alert alert-info">{!! print_r(session('flash_message')) !!}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Criar evento</div>

                    <div class="panel-body">
                        <script type="text/javascript"
                                src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&key=AIzaSyBd49Kf65vAG2amO_d6nl37AiRyYfJUqHk"></script>

                        <form action="{!! url('user/criar-evento-rapido') !!}" method="post"
                              enctype='multipart/form-data'>
                            <input type='hidden' name='_token' value="{!! csrf_token() !!}">
                            <input type='hidden' name='city' value="{!! $city !!}">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="nome">Nome do Evento</label>
                                        <div class='text-danger'>{{$errors->first('nome')}}</div>
                                        <input type="text" name="nome" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="desc">Descrição</label>
                                        <div class='text-danger'>{{$errors->first('desc')}}</div>
                                        <textarea class="form-control" name="desc" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="local">Onde?
                                </label>
                                <div id="input-vinc">
                                    <input type="text" class="form-control" autocomplete="off"
                                           placeholder="Digite o nome do local e selecione a sugestão" name="local"
                                           id="autocomplete">
                                </div>
                                <input type="hidden" class="local-id" value="0" name="localid">
                                <div id="vinculo-val"></div>
                            </div>
                            <div class="row">

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="control-label">Quando?</label>
                                            <div class='text-danger'>{{$errors->first('inicio')}}</div>
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input placeholder="25/09/2016 18:00" type='text' class="form-control"
                                                       name="inicio"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $(document).ready(function () {
                                                    $('#datetimepicker1').datetimepicker({
                                                        locale: 'pt-br',
                                                        format: 'DD/MM/YYYY HH:mm'
                                                    });
                                                });
                                            });

                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn-add-comercio btn btn-success">Salvar</button>
                            </div>
                        </form>
                        <a href="{!! url('user/criar-evento') !!}">Importar do Facebook</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="input-vinc-st">

    </div>


    <script>
        $(document).ready(function () {

            var locais = [
                    @foreach($lugares as $item)
                {
                    value: "{!! $item->nome !!}", data: "{!! $item->id !!}"
                },
                @endforeach
            ];

            $('#autocomplete').autocomplete({
                lookup: locais,
                onSelect: function (suggestion) {
                    $('.local-id').val(suggestion.data);
                    $('#vinculo-val').append('<p>Vinculado: ' + suggestion.value + ' <button type="button" onclick="removervinc()"><i class="fa fa-trash"></i></button> </p>');
                    $('#input-vinc-st').empty();
                    $('#input-vinc-st').append('<style> #autocomplete{display: none;}.onde-hid{display: none}.cidade-hid{display: none}.mapa-hid{display: none}</style>');
                }
            });
        });
    </script>

@endsection



