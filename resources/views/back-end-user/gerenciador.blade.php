@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Contribuições <a class="pull-right" href="{!! url('user/home') !!}"><i
                                        class="fa fa-chevron-circle-left"></i>&nbsp;
                                <small>Voltar</small>
                            </a></h3>
                    </div>

                    <div class="panel-body">
                        @if(count($comercios)==0 && count($cardaprio)==0 && count($eventos)==0)
                            <div class="col-xs-12">
                                <h4 class="text-center">{{ Auth::user()->name }}, você ainda não deixou sua marca por
                                    aqui</h4>
                            </div><br>
                            <div class="col-xs-12">
                                <h5 class="text-center">Começe <a href="{!! url('user/add-local') !!}"
                                                                  class="btn btn-success">Adicionando um Local</a> ao
                                    CityTips ou <a href="{!! url('user/criar-evento') !!}" class="btn btn-success">Anuncie
                                        um Evento</a> que vai acontecer na sua cidade em algum lugar público ou em algum
                                    bar, balada ou restauarante.</h5>
                                <br>
                                <h4 class="text-center">É Grátis! E você ajuda divulgar os seus lugares favoritos na sua
                                    cidade.</h4>

                                <br><br>
                            </div>
                            <br>
                        @endif
                        <h5>Meus estabelecimentos | <a href="{!! url('user/add-local') !!}">Adicionar
                                Estabelecimento</a></h5>

                        @if(count($comercios)==0)
                            <p>Nada ainda</p>
                        @endif


                        <ul class="list-group">
                            @foreach($comercios as $item)
                                <li class="list-group-item">
                                    <span class="badge <?
                                    if ($item->status == 1) {
                                        echo "badge-wait";
                                    } elseif ($item->status == 3) {
                                        echo "badge-danger";
                                    } else {
                                        echo "badge-success";
                                    }?>">
                                        <?
                                        if ($item->status == 1) {
                                            echo 'Aguardadndo aprovação <i class="fa fa-clock-o"></i>';
                                        } elseif ($item->status == 3) {
                                            echo 'Não aprovado: <a href=""></a>  <i class="fa fa-exclamation-triangle"></i>';
                                        } else {
                                            echo 'Aprovado  <i class="fa fa-check"></i>';
                                        } ?></span>
                                    <div class="btn-group">
                                        <a class="clickable btn btn-default" type="button" data-toggle="dropdown"
                                           aria-haspopup="true"
                                           aria-expanded="false">
                                            <span class="glyphicon glyphicon-chevron-down"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{!! url('tips/suporte/1/1/'.$item->id) !!}"><span
                                                            class="fa fa-remove"></span> Remover</a>
                                            </li>
                                            <li>
                                                <a href="{!! url('tips/suporte/1/2/'.$item->id) !!}"><span
                                                            class="fa fa-edit"></span> Alterar</a>
                                            </li>
                                        </ul>
                                    </div>
                                    &nbsp; {!! $item->nome !!}
                                </li>
                            @endforeach
                        </ul>


                        @if(count($cardaprio)!=0)
                            <hr>
                            <h5>Meus itens de cardáprio</h5>

                        @endif
                        <ul class="list-group">
                            @foreach($cardaprio as $item)
                                <li class="list-group-item">
                                    <span class="badge <?
                                    if ($item->status == 1) {
                                        echo "badge-wait";
                                    } elseif ($item->status == 3) {
                                        echo "badge-danger";
                                    } else {
                                        echo "badge-success";
                                    }?>">
                                        <?
                                        if ($item->status == 1) {
                                            echo 'Aguardadndo aprovação <i class="fa fa-clock-o"></i>';
                                        } elseif ($item->status == 3) {
                                            echo 'Não aprovado: <a href=""></a>  <i class="fa fa-exclamation-triangle"></i>';
                                        } else {
                                            echo 'Aprovado  <i class="fa fa-check"></i>';
                                        } ?></span>
                                    <div class="btn-group">
                                        <a class="clickable btn-default btn" type="button" data-toggle="dropdown"
                                           aria-haspopup="true"
                                           aria-expanded="false">
                                            <span class="glyphicon glyphicon-chevron-down"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{!! url('tips/suporte/2/1/'.$item->id) !!}"><span
                                                            class="fa fa-remove"></span> Remover</a>
                                            </li>
                                            <li>
                                                <a href="{!! url('tips/suporte/2/2/'.$item->id) !!}"><span
                                                            class="fa fa-edit"></span> Alterar</a>
                                            </li>
                                        </ul>
                                    </div>
                                    &nbsp; {!! $item->nome !!} | {!! $item->comercio_nome !!}
                                </li>
                            @endforeach
                        </ul>
                        <hr>
                        <h5>Meus eventos | <a href="{!! url('user/criar-evento') !!}">Adicionar Evento</a></h5>
                        <small>Hey! Contas verificadas não precisam esperar aprovação para seus eventos. Entre em
                            contato em
                            <a target="_blank" href="http://m.me/1398707946825318">nosso canal de ajuda</a> para
                            verificar sua conta.
                        </small>

                        @if(count($eventos)==0)
                            <p>Nada ainda</p>
                        @endif
                        <ul class="list-group">
                            @foreach($eventos as $item)
                                <li class="list-group-item">
                                    <span class="badge <?
                                    if ($item->status == 1) {
                                        echo "badge-wait";
                                    } elseif ($item->status == 3) {
                                        echo "badge-danger";
                                    } else {
                                        echo "badge-success";
                                    }?>">
                                        <?
                                        if ($item->status == 1) {
                                            echo 'Aguardadndo aprovação <i class="fa fa-clock-o"></i>';
                                        } elseif ($item->status == 3) {
                                            echo 'Não aprovado: <a href=""></a>  <i class="fa fa-exclamation-triangle"></i>';
                                        } else {
                                            echo 'Aprovado  <i class="fa fa-check"></i>';
                                        } ?></span>
                                    <div class="btn-group">
                                        <a class="clickable btn-default btn" type="button" data-toggle="dropdown"
                                           aria-haspopup="true"
                                           aria-expanded="false">
                                            <span class="glyphicon glyphicon-chevron-down"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{!! url('tips/suporte/3/1/'.$item->id) !!}"><span
                                                            class="fa fa-remove"></span> Remover</a>
                                            </li>
                                            <li>
                                                <a href="{!! url('tips/suporte/3/2/'.$item->id) !!}"><span
                                                            class="fa fa-edit"></span> Alterar</a>
                                            </li>
                                        </ul>
                                    </div>
                                    &nbsp; {!! $item->nome !!}
                                </li>
                            @endforeach
                        </ul>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
