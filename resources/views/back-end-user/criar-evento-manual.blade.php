@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Criar evento</div>

                    <div class="panel-body">

                        <small class="pull-right">*Obrigatório</small>

                        <script type="text/javascript"
                                src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&key=AIzaSyBd49Kf65vAG2amO_d6nl37AiRyYfJUqHk"></script>

                        <form action="{!! url('user/criar-evento') !!}" method="post" enctype='multipart/form-data'>
                            <input type='hidden' name='_token' value="{!! csrf_token() !!}">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="nome">Nome*</label>
                                        <div class='text-danger'>{{$errors->first('nome')}}</div>
                                        <input type="text" name="nome" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="desc">Descrição*</label>
                                        <div class='text-danger'>{{$errors->first('desc')}}</div>
                                        <textarea class="form-control" name="desc" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="local">Vincule a um local existente no CityTips
                                    <small>(Opcional)</small>
                                </label>
                                <div id="input-vinc">
                                    <input type="text" class="form-control" autocomplete="off"  placeholder="Digite o nome do local e selecione a sugestão" name="local" id="autocomplete">
                                </div>
                                <input type="hidden" class="local-id" value="0" name="localid">
                                <div id="vinculo-val"></div>
                            </div>
                            <div class="row onde-hid">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="endereco">Onde?*</label>
                                        <div class='text-danger'>{{$errors->first('endereco')}}</div>
                                        <input type="text" name="endereco" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 cidade-hid">
                                    <div class="form-group">
                                        <label for="sel1">Cidade*</label>
                                        <select name="cidade" class="form-control" id="sel1">
                                            @foreach($cidades as $item)
                                                <option value="{!! $item->id !!}">{!! $item->nome !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="control-label">Quando?*</label>
                                            <div class='text-danger'>{{$errors->first('inicio')}}</div>
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input placeholder="25/09/2016 18:00" type='text' class="form-control" name="inicio"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $(document).ready(function () {
                                                    $('#datetimepicker1').datetimepicker({
                                                        locale: 'pt-br',
                                                        format: 'DD/MM/YYYY HH:mm'
                                                    });
                                                });
                                            });

                                        </script>
                                    </div>
                                </div>
                            </div>
                            <div class="mapa-hid form-group">
                                <label class="form-group">Selecione a localização do evento no mapa
                                </label>
                                <div id="googleMap" style="height: 400px;"></div>
                                <small>Assim as pessoas podem localizar rapidamente com um GPS</small>

                            </div>

                            <input type='hidden' name='lat' id='lat'>
                            <input type='hidden' name='lng' id='lng'>
                            <div class="col-xs-12">
                                <button type="submit" class="btn-add-comercio btn btn-success">Salvar</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="input-vinc-st">

    </div>



    <script>
        $(document).ready(function () {

            var locais = [
                    @foreach($lugares as $item)
                {
                    value: "{!! $item->nome !!}", data: "{!! $item->id !!}"
                },
                @endforeach
            ];

            $('#autocomplete').autocomplete({
                lookup: locais,
                onSelect: function (suggestion) {
                    $('.local-id').val(suggestion.data);
                    $('#vinculo-val').append('<p>Vinculado: '+suggestion.value+' <button type="button" onclick="removervinc()"><i class="fa fa-trash"></i></button> </p>');
                    $('#input-vinc-st').empty();
                    $('#input-vinc-st').append('<style> #autocomplete{display: none;}.onde-hid{display: none}.cidade-hid{display: none}.mapa-hid{display: none}</style>');
                }
            });
        });

        function removervinc(){
            $('#input-vinc-st').empty();
            $('#input-vinc-st').append('<style> #autocomplete{display: block;}.onde-hid{display: block}.cidade-hid{display: block}.mapa-hid{display: block}</style>');
            $('#vinculo-val').empty();
            $('#autocomplete').val('');
            $('.local-id').val('0');
        }


        function initialize() {
            var myLatlng = new google.maps.LatLng(-23.408742, -51.443850);
            var mapProp = {
                center: myLatlng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP

            };

            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Selecione o local exato do estabelecimento no mapa',
                draggable: true
            });
            document.getElementById('lat').value = -23.408742
            document.getElementById('lng').value = -51.443850

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };

                    document.getElementById('lat').value = position.coords.latitude
                    document.getElementById('lng').value = position.coords.longitude

//                    alert(geocodePosition(marker.getPosition()))

                    marker.setPosition(pos);
                    map.setCenter(pos);
                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            }

            // marker drag event
            google.maps.event.addListener(marker, 'drag', function (event) {
                document.getElementById('lat').value = event.latLng.lat();
                document.getElementById('lng').value = event.latLng.lng();
            });

            //marker drag event end
            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById('lat').value = event.latLng.lat();
                document.getElementById('lng').value = event.latLng.lng();
//                alert("lat=>"+event.latLng.lat());
//                alert("long=>"+event.latLng.lng());
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

@endsection



