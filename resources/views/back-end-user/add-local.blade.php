@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Adicionar local</div>
                    <div class="panel-body">


                        <script type="text/javascript"
                                src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&key=AIzaSyBd49Kf65vAG2amO_d6nl37AiRyYfJUqHk"></script>

                        <small class="pull-right">*Obrigatório</small>
                        <form action="{!! url('user/add-local') !!}" method="post" enctype='multipart/form-data'>
                            <input type='hidden' name='_token' value="{!! csrf_token() !!}">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="nome">Nome*</label>
                                        <div class='text-danger'>{{$errors->first('nome')}}</div>
                                        <input type="text" name="nome" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="image">Logo do estabelecimento*</label>
                                        <p>
                                            <small>-A imagem deve ser <strong>quadrada</strong> e não ultrapassar 200kb</small>
                                        </p>
                                        <input type="file" name="image">
                                        <div class='text-danger'>{{$errors->first('image')}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="endereco">Endereço*</label>
                                        <div class='text-danger'>{{$errors->first('endereco')}}</div>
                                        <input type="text" name="endereco" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="form-group">
                                        <label for="site">Site ou Facebook <small>separar por linha</small></label>
                                        <textarea type="text" name="site" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="entrega">Importar automaticamente eventos do facebook</label>
                                        <div class='text-danger'>{{$errors->first('fbimport')}}</div>
                                        <br>
                                        <input type="radio" name="import" checked value="1"> Sim
                                        <input type="radio" name="import" value="0"> Não
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="telefone">Telefone*</label>
                                        <div class='text-danger'>{{$errors->first('telefone')}}</div>
                                        <input type="text" name="telefone" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="horario">Horário de funcionamento*</label>
                                        <div class='text-danger'>{{$errors->first('horario')}}</div>
                                        <input type="text" name="horario" placeholder="ex: Seg-Qui: 18:00 às 23:00 Sex-Sáb: 18:00 às 00:00" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="entrega">Faz entrega?*</label>
                                        <div class='text-danger'>{{$errors->first('entrega')}}</div>
                                        <br>
                                        <input type="radio" name="entrega" value="1"> Sim
                                        <input type="radio" name="entrega" value="0"> Não
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="sel1">Cidade*</label>
                                        <select name="cidade" class="form-control" id="sel1">
                                            @foreach($cidades as $item)
                                                <option value="{!! $item->id !!}">{!! $item->nome !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="sel1">Categoria*</label>
                                        <select name="tipo" class="form-control" id="sel1">
                                            <option value="1">Refeição</option>
                                            <option value="2">Happy-Hour</option>
                                            <option value="3">Diversão(Balada)</option>
                                            <option disabled>──────────</option>
                                            <option value="12">Refeição e Happy-Hour</option>
                                            <option value="23">Diversão(Balada) e Happy-Hour</option>
                                            <option value="13">Diversão(Balada) e Refeição</option>
                                            <option disabled>──────────</option>
                                            <option value="123">Todas as Categorias</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-group">Selecione a localização do estabelecimento no mapa*</label>
                                <div id="googleMap" style="height: 400px;"></div>
                            </div>
                            <input type='hidden' name='lat' id='lat'>
                            <input type='hidden' name='lng' id='lng'>
                            <br>
                            <div class="alert alert-info" role="alert">Após adicionar um local, ele estará
                                sujeito a
                                aprovação antes de aparecer no site. Acompanhe o status em <a href="{!! url('user/gerenciador') !!}" class=""><i class="fa fa-puzzle-piece"></i>&nbsp;Minhas
                                    contribuições</a>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn-add-comercio btn btn-success">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function initialize() {
            var myLatlng = new google.maps.LatLng(-23.408742, -51.443850);
            var mapProp = {
                center: myLatlng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP

            };

            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Selecione o local exato do estabelecimento no mapa',
                draggable: true
            });
            document.getElementById('lat').value = -23.408742
            document.getElementById('lng').value = -51.443850

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };

                    document.getElementById('lat').value = position.coords.latitude
                    document.getElementById('lng').value = position.coords.longitude

//                    alert(geocodePosition(marker.getPosition()))

                    marker.setPosition(pos);
                    map.setCenter(pos);
                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            }

            // marker drag event
            google.maps.event.addListener(marker, 'drag', function (event) {
                document.getElementById('lat').value = event.latLng.lat();
                document.getElementById('lng').value = event.latLng.lng();
            });

            //marker drag event end
            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById('lat').value = event.latLng.lat();
                document.getElementById('lng').value = event.latLng.lng();
//                alert("lat=>"+event.latLng.lat());
//                alert("long=>"+event.latLng.lng());
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    @include('layouts.footer')

@endsection
