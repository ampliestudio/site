@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Configurar conta <a class="pull-right"
                                                                    href="{!! url('user/home') !!}"><i
                                        class="fa fa-chevron-circle-left"></i>&nbsp;
                                <small>Voltar</small>
                            </a></h3>
                    </div>
                    @if(Session::has('flash_message'))
                        <div class="alert alert-danger"><span
                                    class="glyphicon glyphicon-exclamation-sign"></span> {!! session('flash_message') !!}<a href="#"
                                                                                                                            class="close"
                                                                                                                            data-dismiss="alert"
                                                                                                                            aria-label="close">&times;</a>
                        </div>
                    @endif
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center"><img class="img-responsive img-circle"
                                                                                alt="User Pic"
                                                                                src="{!! $user->foto !!}">
                            </div>
                            <div class=" col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>

                                    <tr>
                                        <td>Nome</td>
                                        <td>
                                            {!! Auth::user()->name !!}
                                            <a data-toggle="modal" data-target="#modalNome" class="pull-right"><i
                                                        class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>
                                            {!! Auth::user()->email !!}
                                            {{--<a onclick=""><a data-toggle="modal" data-target="#modalEmail"--}}
                                                             {{--class="pull-right"><i--}}
                                                            {{--class="fa fa-pencil"></i></a></a>--}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Notificações por email</td>
                                        <td>
                                            <a href="{!! url('user/email-notificacao') !!}" class="btn btn-xs <?= Auth::user()->email_status ? "btn-success" : "btn-danger" ?> "><?= Auth::user()->email_status ? "Ligado" : "Desligado" ?></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Telefone</td>
                                        <td>
                                            <?= Auth::user()->telefone ? Auth::user()->telefone : 'Insira seu telefone' ?>
                                            <a data-toggle="modal" data-target="#modalTelefone" class="pull-right"><i
                                                            class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                {{--<a href="#" class="btn btn-primary">My Sales Performance</a>--}}
                                {{--<a href="#" class="btn btn-primary">Team Sales Performance</a>--}}
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <small>
                            <a data-toggle="modal" data-target="#myModal">
                                Encerrar minha conta
                            </a>
                        </small>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Você deixará saudades...</h4>
                                    </div>
                                    <div class="modal-body">
                                        Tem certeza que quer encerrar sua conta CityTips.com.br? Este processo não
                                        poderá ser revertido.
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar
                                        </button>
                                        <form action="{!! url('user/desativar-conta') !!}" method="post">
                                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                            <input type="hidden" name="id"
                                                   value="{!! \Illuminate\Support\Facades\Auth::id() !!}">
                                            <button class="btn fix-desativar-conta btn-danger">Desativar
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--modal nome--}}

                        <div class="modal fade" id="modalNome" tabindex="-1" role="dialog" aria-labelledby="modalNome"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h5 class="modal-title" id="myModalLabel">Alterar nome</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="{!! url('user/name-changer') !!}">
                                            <small>O nome só pode ser alterado uma vez a cada 60 dias</small>
                                            <br>
                                            <small>O novo nome deve estar de acordo com os termos de CityTips</small>
                                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                            <input type="text" name="name" class="form-control" value="{!! Auth::user()->name !!}">
                                            <br>
                                            <input type="submit" class="btn btn-primary" value="salvar">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{--modal email--}}

                        <div class="modal fade" id="modalEmail" tabindex="-1" role="dialog" aria-labelledby="modalEmail"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel"></h4>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="{!! url('user/email-changer') !!}">
                                            <small>Um email de confirmação será enviado para seu novo email para confirmar a mudança</small>
                                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                            <input type="text" name="email" class="form-control" value="{!! Auth::user()->email !!}">
                                            <br>
                                            <input type="submit" class="btn btn-primary" value="salvar">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--modal telefone--}}

                        <div class="modal fade" id="modalTelefone" tabindex="-1" role="dialog" aria-labelledby="modalTelefone"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h5 class="modal-title" id="myModalLabel">Alterar telefone</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="{!! url('user/telefone-changer') !!}">
                                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                            <input type="text" name="telefone" placeholder="(00) 0000 - 0000" class="form-control" value="{!! Auth::user()->telefone !!}">
                                            <br>
                                            <input type="submit" class="btn btn-primary" value="salvar">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
