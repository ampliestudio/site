@extends('layouts.app')
@section('content')

    <br><br>
    <div class="container-fluid">
        <div class="col-md-offset-1 col-md-10">

            <div class="jumbotron">
                <h1 class="text-center" style="color: white">Uau! Você encontrou uma página de teste! <i class="fa fa-fighter-jet"></i></h1>
                <br><br>
                <p class="text-justify" style="color: white; font-size: 12pt">
                    Bom.. isso é tudo por enquanto, ou você pode clicar <a href="{!! url("/") !!}" style="color: white;;"><strong>aqui.</strong></a>
                </p>
            </div>
        </div>
    </div>
    <br><br><br>
    <div class="container">
        <div style="background-color: rgb(238, 238, 238); border-radius: 5px; padding: 10px">
            <iframe class="costumer-capture-frame" src="http://costumer.dev/main" style="width: 100%;" frameborder="0" scrolling="no" onload="resizeIframe(this)">
                <p>Seu navegador não é compatível com nosso site</p>
            </iframe>
        </div>
    </div>

    <script>
        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }
    </script>

    @include('layouts.footer')
@endsection

