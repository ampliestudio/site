<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ComerciosCardaprioCategoria extends Authenticatable
{

    protected $fillable = [
        'nome'
    ];

    protected $table = 'comercios_cardaprio_categoria';

    protected $hidden = [

    ];
}
