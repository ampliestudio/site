<?php

namespace CityTips;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class Eventos extends Authenticatable
{

    protected $fillable = [
        'nome', 'descricao', 'user_id', 'inicio', 'local', 'status', 'score_count', 'slug_id', 'link_fb', 'cidade_id'
    ];

    protected $table = 'eventos';

    protected $hidden = [

    ];


    public static function getEventosUmOficialPrimeiro($cidade_id)
    {
        $return = Eventos::where('eventos.cidade_id', $cidade_id)
            ->whereDate('inicio', '=', Carbon::today()->toDateString())
            ->where('eventos.status', 2)
            ->where('tipo', 1)
            ->orderBy('score_count', 'desc')
            ->orderBy('created_at', 'desc')
            ->first();
        return $return;
    }

    public static function getEventosHoje($cidade_id)
    {
        $return = Eventos::where('eventos.cidade_id', $cidade_id)
            ->whereDate('inicio', '=', Carbon::today()->toDateString())
            ->where('eventos.status', 2)
            ->orderBy('score_count', 'desc')
            ->orderBy('created_at', 'desc')
            ->get();
        return $return;
    }

    public static function getEventosFuturos($cidade_id)
    {
        $return = Eventos::where('eventos.cidade_id', $cidade_id)
            ->whereDate('inicio', '>', Carbon::today()->toDateString())
            ->where('eventos.status', 2)
            ->orderBy('inicio', 'asc')
            ->orderBy('score_count', 'desc')
            ->get();
        return $return;
    }

    public static function getEventosPassados($cidade_id)
    {
        $return = Eventos::where('eventos.cidade_id', $cidade_id)
            ->whereDate('inicio', '<', Carbon::today()->toDateString())
            ->where('eventos.status', 2)
            ->orderBy('inicio', 'desc')
            ->limit(7)
            ->get();
        return $return;
    }

    public static function queryArrayDeEventos($array)
    {
        $array_fix = implode(',', $array);
        //verifica se não tem eventos!
        if(count($array)==0){
            return $array;
        }

        $return = Eventos::whereIn('eventos.id', $array)
            ->leftJoin('comercios', 'eventos.id_local', '=', 'comercios.id')
            ->join('users', 'eventos.user_id', '=', 'users.id')
            ->select('eventos.*', 'users.foto as user_image', 'users.name as user_name', 'comercios.slug as local_slug', 'comercios.foto as local_foto', 'comercios.nome as local_nome', 'comercios.alias as local_alias')
            ->orderByRaw("FIELD(eventos.id,$array_fix)")
            ->get();
        return $return;
    }
//mesma de cima só que pra pagina single
    public static function queryArrayDeEventosSingle($array)
    {
        $array_fix = implode(',', $array);

        $return = Eventos::whereIn('eventos.id', $array)
            ->leftJoin('comercios', 'eventos.id_local', '=', 'comercios.id')
            ->join('users', 'eventos.user_id', '=', 'users.id')
            ->select('eventos.*', 'users.foto as user_image', 'users.name as user_name', 'comercios.slug as local_slug', 'comercios.foto as local_foto', 'comercios.nome as local_nome', 'comercios.alias as local_alias')
            ->orderByRaw("FIELD(eventos.id,$array_fix)")
            ->limit(4)
            ->get();
        return $return;
    }

    public static function algoritmoDeEventos($cidade_id)
    {
        $eventos_id_array = [];
        $first = Eventos::getEventosUmOficialPrimeiro($cidade_id);
        if (isset($first)) {
            $eventos_id_array[] = $first->id;
        }
        date_default_timezone_set('America/Sao_Paulo');
        Carbon::setLocale('pt_BR');
        $eventos = Eventos::getEventosHoje($cidade_id);
//cria um array com os id s de eventos
        $user_id = 0;
        $prioridade_baixa_user_flood = [];
        foreach ($eventos as $item) {
            if (isset($first)) {
                if ($first->id == $item->id) {
                    continue;
                }
            }

            //lida com usuários que postam muita coisa e coloca pra baixo:
            if ($item->tipo == 2) {
                if ($user_id == $item->user_id) {
                    $prioridade_baixa_user_flood[] = $item->id;
                    continue;
                } else {
                    $user_id = $item->user_id;
                }
            }

            $eventos_id_array[] = $item->id;
        }
        if (isset($prioridade_baixa_user_flood)) {
            foreach ($prioridade_baixa_user_flood as $item) {
                $eventos_id_array[] = $item;
            }
        }
        $eventos = Eventos::getEventosFuturos($cidade_id);
//cria um array com os id s de eventos
        foreach ($eventos as $item) {
            $eventos_id_array[] = $item->id;
        }

        $eventos = Eventos::getEventosPassados($cidade_id);
        foreach ($eventos as $item) {
            $eventos_id_array[] = $item->id;
        }
//seleciona di banco os eventos dentro do array na ordem certa de prioridades

        return $eventos_id_array;
    }



//    ATUALIZAÇÃO DE EVENTOS EM CRON

    public static function atualizarCidade($id)
    {
        $locais = Comercios::where('cidade_id', $id)->get();
        $id_comercio_array = [];
        foreach ($locais as $local) {
            $id_comercio_array[] = $local->id;
        }

        date_default_timezone_set('America/Sao_Paulo');

        if (Auth::id() != 4) {
            return Redirect::to(URL::previous());
        }
        $paginas = FacebookPages::whereIn('comercio_id', $id_comercio_array)->get();

        $resultado = [];
        foreach ($paginas as $pagina) {
            $user = User::find(4);
            $json = file_get_contents("https://graph.facebook.com/v2.8/" . $pagina->link . "?fields=events%7Bstart_time%2Cdescription%2Cplace%2Cname%2Ctype%7D&access_token=" . $user->password);
            $dados = json_decode($json);
            if (isset($dados->events)) {
                $resultado[] = '--------importando: ' . $pagina->nome . '<br>';


                foreach ($dados->events->data as $objeto_evento) {

                    if ($objeto_evento->type != 'public') {
                        //evento não é público
                        $resultado[] = 'evento não é publico não importado: ' . $objeto_evento->name . '<br>';
                        continue;
                    }
                    $descricao = '';
                    if (isset($objeto_evento->description)) {
                        $descricao = $objeto_evento->description;
                    }
                    $nome = $objeto_evento->name;

                    $inicio = $objeto_evento->start_time;

                    if ($inicio < Carbon::today()) {
//                            $resultado[] = 'evento antigo não importado: ' . $nome . '<br>';
                        continue;
                    }

                    $inicio = strtotime($inicio);


                    $id = $objeto_evento->id;
                    try {
                        $comercio = Comercios::find($pagina->comercio_id);
                    } catch (\Exception $e) {
                        abort(500);
//                    echo 'erro não tem esse comercio';
                    }

                    $lugar = $comercio->nome;
                    $lat = $comercio->lat;
                    $lon = $comercio->lon;
                    $cidade = $comercio->cidade_id;

                    $salvo = Eventos::salvarEvento($descricao, $nome, $lugar, $inicio, $lat, $lon, 'importacao automatica', $id, $cidade, $pagina->comercio_id);
                    if ($salvo) {
                        $resultado[] = $lugar . ' sucesso: ' . $nome . '<br>';

                    }
                }
            } else {
                $resultado[] = ' sem eventos: ' . $pagina->nome . '<br>';

                continue;
            }
        }
    }
    public static function salvarEvento($descricao, $nome, $lugar, $inicio, $lat, $lon, $link, $id, $cidade, $idlocal)
    {

        //se existe, atualiza evento (só para importação em massa)
        $if = Eventos::where('slug_id', $id)->get();
        if (count($if) > 0) {
            //evento já criado, atualiza
            try {
                $evento = Eventos::where('slug_id', $id)->first();
                $evento->nome = $nome;
                $evento->descricao = $descricao;
                $evento->local = $lugar;
                $evento->inicio = date('Y-m-d H:i:s', $inicio);
                $evento->lat = $lat;
                $evento->lon = $lon;
                $evento->link_fb = $link;
                $evento->slug_id = $id;
                $evento->id_local = $idlocal;
                $evento->user_id = Auth::id();
                $evento->cidade_id = $cidade;
                $evento->status = 2; // todo: autorizando eventos para todos :x
                if (Auth::user()->role == 1) {
                    $evento->status = 2;
                }
                $evento->save();
                return true;
            } catch (\Exception $e) {
                dd($e);
                return false;
            }
        }
        //fim atulização

        try {
            $evento = new Eventos();
            $evento->nome = $nome;
            $evento->descricao = $descricao;
            $evento->local = $lugar;
            $evento->inicio = date('Y-m-d H:i:s', $inicio);
            $evento->lat = $lat;
            $evento->lon = $lon;
            $evento->link_fb = $link;
            $evento->slug_id = $id;
            $evento->id_local = $idlocal;
            $evento->user_id = Auth::id();
            $evento->cidade_id = $cidade;
            if (Auth::user()->role == 1) {
                $evento->status = 2;
            }
            $evento->save();
            return true;
        } catch (\Exception $e) {
            dd($e);
            return false;
        }

    }

}
