<?php

namespace CityTips\Console;

use CityTips\Eventos;
use CityTips\Suporte;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Auth\User;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \CityTips\Console\Commands\Inspire::class

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {


        $schedule->call(function () {
//            $suporte = new Suporte();
//            $suporte->texto = 'oi';
//            $suporte->user_id = 'oi';
//            $suporte->nome = 'oi';
//            $suporte->email = 'oi';
//            $suporte->info = 'oi';
//            $suporte->save();

//            $user = User::find(4);

            $users = User::where('email_status',1)->get();
            foreach ($users as $user) {
                $cidade = \CityTips\Cidades::where('slug', $user->ultima_cidade)->first();
                $ev = Eventos::algoritmoDeEventos($cidade->id);
                $data = Eventos::queryArrayDeEventos($ev);

                Mail::send('auth.emails.semanario', ['user' => $user, 'data' => $data], function ($m) use ($user) {
                    $m->from('staff@citytips.com', 'CityTips');

                    $m->to($user->email, $user->name)->subject('Agenda do fim de semana!');
                });
            }
        })->weeklyOn(4, '03:00');
//        })->weeklyOn(1, '14:05');
    }
}


