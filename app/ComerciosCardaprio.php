<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ComerciosCardaprio extends Authenticatable
{

    protected $fillable = [
        'user_id', 'categoria_id', 'comercio_id', 'status', 'preco', 'descricao', 'slug', 'nome'
    ];

    protected $table = 'comercios_cardaprio';

    protected $hidden = [

    ];
}
