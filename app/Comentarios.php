<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Comentarios extends Authenticatable
{

    protected $fillable = [
        'user_id', 'titulo', 'avaliacao', 'nota', 'comercio_id'
    ];

    protected $table = 'comentarios';

    protected $hidden = [

    ];
}
