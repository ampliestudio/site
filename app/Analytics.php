<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Analytics extends Authenticatable
{

    protected $fillable = [
        'user_id', 'request'
    ];

    protected $table = 'analytics';

    protected $hidden = [

    ];
}
