<?php

namespace CityTips;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsersCidadeHistorico extends Authenticatable
{

    protected $fillable = [
        'user_id', 'cidade_id'
    ];
    protected $table = 'users_cidade_historico';


}
