<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Conf extends Authenticatable
{

    protected $fillable = [
        'desc', 'value',
    ];

    protected $table = 'conf';

    protected $hidden = [

    ];
}
