<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Cidades extends Authenticatable
{

    protected $fillable = [
        'nome', 'slug'
    ];

    protected $table = 'cidades';

    protected $hidden = [

    ];
}
