<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Suporte extends Authenticatable
{

    protected $fillable = [
        'email', 'nome', 'user_id', 'texto',
    ];

    protected $table = 'suporte';

    protected $hidden = [

    ];
}
