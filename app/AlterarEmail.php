<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class AlterarEmail extends Authenticatable
{

    protected $fillable = [
        'email', 'user_id'
    ];

    protected $table = 'alterar_email';

    protected $hidden = [

    ];
}
