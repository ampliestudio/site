<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class EventoScore extends Authenticatable
{

    protected $fillable = [
        'user_id', 'evento_id', 'recomendado'
    ];

    protected $table = 'eventos_score';

    protected $hidden = [

    ];
}
