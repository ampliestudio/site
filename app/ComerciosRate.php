<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ComerciosRate extends Authenticatable
{

    protected $fillable = [
        'user_id', 'comercio_id', 'recomendado'
    ];

    protected $table = 'comercios_rate';

    protected $hidden = [

    ];
}
