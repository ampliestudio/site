<?php

namespace CityTips\Http\Controllers;

use Carbon\Carbon;
use CityTips\Cidades;
use CityTips\Comercios;
use CityTips\Comentarios;
use CityTips\ComerciosRate;
use CityTips\EventoScore;
use Illuminate\Http\Request;

use CityTips\Http\Requests;

class ComerciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create($id)
//    {
//
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request)
//    {
//        //
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        date_default_timezone_set('America/Sao_Paulo');

        $comercio = Comercios::where('comercios.slug',$id)
            ->join('users','comercios.user_id','=','users.id')
            ->join('cidades','comercios.cidade_id','=','cidades.id')
            ->select('comercios.*','users.name as user_name', 'cidades.nome as cidade_nome')
            ->first();

        try{
            $rate = ComerciosRate::where('comercio_id',$comercio->id)->where('recomendado', 1)->get();
        }catch(\Exception $e){
            abort(404);
        }

        $eventos = \CityTips\Eventos::where('id_local', $comercio->id)
//            ->whereDate('inicio', '>=', Carbon::today()->toDateString())
            ->join('users', 'eventos.user_id', '=', 'users.id')
            ->select('eventos.*', 'users.foto as user_image')
            ->orderBy('inicio','desc')
            ->where('eventos.status',2)
            ->paginate(4);

        $avaliacoes = Comentarios::where('comercio_id', $comercio->id)
            ->join('users','comentarios.user_id','=','users.id')
            ->select('comentarios.*','users.foto as userimg','users.name as username')
            ->orderBy('updated_at', 'desc')
            ->paginate(10);

        $score = 0;
        $eventos_id_array[] = 0;
        foreach ($eventos as $item) {
            $eventos_id_array[] = $item->id;
        }
        $score = EventoScore::whereIn('evento_id', $eventos_id_array)->where('recomendado', 1)->get();

        $capa = Comercios::retornaCapa($comercio->id);

        if(count($comercio)==1){
            return view('sessoes/comercios-single')
                ->withComercio($comercio)
                ->withEventos($eventos)
                ->withRate($rate)
                ->withCapa($capa)
                ->withScore($score)
                ->withAvaliacoes($avaliacoes)
                ->withCidade(Cidades::find($comercio->cidade_id))
                ->withCidades(\CityTips\Cidades::all());
        }else{
//não existe a cidade digitada
            abort(404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //        função para retorno ajax dos eventos no scroll infinito
    public function edit($id)
    {
        date_default_timezone_set('America/Sao_Paulo');

        $comercio = Comercios::where('comercios.id',$id)
            ->join('users','comercios.user_id','=','users.id')
            ->join('cidades','comercios.cidade_id','=','cidades.id')
            ->select('comercios.*','users.name as user_name', 'cidades.nome as cidade_nome')
            ->first();
        $eventos = \CityTips\Eventos::where('id_local', $comercio->id)
//            ->whereDate('inicio', '>=', Carbon::today()->toDateString())
            ->join('users', 'eventos.user_id', '=', 'users.id')
            ->select('eventos.*', 'users.foto as user_image')
            ->orderBy('inicio','desc')
            ->where('eventos.status',2)
            ->paginate(4);

        $score = 0;
        $eventos_id_array[] = 0;
        foreach ($eventos as $item) {
            $eventos_id_array[] = $item->id;
        }
        $score = EventoScore::whereIn('evento_id', $eventos_id_array)->where('recomendado', 1)->get();

        $view = view('sessoes/sub-comercio/comercios-feed-ajax')
            ->withScore($score)
            ->withComercio($comercio)
            ->withEventos($eventos);
        $contents = (string) $view;
        echo $contents;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, $id)
//    {
//        //
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function destroy($id)
//    {
//        //
//    }
}
