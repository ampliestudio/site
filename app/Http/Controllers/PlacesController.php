<?php

namespace CityTips\Http\Controllers;

use Carbon\Carbon;
use CityTips\Cidades;
use CityTips\Comercios;
use CityTips\Comentarios;
use CityTips\ComerciosRate;
use CityTips\Conf;
use CityTips\EventoScore;
use CityTips\User;
use CityTips\UsersCidadeHistorico;
use Illuminate\Http\Request;

use CityTips\Http\Requests;
use Illuminate\Support\Facades\Auth;

class PlacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create($id)
//    {
//
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request)
//    {
//        //
//    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($cidade)
    {
        try {
            date_default_timezone_set('America/Sao_Paulo');
            $atual = Cidades::where('slug', $cidade)->first();
            //coloca a cidade como a ultima do usuário

            if (Auth::check()) {
                $user = User::find(Auth::id());
                $user->ultima_cidade = $atual->slug;
                $user->save();
                $user_cidade_historico = new UsersCidadeHistorico();
                $user_cidade_historico->user_id = Auth::id();
                $user_cidade_historico->cidade_id = $atual->id;
                $user_cidade_historico->save();
            }

            $comercios = Comercios::comerciosDaCidade($atual->id);

            $comercios_id_array = [];
//cria um array com os id s de comércios
            foreach ($comercios as $item) {
                $comercios_id_array[] = $item->id;
            }
//retorna as avaliações dos comercios que estão no array
            $rate = ComerciosRate::whereIn('comercio_id', $comercios_id_array)->where('recomendado', 1)->get();

//----------------fim da parte dos comércios--------------------
            return view('sessoes/cidade-home-locais')
                ->withConf(Conf::all())
                ->withCidade($atual)
                ->withRate($rate)
                ->withCidades(Cidades::orderBy('nome', 'asc')->get())
                ->withComercios($comercios)
                ->withLugares(Comercios::where('status', 2)->where('cidade_id', $atual->id)->get());
        } catch (Exception $e) {
//não existe a cidade digitada
//            dd($e);
            abort(404);
        }

    }

}
