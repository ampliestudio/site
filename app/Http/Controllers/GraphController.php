<?php

namespace CityTips\Http\Controllers;


use CityTips\Cidades;
use CityTips\Comercios;
use CityTips\ComerciosRate;
use CityTips\Eventos;
use CityTips\EventoScore;
use CityTips\Http\Requests;
use Exception;
use Validator;
use CityTips\UsersCidadeHistorico;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;


class GraphController extends Controller
{

    function getEvents(){


        //    Parte dos comércios

        $cidade = "londrina";

        try {
            $atual = Cidades::where('slug', $cidade)->first();
            //coloca a cidade como a ultima do usuário

            if (Auth::check()) {
                $user = User::find(Auth::id());
                $user->ultima_cidade = $atual->slug;
                $user->save();
                $user_cidade_historico = new UsersCidadeHistorico();
                $user_cidade_historico->user_id = Auth::id();
                $user_cidade_historico->cidade_id = $atual->id;
                $user_cidade_historico->save();
            }

            $comercios = Comercios::comerciosDaCidade($atual->id);

            $comercios_id_array = [];
//cria um array com os id s de comércios
            foreach ($comercios as $item) {
                $comercios_id_array[] = $item->id;
            }
//retorna as avaliações dos comercios que estão no array
            $rate = ComerciosRate::whereIn('comercio_id', $comercios_id_array)->where('recomendado', 1)->get();

//----------------fim da parte dos comércios--------------------


//algoritimo de ordenação dos eventos por relevância:
//eventos selecionados são postos na ordem neste array
            $eventos_id_array = Eventos::algoritmoDeEventos($atual->id);
            //seleciona os eventos apartir do array
//            dd($eventos_id_array);
            $eventos = Eventos::queryArrayDeEventos($eventos_id_array);

            //retorna as avaliações dos eventos que estão no array
            $score = EventoScore::whereIn('evento_id', $eventos_id_array)->where('recomendado', 1)->get();

//fim da parte dos eventos
            $carousel = Comercios::slideShowPosts($atual->id);


        } catch (Exception $e) {
//não existe a cidade digitada
//            dd($e);
            abort(404);
        }

//Gerador de JSON

        $json = '[';
        $loopCount = 0;
        foreach($eventos as $event){
            if($loopCount>0){$json.=',';}
            $loopCount ++;
            $json.='{';
            $json.= '"id":'.json_encode($event->id).',';
            $json.= '"name":'.json_encode($event->nome).',';
            $json.= '"time":'.json_encode($event->inicio).',';
            $json.= '"description":'.json_encode($event->descricao).',';
            $json.= '"local":'.json_encode($event->local).',';
            if($event->local_foto == null){
                $json.= '"picture":"'.asset('uploads/alias-image.png').'"';
            }else{
                $json.= '"picture":"'.asset('uploads/'.$event->local_foto).'"';
            }
            $json.='}';
        }
        $json.= ']';
        echo $json;

    }

}