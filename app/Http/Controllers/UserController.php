<?php

namespace CityTips\Http\Controllers;


use Carbon\Carbon;
use CityTips\Alias;
use CityTips\AlterarEmail;
use CityTips\Cidades;
use CityTips\Comercios;
use CityTips\Comentarios;
use CityTips\ComerciosCardaprio;
use CityTips\ComerciosCardaprioCategoria;
use CityTips\ComerciosRate;
use CityTips\Eventos;
use CityTips\EventoScore;
use CityTips\FacebookPages;
use CityTips\Http\Requests;
use CityTips\Status;
use CityTips\Suporte;
use CityTips\User;
use CityTips\UsersLoginLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Mockery\CountValidator\Exception;
use Validator;


class UserController extends Controller
{

    private $view_path = '/back-end-user';

    private $messages = [
        'image.required' => 'A imagem é obrigatória',
        'image.dimensions' => 'A imagem deve ser quadrada',
        'required' => 'Este campo é obrigatório',
        'image.image' => 'Formato não permitido',
        'image.max' => 'A imagem é muito grande',
    ];

    private function userLog()
    {
        $log = new UsersLoginLog();
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $log->ip = $ip;
        $log->user_id = Auth::id();
        $log->html_request = serialize(Input::all());
        $log->save();
    }

    public function __construct()
    {
        $this->middleware('auth');
        if (Auth::check()) {
//            $this->userLog();
        }
    }

    public function getHome()
    {
        //verifica se a pessoa contribuiu com cmoercios
        $comercios = Comercios::where('user_id', Auth::id())
            ->orderBy('updated_at', 'desc')
            ->get();

        $comercio = false;
        $cc = count($comercios);
        if ($cc > 0) {
            $comercio = true;
        }

        //verifica se a pessoa contribuiu com itens de cardáprio
        $itens_cardaprio = ComerciosCardaprio::where('comercios_cardaprio.user_id', Auth::id())
            ->join('comercios', 'comercios_cardaprio.comercio_id', '=', 'comercios.id')
            ->select('comercios_cardaprio.*', 'comercios.nome as comercio_nome')
            ->orderBy('updated_at', 'desc')
            ->get();

        $cardaprio = false;
        $cc = count($comercios);
        if ($cc > 0) {
            $cardaprio = true;
        }

        //retorna os comercios recomendados pela pessoa logada
        $array_dos_comercios_recomendados = [];
        $id_comercios_recomendados = ComerciosRate::where('user_id', Auth::id())->where('recomendado', 1)->get();
        foreach ($id_comercios_recomendados as $item) {
            $array_dos_comercios_recomendados[] = $item->comercio_id;
        }

        $recomendados = Comercios::whereIn('id', $array_dos_comercios_recomendados)->get();

        //retorna os eventos marcados

        $array_dos_eventos_marcados = [];
        $id_dos_eventos_marcados = EventoScore::where('user_id', Auth::id())->where('recomendado', 1)->get();
        foreach ($id_dos_eventos_marcados as $item) {
            $array_dos_eventos_marcados[] = $item->evento_id;
        }
        $marcados = Eventos::whereIn('id', $array_dos_eventos_marcados)
            ->orderBy('inicio', 'asc')
            ->get();

        return view($this->view_path . '/home')
            ->withConf(\CityTips\Conf::all())
            ->withComercio($comercio)
            ->withCardapri($cardaprio)
            ->withRecomendados($recomendados)
            ->withCidades(\CityTips\Cidades::all())
            ->withUser(User::find(Auth::id()))
            ->withMarcados($marcados);
    }

    public function getGerenciador()
    {
        $comercios = Comercios::where('user_id', Auth::id())
            ->orderBy('updated_at', 'desc')
            ->get();

        $itens_cardaprio = ComerciosCardaprio::where('comercios_cardaprio.user_id', Auth::id())
            ->join('comercios', 'comercios_cardaprio.comercio_id', '=', 'comercios.id')
            ->select('comercios_cardaprio.*', 'comercios.nome as comercio_nome')
            ->orderBy('updated_at', 'desc')
            ->get();

        $eventos = Eventos::where('user_id', Auth::id())
            ->orderBy('updated_at', 'desc')
            ->get();

        return view($this->view_path . '/gerenciador')
            ->withConf(\CityTips\Conf::all())
            ->withEventos($eventos)
            ->withComercios($comercios)
            ->withCardaprio($itens_cardaprio);
    }

    public function getRateAjax(Request $request)
    {


        $id = $request->id;


        $rate = ComerciosRate::where('user_id', Auth::id())->where('comercio_id', $id)->first();
        $comercio = Comercios::find($id);

//        dd($rate);
        if (count($rate) == 0) {
            $rate = new ComerciosRate();
        }

        $rate->user_id = Auth::id();
        $rate->comercio_id = $id;

//        return dd($rate);
        if ($rate->recomendado == 1) {
            $rate->recomendado = 0;
            $comercio->rate_count--;
            $dados['up'] = 0;
        } else {
            $rate->recomendado = 1;
            $comercio->rate_count++;
            $dados['up'] = 1;
        }
        $rate->save();
        $comercio->save();

        $dados['sucesso'] = 1;


        echo json_encode($dados);

    }

    //recomenda ou "des"recomenda lugar
//    public function getRate($id)
//    {
//        $id = Crypt::decrypt($id);
////        dd($id);
//
//        $rate = ComerciosRate::where('user_id', Auth::id())->where('comercio_id', $id)->first();
//        $comercio = Comercios::find($id);
//
////        dd($rate);
//        if (count($rate) == 0) {
//            $rate = new ComerciosRate();
//        }
//
//        $rate->user_id = Auth::id();
//        $rate->comercio_id = $id;
//
////        return dd($rate);
//        if ($rate->recomendado == 1) {
//            $rate->recomendado = 0;
//            $comercio->rate_count--;
//        } else {
//            $rate->recomendado = 1;
//            $comercio->rate_count++;
//        }
//        $rate->save();
//        $comercio->save();
//
//
//        return Redirect::to(URL::previous() . "#com-" . $id);
//    }

    //recomenda ou "des"recomenda evento sem ajax (não está em uso)
//    public function getScore($id)
//    {
//        $id = Crypt::decrypt($id);
////        dd($id);
//
//        $rate = EventoScore::where('user_id', Auth::id())->where('evento_id', $id)->first();
//        $comercio = Eventos::find($id);
//
////        dd($rate);
//        if (count($rate) == 0) {
//            $rate = new EventoScore();
//        }
//
//        $rate->user_id = Auth::id();
//        $rate->evento_id = $id;
//
////        return dd($rate);
//        if ($rate->recomendado == 1) {
//            $rate->recomendado = 0;
//            $comercio->score_count--;
//        } else {
//            $rate->recomendado = 1;
//            $comercio->score_count++;
//        }
//        $rate->save();
//        $comercio->save();
//
//
//        return Redirect::to(URL::previous() . "#tab-eventos");
//    }

    public function getScoreAjax(Request $request)
    {

        $id = $request->id;

        $rate = EventoScore::where('user_id', Auth::id())->where('evento_id', $id)->first();
        $comercio = Eventos::find($id);

        if (count($rate) == 0) {
            $rate = new EventoScore();
        }

        $rate->user_id = Auth::id();
        $rate->evento_id = $id;

        if ($rate->recomendado == 1) {
            $rate->recomendado = 0;
            $comercio->score_count--;
            $dados['up'] = 0;
        } else {
            $rate->recomendado = 1;
            $comercio->score_count++;
            $dados['up'] = 1;
        }
        $rate->save();
        $comercio->save();


        $dados['sucesso'] = 1;


        echo json_encode($dados);

    }

    //pagina para add local
    public function getAddLocal()
    {
        return view('back-end-user/add-local')
            ->withCidades(Cidades::all())
            ->withConf(\CityTips\Conf::all());
    }

    public function postAddLocal(Request $request)
    {
        try {
            $rules = [
                'image' => 'required|image|max:300*300|dimensions:ratio=1/1',
//                'endereco' => 'required',
                'nome' => 'required',
//                'telefone' => 'required',
                'entrega' => 'required',
                'cidade' => 'required',
//                'horario' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules, $this->messages);
            if ($validator->fails()) {
                return redirect('user/add-local')->withErrors($validator)->withInput();
            } else {
                $foto_nome = str_random(30) . '-comercio-avatar.png';
                $request->file('image')->move('uploads', $foto_nome);
                $comercio = new Comercios();
                $comercio->nome = $request->nome;
                $comercio->endereco = $request->endereco;
                $comercio->entrega = $request->entrega;
                $comercio->horario = $request->horario;
                $comercio->telefone = $request->telefone;
                $comercio->cidade_id = $request->cidade;
                $comercio->site = '<a href="' . $request->site . '" target="_blank">Facebook</a>';
                $comercio->user_id = Auth::id();
                $comercio->status = 1;
                $comercio->slug = urlencode($request->nome);
                $comercio->foto = $foto_nome;
                $comercio->lat = $request->lat;
                $comercio->lon = $request->lng;
                $comercio->tipo = $request->tipo;
                $comercio->import = $request->import;
                $comercio->save();
                return redirect('user/gerenciador');
            }
        } catch (\Exception $e) {
//            dd(get_class($e));
            abort(500);
        }

    }

    public function getCriarItemCardaprio($id)
    {
        $id = Crypt::decrypt($id);
        $comercio = Comercios::find($id);
        return view('back-end-user/criar-item-cardaprio')
            ->withComercio($comercio)
            ->withCategorias(ComerciosCardaprioCategoria::all());
    }

    public function postCriarItemCardaprio(Request $request)
    {
        try {
            $rules = [
                'descricao' => 'required',
                'nome' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules, $this->messages);
            if ($validator->fails()) {
                return redirect('user/criar-item-cardaprio/' . $request->id)->withErrors($validator)->withInput();
            } else {
                $item = new ComerciosCardaprio();
                $item->user_id = Auth::id();
                $item->comercio_id = Crypt::decrypt($request->id);
                $item->categoria_id = $request->categoria;
                $item->nome = $request->nome;
                $item->descricao = $request->descricao;
                $item->status = 1;
                $item->save();
                return redirect('user/gerenciador');
            }
        } catch (\Exception $e) {
//            dd(get_class($e));
            abort(500);
        }
    }

    public function getCriarEvento()
    {
        return view('back-end-user/criar-evento')
            ->withCidades(Cidades::all())->withLugares(Comercios::where('status', 2)->get());
    }

    public function getCriarEventoManual()
    {
        $eventos_fut = Eventos::where('eventos.user_id', Auth::id())
            ->whereDate('inicio', '>=', Carbon::today()->toDateString())
            ->get();
        if (count($eventos_fut) > 9 && Auth::user()->role == 0) {
            Session::flash('flash_message', 'Por enquanto nós só permitimos que você crie apenas 8 eventos por vez, espere algum deles passar para postar um novo ou, para aumentar seu limite entre em contato conosco clicando em <a target="_blank" href="http://m.me/1398707946825318">Ajuda</a>');
            return redirect('user/criar-evento');
        }

        return view('back-end-user/criar-evento-manual')
            ->withCidades(Cidades::all())->withLugares(Comercios::where('status', 2)->get());
    }

    public function getCriarEventoNovo($id)
    {
//        $eventos_fut = Eventos::where('eventos.user_id', Auth::id())
//            ->whereDate('inicio', '>=', Carbon::today()->toDateString())
//            ->get();
//        if (count($eventos_fut) > 9 && Auth::user()->role == 0) {
//            Session::flash('flash_message', 'Por enquanto nós só permitimos que você crie apenas 8 eventos por vez, espere algum deles passar para postar um novo ou, para aumentar seu limite entre em contato conosco clicando em <a target="_blank" href="http://m.me/1398707946825318">Ajuda</a>');
//            return redirect('user/criar-evento-rapido');
//        }

        return view('back-end-user/criar-evento-rapido')
            ->withCidades(Cidades::all())->withLugares(Comercios::where('status', 2)->where('alias', 0)->where('cidade_id',$id)->get())->withCity($id);
    }

    public function postCriarEvento(Request $request)
    {
        if ($request->localid != 0) {


            $rules = [
                'desc' => 'required',
                'nome' => 'required',
                'inicio' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules, $this->messages);
            if ($validator->fails()) {
                return redirect('user/criar-evento-manual')->withErrors($validator)->withInput();
            } else {
                try {
                    $comercio = Comercios::find($request->localid);
                } catch (\Exception $e) {
                    abort(500);
//                    echo 'erro não tem esse comercio';
                }
                $id = str_random(10) . Auth::id();
                $descricao = $request->desc;
                $nome = $request->nome;
                $lugar = $comercio->nome;
                $cidade = $comercio->cidade_id;
                $inicio = $request->inicio;
                $lat = $comercio->lat;
                $lon = $comercio->lon;
                $inicio = str_replace('/', '-', $inicio);
//        dd($inicio);

                $inicio = strtotime($inicio);

//        $inicio = date('d/m/Y h:m', $inicio);
                $salvo = Eventos::salvarEvento($descricao, $nome, $lugar, $inicio, $lat, $lon, $request->link, $id, $cidade, $request->localid);
                if ($salvo) {
                    return redirect('user/gerenciador');
                }
            }


        } else {

            $rules = [
                'desc' => 'required',
                'nome' => 'required',
                'inicio' => 'required',
                'endereco' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules, $this->messages);
            if ($validator->fails()) {
                return redirect('user/criar-evento-manual')->withErrors($validator)->withInput();
            } else {

                $id = str_random(10) . Auth::id();
//        dd($request);
                $descricao = $request->desc;
                $nome = $request->nome;
                $lugar = $request->endereco;
                $cidade = $request->cidade;
                $inicio = $request->inicio;
                $lat = $request->lat;
                $lon = $request->lng;
                $inicio = str_replace('/', '-', $inicio);
//        dd($inicio);

                $inicio = strtotime($inicio);

//        $inicio = date('d/m/Y h:m', $inicio);
                $salvo = Eventos::salvarEvento($descricao, $nome, $lugar, $inicio, $lat, $lon, $request->link, $id, $cidade, $request->localid);
                if ($salvo) {
                    return redirect('user/gerenciador');
                }
            }
        }
        abort(500);

//        return redirect('user/home');
    }

    public function postCriarEventoRapido(Request $request)
    {
        if ($request->localid != 0) {


            $rules = [
                'desc' => 'required',
                'nome' => 'required',
                'inicio' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules, $this->messages);
            if ($validator->fails()) {
                Session::flash('flash_message', 'Preencha todos os campos');

                return redirect('user/criar-evento-novo/'.$request->city)->withErrors($validator)->withInput();
            } else {
                try {
                    $comercio = Comercios::find($request->localid);
                } catch (\Exception $e) {
                    abort(500);
//                    echo 'erro não tem esse comercio';
                }
                $id = str_random(10) . Auth::id();
                $descricao = $request->desc;
                $nome = $request->nome;
                $lugar = $comercio->nome;
                $cidade = $comercio->cidade_id;
                $inicio = $request->inicio;
                $lat = $comercio->lat;
                $lon = $comercio->lon;
                $inicio = str_replace('/', '-', $inicio);
//        dd($inicio);

                $inicio = strtotime($inicio);

//        $inicio = date('d/m/Y h:m', $inicio);
                $salvo = Eventos::salvarEvento($descricao, $nome, $lugar, $inicio, $lat, $lon, $request->link, $id, $cidade, $request->localid);
                if ($salvo) {
                    Session::flash('flash_message', 'Evento Criado!');

                    return redirect('/');
                }
            }


        } else {
            Session::flash('flash_message', 'Preencha todos os campos');

            return redirect('user/criar-evento-novo/'.$request->city)->withInput();
        }
        abort(500);

//        return redirect('user/home');
    }

    public function postFacebookImport(Request $request)
    {
        date_default_timezone_set('America/Sao_Paulo');

        $eventos_fut = Eventos::where('eventos.user_id', Auth::id())
            ->whereDate('inicio', '>=', Carbon::today()->toDateString())
            ->get();
        if (Auth::user()->role != 1) {
            if (count($eventos_fut) > 9) {
                Session::flash('flash_message', 'Por enquanto nós só permitimos que você crie apenas 8 eventos por vez, espere algum deles passar para postar um novo ou, para aumentar seu limite entre em contato conosco clicando em <a target="_blank" href="http://m.me/1398707946825318">Ajuda</a>');
                return redirect('user/criar-evento');
            }
        }

        $link_array = explode("/", $request->link);
        $n = 1;
//        passa todas as partes do link separado por / até chegar no id do evento, que é o primeiro numero na url
        foreach ($link_array as $item) {
            if (is_numeric($item)) {
//                echo 'é numero <br>';
                try {
                    $lat = 0;
                    $lon = 0;
                    $json = file_get_contents("https://graph.facebook.com/v2.7/" . $item . "?fields=type%2Cname%2Cstart_time%2Cdescription%2Cplace&access_token=" . Auth::user()->password);
                    $objeto_evento = json_decode($json);
                    if ($objeto_evento->type != 'public') {
                        Session::flash('flash_message', 'Oops! Aqui no CityTips só é permitido importar eventos que são públicos. Este evendo pode estar sendo compartilhado apenas com você e seus amigos.');
                        return redirect('user/criar-evento');
                    }
                    $descricao = '';
                    if (isset($objeto_evento->description)) {
                        $descricao = $objeto_evento->description;
                    }
                    $nome = $objeto_evento->name;
                    $lugar = $objeto_evento->place->name;
                    $inicio = $objeto_evento->start_time;
                    if (isset($objeto_evento->place->location)) {
                        $lat = $objeto_evento->place->location->latitude;
                        $lon = $objeto_evento->place->location->longitude;
                    }
                    $inicio = strtotime($inicio);
//                    $inicio = date('d/m/Y h:m', $inicio);
                    $id = $objeto_evento->id;

                    //verifica se já não foi importado uma vez
                    $if = Eventos::where('slug_id', $id)->get();
                    if (count($if) > 0) {
//                        dd($if);
                        Session::flash('flash_message', 'Oops! Este evento já foi criado aqui uma vez.');
                        return redirect('user/criar-evento');
                    }

//                    dd($objeto_evento);
                    $cidade = $request->cidade;

                    if ($request->localid != 0) {
                        try {
                            $comercio = Comercios::find($request->localid);
                        } catch (\Exception $e) {
                            abort(500);
//                    echo 'erro não tem esse comercio';
                        }

                        $lugar = $comercio->nome;
                        $lat = $comercio->lat;
                        $lon = $comercio->lon;
                        $cidade = $comercio->cidade_id;
                    }

                    $salvo = Eventos::salvarEvento($descricao, $nome, $lugar, $inicio, $lat, $lon, $request->link, $id, $cidade, $request->localid);
                    if ($salvo) {
                        return redirect('user/gerenciador');
                    }
                } catch (\Exception $e) {
//                    Session::flash('flash_message', 'O link inserido parece não ser válido.');
//                    return redirect('user/criar-evento');
//                    echo 'erro';
                    dd($e);
                }
            } else {
//                echo 'não é numero <br>';
            }
            $n++;
        }
        Session::flash('flash_message', 'O link inserido parece não ser válido, ou o evento não é publico');
        return redirect('user/criar-evento');
    }

    public function getConf()
    {
        return view('back-end-user/conf')->withUser(User::find(Auth::id()));
    }

    public function postDesativarConta(Request $request)
    {
        $del = User::find($request->id);
        $del->name = 'Usuário CityTips';
        $del->save();
        $del->delete();
        return redirect('/');
    }

    public function postComentar(Request $request)
    {
        $local_id = $request->id;
//        $nota = $request->nota;
        $user_id = Auth::id();
        $avaliacao = $request->avaliacao;

        $aval = Comentarios::where('user_id', $user_id)->where('comercio_id', $local_id)->first();

        if (count($aval) == 0) {
            $aval = new Comentarios();
        } else {
            Session::flash('flash_message_update', 'Avaliação atualizada!');

        }

        $aval->user_id = $user_id;
        $aval->comercio_id = $local_id;
//        $aval->nota = $nota;
        $aval->avaliacao = $avaliacao;


        $aval->save();

        return Redirect::to(URL::previous());
    }

    public function getExcluirComentario($id)
    {
        try {
            $com_id = $id;
            $user_id = Auth::id();

            $id = Comentarios::where('comercio_id', $com_id)->where('user_id', $user_id)->first();

            Comentarios::destroy($id->id);
            return Redirect::to(URL::previous());
        } catch (\Exception $e) {
            abort(500);
        }
    }

    public function getAbusoAvaliacao($id)
    {
        $sup = new Suporte();
        $sup->info = 'Abuso no comentario id: ' . $id;
        $sup->user_id = Auth::id();
        $sup->save();

        Session::flash('flash_message', 'Sentimos muito pelo ocorrido, vamos analizar esta avaliação e se infringir nossos termos será removida. Obrigado pela colaboração');
        return Redirect::to(URL::previous());


    }

    public function postNameChanger(Request $request)
    {
        $user = User::find(Auth::id());
        if (strtotime($user->changed_at) < strtotime('-30 days')) {
            $user->changed_at = Carbon::today()->toDateString();
            $user->nome_original = $user->name;
            $user->name = $request->name;
            $user->save();

            return Redirect::to(URL::previous());
        } else {
            Session::flash('flash_message', 'Você já alterou seu nome nos últimos 60 dias');
            return Redirect::to(URL::previous());
        }


    }

    public function postEmailChanger(Request $request)
    {
        $email = AlterarEmail::where('user_id', Auth::id())->first();
        if (count($email) == 0) {
            $email = new AlterarEmail();
            $email->user_id = Auth::id();
        }

        $email->email = $request->email;

        $content = Crypt::encrypt(Auth::id());


        $user = $email;

        Mail::send('auth.emails.email-change', ['user' => $user, 'content' => $content], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('City tips está com novidades!');
        });

        $email->save();

        return Redirect::to(URL::previous());


    }

    public function getMailConfirmation($id)
    {
        try {
            $id = Crypt::decrypt($id);

            if ($id == Auth::id()) {
                $user = User::find($id);
                $mail = AlterarEmail::where('user_id', $id)->first();
                $user->email = $mail->email;
                $user->save();

                return redirect('user/conf');
            }
            return redirect('user/conf');
        } catch (Exception $e) {
            abort(404);
        }
    }

    public function postTelefoneChanger(Request $request)
    {

        $user = User::find(Auth::id());
        $user->telefone = $request->telefone;
        $user->save();

        return Redirect::to(URL::previous());
    }

    public function getEmailNotificacao()
    {
        $user = User::find(Auth::id());
        if ($user->email_status == 1) {
            $user->email_status = 0;
        } else {
            $user->email_status = 1;
        }
        $user->save();
        return Redirect::to(URL::previous());

    }

    public function getUpdateEventos($id)
    {
        $locais = Comercios::where('cidade_id',$id)->get();
        $id_comercio_array = [];
        foreach ($locais as $local) {
            $id_comercio_array[] = $local->id;
        }

        date_default_timezone_set('America/Sao_Paulo');

        if (Auth::id() != 4) {
            return Redirect::to(URL::previous());
        }
        $paginas = FacebookPages::whereIn('comercio_id',$id_comercio_array)->get();

        $resultado = [];
        try {
            foreach ($paginas as $pagina) {
                $json = file_get_contents("https://graph.facebook.com/v2.8/" . $pagina->link . "?fields=events%7Bstart_time%2Cdescription%2Cplace%2Cname%2Ctype%7D&access_token=" . Auth::user()->password);
                $dados = json_decode($json);
                if (isset($dados->events)) {
                    $resultado[] = '--------importando: ' . $pagina->nome . '<br>';


                    foreach ($dados->events->data as $objeto_evento) {

                        if ($objeto_evento->type != 'public') {
                            //evento não é público
                            $resultado[] = 'evento não é publico não importado: ' . $objeto_evento->name . '<br>';
                            continue;
                        }
                        $descricao = '';
                        if (isset($objeto_evento->description)) {
                            $descricao = $objeto_evento->description;
                        }
                        $nome = $objeto_evento->name;

                        $inicio = $objeto_evento->start_time;

                        if ($inicio < Carbon::today()) {
//                            $resultado[] = 'evento antigo não importado: ' . $nome . '<br>';
                            continue;
                        }

                        $inicio = strtotime($inicio);


                        $id = $objeto_evento->id;
                        try {
                            $comercio = Comercios::find($pagina->comercio_id);
                        } catch (\Exception $e) {
                            abort(500);
//                    echo 'erro não tem esse comercio';
                        }

                        $lugar = $comercio->nome;
                        $lat = $comercio->lat;
                        $lon = $comercio->lon;
                        $cidade = $comercio->cidade_id;

                        $salvo = Eventos::salvarEvento($descricao, $nome, $lugar, $inicio, $lat, $lon, 'importacao automatica', $id, $cidade, $pagina->comercio_id);
                        if ($salvo) {
                            $resultado[] = $lugar . ' sucesso: ' . $nome . '<br>';

                        }
                    }
                } else {
                    $resultado[] = ' sem eventos: ' . $pagina->nome . '<br>';

                    continue;
                }
            }

        } catch (Exception $e) {
            dd($e);

        }

        Session::flash('flash_message', $resultado);

        return Redirect::to(URL::previous());
    }

    public function postUpdateEventosMassa(Request $request)
    {
        $array_cidades = explode(",",$request->id);
//        return dd($array_cidades);

        $locais = Comercios::whereIn('cidade_id',$array_cidades)->get();
//        return dd($locais);
        $id_comercio_array = [];
        foreach ($locais as $local) {
            $id_comercio_array[] = $local->id;
        }

        date_default_timezone_set('America/Sao_Paulo');

        if (Auth::id() != 4) {
            return Redirect::to(URL::previous());
        }
        $paginas = FacebookPages::whereIn('comercio_id',$id_comercio_array)->get();

        $resultado = [];
        try {
            foreach ($paginas as $pagina) {
                $json = file_get_contents("https://graph.facebook.com/v2.8/" . $pagina->link . "?fields=events%7Bstart_time%2Cdescription%2Cplace%2Cname%2Ctype%7D&access_token=" . Auth::user()->password);
                $dados = json_decode($json);
                if (isset($dados->events)) {
                    $resultado[] = '--------importando: ' . $pagina->nome . '<br>';


                    foreach ($dados->events->data as $objeto_evento) {

                        if ($objeto_evento->type != 'public') {
                            //evento não é público
                            $resultado[] = 'evento não é publico não importado: ' . $objeto_evento->name . '<br>';
                            continue;
                        }
                        $descricao = '';
                        if (isset($objeto_evento->description)) {
                            $descricao = $objeto_evento->description;
                        }
                        $nome = $objeto_evento->name;

                        $inicio = $objeto_evento->start_time;

                        if ($inicio < Carbon::today()) {
//                            $resultado[] = 'evento antigo não importado: ' . $nome . '<br>';
                            continue;
                        }

                        $inicio = strtotime($inicio);


                        $id = $objeto_evento->id;
                        try {
                            $comercio = Comercios::find($pagina->comercio_id);
                        } catch (\Exception $e) {
                            abort(500);
//                    echo 'erro não tem esse comercio';
                        }

                        $lugar = $comercio->nome;
                        $lat = $comercio->lat;
                        $lon = $comercio->lon;
                        $cidade = $comercio->cidade_id;

                        $salvo = Eventos::salvarEvento($descricao, $nome, $lugar, $inicio, $lat, $lon, 'importacao automatica', $id, $cidade, $pagina->comercio_id);
                        if ($salvo) {
                            $resultado[] = $lugar . ' sucesso: ' . $nome . '<br>';

                        }
                    }
                } else {
                    $resultado[] = ' sem eventos: ' . $pagina->nome . '<br>';

                    continue;
                }
            }

        } catch (Exception $e) {
            dd($e);

        }

        Session::flash('flash_message', $resultado);

        return Redirect::to(URL::previous());
    }

    public function postCrudEventos(Request $request)
    {
        $new = new FacebookPages();
        $new->nome = $request->nome;
        $new->link = $request->link;
        $new->comercio_id = $request->comercio;
        $new->save();
        return redirect('admin/crud-eventos');
    }

    public function getCrudEventosDel($id)
    {
        if (Auth::id() != 4) {
            return Redirect::to(URL::previous());
        }
        FacebookPages::destroy($id);
        return Redirect::to(URL::previous());
    }

    public function postStatusNovo(Request $request)
    {
//        return Carbon::now();
        try {

//        valida se tem local
            if ($request->localid == 0 && $request->local == "") {
                Session::flash('flash_message', 'Ops! Você não disse onde?!');
                return Redirect::to(URL::previous())->withStatus($request->status);
            }

            $status = new Eventos();
            $status->id_local = $request->localid;


//        caso não exista, cria um alias
            if ($request->localid == 0) {
                $comercio = Comercios::criarAliasDeLocal($request);
                $status->id_local = $comercio->id;
                $nome_lugar = $request->local;
            } else {
                $lugar = Comercios::find($request->localid);
                if (isset($lugar) == false) {
                    abort(500);
                };
                $nome_lugar = $lugar->nome;
            }

            $status->nome = $request->status;
            $status->user_id = Auth::id();
            $status->status = 2;
            $status->tipo = 2;
            $status->slug_id = str_random(10) . Auth::id();
            $status->local = $nome_lugar;
            $status->cidade_id = $request->cidade;
            date_default_timezone_set('America/Sao_Paulo');
            if ($request->datetimetrue == 1) {
                $status->inicio = $request->inicio;
            } else {
                $status->inicio = Carbon::now();
            }
            $status->save();

            return Redirect::to(URL::previous());
        } catch (Exception $e) {
            Session::flash('flash_message', 'Alguma coisa não funcionou, tente novamente.');
            return Redirect::to(URL::previous())->withStatus($request->status);
        }
    }


    public function getCity($id)
    {

//algoritimo de ordenação dos eventos por relevância:
//eventos selecionados são postos na ordem neste array
        $eventos_id_array = Eventos::algoritmoDeEventos($id);

        $eventos = Eventos::queryArrayDeEventos($eventos_id_array);

        //retorna as avaliações dos eventos que estão no array
        $score = \CityTips\EventoScore::whereIn('evento_id', $eventos_id_array)->where('recomendado', 1)->get();
//fim da parte dos eventos
        $view = view('sessoes/sub-evento/evento-scroll')
            ->withScore($score)
            ->withEventos($eventos);
        $contents = (string)$view;
        echo $contents;
    }

    public function getEscolherCidade()
    {
        return view('sessoes/escolher-cidade')
            ->withConf(\CityTips\Conf::all())
            ->withCidades(\CityTips\Cidades::all())
            ->withUser(User::find(Auth::id()));
    }

    public function getExcluirStatus($id)
    {
        $evento = Eventos::find($id);
        if (Auth::id() == $evento->user_id) ;
        Eventos::destroy($id);
        return Redirect::to(URL::previous());
    }

}