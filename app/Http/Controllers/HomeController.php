<?php

namespace CityTips\Http\Controllers;

use CityTips\Http\Requests;
use CityTips\UsersLoginLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //esse controller é padrão do laravel e está desativado
        return view('home')->withConf(\CityTips\Conf::all());
    }
}
