<?php

namespace CityTips\Http\Controllers;


use Carbon\Carbon;
use CityTips\Comercios;
use CityTips\Eventos;
use CityTips\FacebookPages;
use CityTips\Http\Requests;

use CityTips\Suporte;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Validator;


class SiteController extends Controller
{
    private $messages = [
        'image.required' => 'A imagem é obrigatória',
        'required' => 'Este campo é obrigatório',
        'image.image' => 'Formato não permitido',
        'image.max' => 'A imagem é muito grande',
    ];

    public function getCardaprio(Request $request)
    {


        $id = $request->com_id;


        $itens = \CityTips\ComerciosCardaprio::where('comercio_id', $id)->get();
        $cat_array = [];
        foreach ($itens as $item) {
            $cat_array[] = $item->categoria_id;
        }
        $categorias = \CityTips\ComerciosCardaprioCategoria::whereIn('id', $cat_array)->get();
        $output = '';
        if (count($itens) == 0) {
            $output .= '<p>Começe compartinhando seu prato favorito, bebidas ou outros produtos que este estabelecimento venda.</p>';
        } else {
            $output .= ' <table class="table-responsive table-hover table"><tbody>';
            foreach ($categorias as $categoria) {
                $output .= '<tr><td colspan="3"><strong>' . $categoria->nome . '</strong></td></tr>';
                foreach ($itens as $item) {
                    if ($categoria->id == $item->categoria_id) {
                        $output .= '<tr><td>' . $item->nome . '</td><td><small>' . $item->descricao . '</small></td><td>' . $item->preco . '</td></tr>';
                    }
                }
            }
            $output .= '</tbody></table>';
        }
        $dados['comercio'] = (string)$id;
        $dados['cardaprio'] = (string)$output;
        $dados['sucesso'] = 1;
        echo json_encode($dados);
    }

    public function getSuporte($tipo, $motivo, $id)
    {
        switch ($tipo) {
            case 0://geral

                switch ($motivo) {
                    case 4:
                        return view('suporte/0-4-0');//Reportar Erro ou Abuso
                    case 6:
                        return view('suporte/0-6-0');//Incluir Cidade
                    default:
                        abort(404);
                }
                break;
            case 1://sobre estabelecimentos

                switch ($motivo) {
                    case 1:
                        return view('suporte/1-1-0')->withComercio($id); //excluir item
                    case 2:
                        return view('suporte/1-2-0')->withComercio($id);//editar item
                    case 4:
                        return view('suporte/1-4-0')->withComercio($id);//reportar abuso comercio
                    case 5:
                        return view('suporte/1-5-0')->withComercio($id);//sugerir mudança comercio

                    default:
                        abort(404);
                }
                break;
            case 2://sobre itens de cardáprio
                switch ($motivo) {
                    case 1:
                        return view('suporte/2-1-0')->withItem($id);//excluir item
                    case 2:
                        return view('suporte/2-2-0')->withItem($id);//editar item
                    case 4:
                        return view('suporte/2-4-0')->withItem($id);//reportar abuso item
                    default:
                        abort(404);
                }
                break;
            case 3://sobre estabelecimentos

                switch ($motivo) {
                    case 1:
                        return view('suporte/3-1-0')->withComercio($id); //excluir item
                    case 2:
                        return view('suporte/3-2-0')->withComercio($id);//editar item
                    case 4:
                        return view('suporte/3-4-0')->withComercio($id);//reportar abuso evento
                    case 5:
                        return view('suporte/3-5-0')->withComercio($id);//sugerir mudança evento

                    default:
                        abort(404);
                }
                break;
            default:
                abort(404);
        }
        /*
 * LEGENDAS DE CÓDIGOS
 *
 * ao reportar
 * 0 = assunto sem categoria
 * 1 = reportado comercio
 * 2 = reportado item de cardaprio
 * segundo item
 * 1 = excluir
 * 2 = modificar
 * 3 = reportar erro
 * 4 = reportar abuso
 * 5 = sugerir mudança
 * terceiro item = id
 * 0/6/0 add cidade
 */
    }

    public function postSuporte(Request $request)
    {
        $rules = [
            'email' => 'required',
            'nome' => 'required',
            'texto' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',

        ];
        $validator = Validator::make($request->all(), $rules, $this->messages);
        if ($validator->fails()) {

            return redirect(URL::previous())->withErrors($validator)->withInput();
        } else {

            $suporte = new Suporte();
            $suporte->texto = $request->texto;
            $suporte->user_id = $request->id;
            $suporte->nome = $request->nome;
            $suporte->email = $request->email;
            $suporte->info = $request->info;
            $suporte->save();

            return view('suporte/confirma')->withId($suporte->id);
        }
    }

    public function getSobre()
    {
        return view('paginas/sobre');
    }

    public function getTermos()
    {
        return view('paginas/termos');
    }

    public function getLegal()
    {
        return view('paginas/legal');
    }

    public function getCookies()
    {
        return view('paginas/cookies');
    }

    public function postFaltaAlgo(Request $request)
    {
        $suporte = new Suporte();
        $suporte->texto = $request->texto;
        $suporte->email = $request->email;
        $suporte->info = 'falta algo';
        $suporte->save();
        return Redirect::to(URL::previous());
    }

    public function getCronUpdateLocaisLevaUm()
    {
        Eventos::atualizarCidade(1);
//        Eventos::atualizarCidade(3);
    }
}