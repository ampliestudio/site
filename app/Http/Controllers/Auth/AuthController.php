<?php

namespace CityTips\Http\Controllers\Auth;

use CityTips\User;
use CityTips\UsersLoginLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;
use Validator;
use CityTips\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    public function redirectToProvider()
    {
        try {
            return Socialite::driver('facebook')->redirect();
        } catch (\Exception $e) {
//            dd($e);
            abort(500);
        }
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
//            dd($user);

            if($user->email == null){
                $user->email = $user->id;
            }

//        $data  = ['name'=>$user->name, 'email'=>$user->email, 'password'=>$user->token];

            $userDeleted = User::where('email', $user->email)->onlyTrashed()->first();
            if (!is_null($userDeleted)) {
                $userDeleted->restore();
                Auth::login($userDeleted);
                if ($userDeleted->id == 1) {
                    return redirect('/admin/home');
                }
                return redirect('/');
            }

            $userDB = User::where('email', $user->email)->first();
            if (!is_null($userDB)) {
                Auth::login($userDB);

                if ($userDB->id == 1) {
                    return redirect('/admin/home');
                }
                return redirect('/');

            } else {
                if($user->email == null){
                    $user->email = $user->id;
                }
                $novo = new User();
                $novo->name = $user->name;
                $novo->email = $user->email;
                $novo->password = $user->token;
                $novo->foto = $user->avatar;
                $novo->save();

                Auth::login($novo);


                if ($novo->id == 1) {
                    return redirect('/admin/home');
                }
                return redirect('/user/escolher-cidade');

            }
        } catch (\Exception $e) {
//            dd($e);
            abort(500);
        }

    }
}
