<?php

namespace CityTips\Http\Controllers;

use CityTips\Cidades;
use CityTips\Comercios;
use CityTips\ComerciosRate;
use CityTips\Eventos;
use CityTips\EventoScore;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Http\Request;

use CityTips\Http\Requests;

class EventoController extends Controller
{

    public function index()
    {
        abort(404);
    }

    public function show($id)
    {
        date_default_timezone_set('America/Sao_Paulo');

        $evento = Eventos::where('slug_id',$id)
            ->leftJoin('comercios','eventos.id_local','=','comercios.id')
            ->join('users','eventos.user_id','=','users.id')
            ->select('eventos.*','comercios.slug as local_slug', 'comercios.foto as local_foto', 'users.name as username', 'users.foto as userimage')
            ->first();

        $rate = EventoScore::where('evento_id',$evento->id)->where('recomendado', 1)->get();


        $eventos_id_array = Eventos::algoritmoDeEventos($evento->cidade_id);
        //seleciona os eventos apartir do array
        $eventos = Eventos::queryArrayDeEventosSingle($eventos_id_array);

        //retorna as avaliações dos eventos que estão no array
        $score = EventoScore::whereIn('evento_id', $eventos_id_array)->where('recomendado', 1)->get();

        if(count($evento)==1){
            return view('sessoes/eventos-single')
                ->withEvento($evento)
                ->withScorelist($score)
                ->withEventos($eventos)
                ->withScore($rate)
                ->withCidade(Cidades::find($evento->cidade_id))
                ->withCidades(\CityTips\Cidades::all());
        }else{
//não existe o evento digitado
            abort(404);
        }

    }

}
