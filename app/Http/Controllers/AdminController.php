<?php

namespace CityTips\Http\Controllers;


use Carbon\Carbon;
use CityTips\Analytics;
use CityTips\Cidades;
use CityTips\Comercios;
use CityTips\ComerciosCardaprio;
use CityTips\ComerciosCardaprioCategoria;
use CityTips\Eventos;
use CityTips\FacebookPages;
use CityTips\Http\Requests;
use Validator;
use CityTips\Suporte;
use CityTips\UsersCidadeHistorico;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;


class AdminController extends Controller
{

    private $messages = [
        'image.required' => 'A imagem é obrigatória',
        'image.dimensions' => 'A imagem deve ser quadrada',
        'required' => 'Este campo é obrigatório',
        'site.required' => 'O ID do facebook é obrigatório',
        'image.image' => 'Formato não permitido',
        'image.max' => 'A imagem é muito grande',
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function getHome()
    {

        $comercios_pendentes = Comercios::orderBy('status', 'asc')
            ->orderBy('nome', 'asc')
            ->whereIn('status', [1,3])
            ->join('cidades', 'comercios.cidade_id', '=', 'cidades.id')
            ->select('comercios.*', 'cidades.nome as cidade_nome')
            ->paginate(100);
//        $comercios_pendentes = Comercios::where('status', 1 )->orWhere('status', 3 )->get();

        return view('/back-end-admin/home')
            ->withConf(\CityTips\Conf::all())
            ->withComerciospendentes($comercios_pendentes)
            ->withCidades(Cidades::all());
    }

    public function postUpdate(Request $request)
    {


        $update = Comercios::find($request->id);
        $update->nome = $request->nome;
        $update->endereco = $request->endereco;
        $update->entrega = $request->entrega;
        $update->horario = $request->horario;
        $update->telefone = $request->telefone;
        $update->status = $request->status;
        $update->slug = $request->slug;
        $update->cidade_id = $request->cidade_id;
        $update->legal_info = $request->legal_info;
        $update->user_id = $request->user_id;
        $update->oficial = $request->oficial;
        $update->descricao = $request->descricao;
//        $update->rate_count = $request->rate_count;
        $update->lat = $request->lat;
        $update->lon = $request->lon;
//        $update->foto = $request->foto;
        $update->site = $request->site;
        $update->tipo = $request->tipo;
        $update->save();

        return Redirect::to(URL::previous());
    }

    public function getItens()
    {
        $itens = ComerciosCardaprio::orderBy('status', 'asc')
            ->join('comercios', 'comercios_cardaprio.comercio_id', '=', 'comercios.id')
            ->select('comercios_cardaprio.*', 'comercios.nome as com_nome', 'comercios.foto as foto')
            ->paginate(15);
        return view('/back-end-admin/itens')->withConf(\CityTips\Conf::all())->withItens($itens)->withCat(ComerciosCardaprioCategoria::all());

    }

    public function getEvento()
    {

        $eventos = Eventos::orderBy('updated_at', 'desc')->paginate(30);
        return view('/back-end-admin/evento')->withConf(\CityTips\Conf::all())->withEventos($eventos);
    }

    public function postUpdateEvento(Request $request)
    {

        $update = Eventos::find($request->id);
        $update->nome = $request->nome;
        $update->local = $request->local;
        $update->inicio = $request->inicio;
        $update->slug_id = $request->slug_id;
//        $update->cidade_id = $request->cidade_id;
        $update->link_fb = $request->link_fb;
        $update->descricao = $request->descricao;
        $update->status = $request->status;
        $update->lat = $request->lat;
        $update->lon = $request->lon;
//        $update->user_id = $request->user_id;
        $update->save();

        return redirect('/admin/evento');

    }

    public function getAprovarItem($id)
    {
        $item = ComerciosCardaprio::find($id);
        $item->status = 2;
        $item->save();
        return redirect('/admin/itens');


    }

    public function postUpdateItem(Request $request)
    {


        $update = ComerciosCardaprio::find($request->id);
        $update->nome = $request->nome;
        $update->slug = $request->slug;
        $update->descricao = $request->descricao;
        $update->preco = $request->preco;
        $update->status = $request->status;

        $update->save();

        return redirect('/admin/itens');
    }

    public function getSuporte()
    {

        $itens = Suporte::orderBy('created_at', 'desc')->get();
        return view('/back-end-admin/suporte')->withConf(\CityTips\Conf::all())->withItens($itens);

    }

    public function getAnalytics()
    {
        $hoje = Analytics::whereDate('created_at','=', Carbon::today()->toDateString())->get();
        $hoje = count($hoje);

        $semana = Analytics::whereDate('created_at','>=', Carbon::today()->subDays(7)->toDateString())->get();
        $semana = count($semana);

        $mes = Analytics::whereDate('created_at','>=', Carbon::today()->subDays(30)->toDateString())->get();
        $mes = count($mes);

        if($mes == 0 ){
            $hojec = 0;
            $semanac = 0;
        }else{
            $hojec = ($hoje*100)/$mes;
            $semanac = ($semana*100)/$mes;
        }

        $itens = Analytics::orderBy('created_at', 'desc')
            ->leftJoin('users','analytics.user_id', '=', 'users.id')
            ->select('analytics.*','users.name as username')
            ->get();
        return view('/back-end-admin/analytics')
            ->withConf(\CityTips\Conf::all())
            ->withItens($itens)
            ->withHoje($hoje)
            ->withHojec($hojec)
            ->withSemana($semana)
            ->withSemanac($semanac)
            ->withMes($mes);

    }

    public function getUsers()
    {

        $itens = User::orderBy('created_at', 'desc')->get();
        return view('/back-end-admin/usuarios')->withConf(\CityTips\Conf::all())->withItens($itens);

    }
    public function getUsersByMod()
    {

        $itens = User::orderBy('updated_at', 'desc')->get();
        return view('/back-end-admin/usuarios')->withConf(\CityTips\Conf::all())->withItens($itens);

    }

    public function getDropSuporte($id)
    {

        Suporte::destroy($id);
        return redirect('/admin/suporte');

    }

    public function getEmailMkt($id)
    {
        $user = User::find($id);
        $data['name'] = $user->name;
        $emails = $user->email;
        Mail::send('auth.emails.email-mkt', $data, function ($message) use ($emails) {
            $message->from('staff@citytips.com.br', 'Paulo da CityTips');
            $message->to($emails)->subject('Agenda de Terça para Londrina!');
        });
        Session::flash('flash_message', Mail:: failures());

        return Redirect::to(URL::previous());
    }

    public function getCrudEventos()
    {
        if (Auth::id() != 4) {
            return Redirect::to(URL::previous());
        }

        $pages = FacebookPages::join('comercios', 'facebook_pages.comercio_id', '=', 'comercios.id')
            ->select('facebook_pages.*', 'comercios.slug as comercio')
            ->get();

        $pronto = [];

        foreach ($pages as $item) {
            $pronto[] = $item->comercio_id;
        }
//        whereNotIn('id', $pronto)->
        return view('back-end-admin/crud-eventos')->withPaginas($pages)->withComercios(Comercios::where('import', 1)->orderBy('nome', 'asc')->get());
    }

    public function getGerenciarCidade($id){
        $comercios_pendentes = Comercios::orderBy('status', 'asc')
            ->orderBy('nome', 'asc')
            ->where('cidade_id',$id)
            ->join('cidades', 'comercios.cidade_id', '=', 'cidades.id')
            ->select('comercios.*', 'cidades.nome as cidade_nome')
            ->paginate(100);
//        $comercios_pendentes = Comercios::where('status', 1 )->orWhere('status', 3 )->get();

        return view('/back-end-admin/gerenciar-cidade')
            ->withConf(\CityTips\Conf::all())
            ->withId($id)
            ->withComerciospendentes($comercios_pendentes);
    }

    public function getUserCityHistory($id){
        $historico = UsersCidadeHistorico::where('user_id', $id)
            ->join('cidades','users_cidade_historico.cidade_id','=','cidades.id')
            ->orderBy('created_at','desc')
            ->select('users_cidade_historico.*','cidades.nome as cidadenome')
            ->get();


        return view('/back-end-admin/user-cidade-historico')
            ->withItens($historico)
            ->withUser(User::find($id));


    }

    public function getImportaLocalDoFacebook(){
        return view('/back-end-admin/importa-local-do-facebook')
            ->withCidades(Cidades::all());
    }

    public function postImportaLocalDoFacebook(Request $request){
        try {
            $rules = [
                'image' => 'required|image|max:300*300|dimensions:ratio=1/1',
                'entrega' => 'required',
                'cidade' => 'required',
                'site' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules, $this->messages);
            if ($validator->fails()) {
                return redirect('/admin/importa-local-do-facebook')->withErrors($validator)->withInput();
            } else {
                $foto_nome = str_random(30) . '-comercio-avatar.png';
                $request->file('image')->move('uploads', $foto_nome);

                Comercios::importaLocalDoFacebook($request, $foto_nome);
            }
        } catch (\Exception $e) {
//            dd($e);
            abort(500);
        }
        return redirect('/admin/home');
    }

    public function getDeleteComercio($id){
        if(Auth::id() == 3){

        }else{
            var_dump('não autorizado');
            return redirect('/');
        }
    }

    public function getUpdateCovers(){
        $comercios = Comercios::all();
        foreach($comercios as $comercio){
            Comercios::updateCover($comercio->id);
        }
        return Redirect::to(URL::previous());
    }
}