<?php

namespace CityTips\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $id = Auth::id();

        if ($id <> 4)
        {
            return redirect('/user/home');
        }


        return $next($request);
    }
}
