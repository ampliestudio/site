<?php

namespace CityTips\Http\Middleware;

use CityTips\Analytics;
use CityTips\Http\Requests\Request;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AnalyticsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//        dd($request);
        if (Auth::check()) {
            if (Auth::id() != 4) {
                $analise = new Analytics();
                $analise->request = $request;
                $analise->url = $request->url();
                $analise->user_id = Auth::id();
                $analise->save();
            }
        }else{
//            $analise = new Analytics();
//            $analise->request = $request;
//            $analise->url = $request->url();
//            $analise->save();
        }
        return $next($request);
    }
}
