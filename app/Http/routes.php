<?php

/*
 * user role 0 padrão 1 adm dos seus lugares
 */


use Carbon\Carbon;
use CityTips\Cidades;
use CityTips\Comercios;
use CityTips\ComerciosRate;
use CityTips\Conf;
use CityTips\Eventos;
use CityTips\EventoScore;
use CityTips\User;
use CityTips\UsersCidadeHistorico;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

Route::group(['middleware' => ['analytics']], function () {
//pagina inicial
    Route::get('/', function () {
        if (Auth::check()) {
            $cidade = Cidades::where('slug', Auth::user()->ultima_cidade)->first();
            if ($cidade != null) {
                return redirect('/' . $cidade->slug);
            }
            return view('welcome')->withConf(Conf::all())->withCidades(Cidades::orderBy('nome', 'asc')->get());
        }
        return view('welcome')->withConf(Conf::all())->withCidades(Cidades::orderBy('nome', 'asc')->get());
    });
});


Route::get('/open-mail', function () {
    return view('auth.emails.email-mkt')->withName('');
});

Route::get('/desativar-mail/{id}', function ($id) {
    $id = Crypt::decrypt($id);
    $user = User::find($id);
    $user->email_status = 0;
    $user->save();
    return view('suporte.desativa')->withEmail($user->email);
});

Route::get('/teste', function () {

//    Eventos::atualizarCidade(1);

//    Comercios::slideShowPosts(1);

    $now = \Carbon\Carbon::now();
    dd($now);

//    $user = User::find(4);
//    $ev = Eventos::algoritmoDeEventos(3);
//    $data = Eventos::queryArrayDeEventos($ev);
//    return view('auth.emails.semanario')->withData($data)->withUser($user);
});

//login admin
Route::get('/login-admin-84361522', function () {
    return view('auth/login-admin');
});


Route::get('eventos-antigos/{id}', function ($id) {
    $cidade = Cidades::where('slug', $id)->first();
    if (count($cidade) == 0) {
        abort(404);
    }
    $eventos = Eventos::where('eventos.cidade_id', $cidade->id)
        ->whereDate('inicio', '<', Carbon::today()->toDateString())
        ->orderBy('inicio', 'desc')
        ->where('eventos.status', 2)
        ->paginate(50);
    try {
        return view('sessoes/eventos-antigos')
            ->withCidade($cidade)
            ->withEventos($eventos)
            ->withCidades(Cidades::orderBy('nome', 'asc')->get());
    } catch (\Exception $e) {
        abort(404);
    }

});

Route::auth();

//controller para exibir uma unica pagina de um comércio
Route::resource('place', 'ComerciosController');

//controller para exibir uma unica pagina de um comércio
Route::resource('evento', 'EventoController');

//controlador de paginas internas sem login
Route::controller('/tips', 'SiteController');

//controlador da interface administrativa
Route::controller('/admin', 'AdminController');

//controlador da API gráfica
Route::controller('/graph', 'GraphController');

//controlador da página de lugares para as cidades
Route::resource('/places', 'PlacesController');

//controlador da interface de usuário
Route::controller('/user', 'UserController');

//login com facebook
Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('callback', 'Auth\AuthController@handleProviderCallback');

Route::get('cron3367842676947734789632bc7829bn1b1nc2', 'CronController@cronone');


//precisa logar
Route::group(['middleware' => ['analytics']], function () {

//ROTA PARA PAGINA PRINCIPAL DAS CIDADES -- retorna a cidade desejada com URL amigável
    Route::get('{cidade}', function ($cidade) {

//    Parte dos comércios

        try {
            $atual = Cidades::where('slug', $cidade)->first();
            //coloca a cidade como a ultima do usuário

            if (Auth::check()) {
                $user = User::find(Auth::id());
                $user->ultima_cidade = $atual->slug;
                $user->save();
                $user_cidade_historico = new UsersCidadeHistorico();
                $user_cidade_historico->user_id = Auth::id();
                $user_cidade_historico->cidade_id = $atual->id;
                $user_cidade_historico->save();
            }

            $comercios = Comercios::comerciosDaCidade($atual->id);

            $comercios_id_array = [];
//cria um array com os id s de comércios
            foreach ($comercios as $item) {
                $comercios_id_array[] = $item->id;
            }
//retorna as avaliações dos comercios que estão no array
            $rate = ComerciosRate::whereIn('comercio_id', $comercios_id_array)->where('recomendado', 1)->get();

//----------------fim da parte dos comércios--------------------


//algoritimo de ordenação dos eventos por relevância:
//eventos selecionados são postos na ordem neste array
            $eventos_id_array = Eventos::algoritmoDeEventos($atual->id);
            //seleciona os eventos apartir do array
//            dd($eventos_id_array);
            $eventos = Eventos::queryArrayDeEventos($eventos_id_array);

            //retorna as avaliações dos eventos que estão no array
            $score = EventoScore::whereIn('evento_id', $eventos_id_array)->where('recomendado', 1)->get();

//fim da parte dos eventos
            $carousel = Comercios::slideShowPosts($atual->id);

            return view('sessoes/cidade-home-eventos')
                ->withConf(Conf::all())
                ->withCidade($atual)
                ->withRate($rate)
                ->withScore($score)
                ->withEventos($eventos)
                ->withCidades(Cidades::orderBy('nome', 'asc')->get())
                ->withComercios($comercios)
                ->withCarousel($carousel)
                ->withLugares(Comercios::where('status', 2)->where('cidade_id', $atual->id)->get());
        } catch (Exception $e) {
//não existe a cidade digitada
//            dd($e);
            abort(404);
        }
    });

});

//retornar variaveis para a página de erro 404
View::composer('errors/404', function ($view) {
    $view->withCidades(Cidades::orderBy('nome', 'asc')->get());

});

//controlador padrão do laravel para páginas internas desativado
//Route::get('/home', 'HomeController@index');
// -----------inicio testes com facebook
//Route::get('/facebook/login', function(SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {
//    $login_link = $fb
//        ->getRedirectLoginHelper()
//        ->getLoginUrl('http://citytips.dev/facebook/callback', ['email', 'user_events']);
//
//    echo '<a href="' . $login_link . '">Log in with Facebook</a>';
//});
//Route::get('/facebook/callback', function(SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb) {
//    try {
//        $token = $fb
//            ->getRedirectLoginHelper()
//            ->getAccessToken();
//    } catch (Facebook\Exceptions\FacebookSDKException $e) {
//        // Failed to obtain access token
//        dd($e->getMessage());
//    }
//
//    try {
//        $response = $fb->get('/nylounge?fields=events{name,description}', $token);
//    } catch(\Facebook\Exceptions\FacebookSDKException $e) {
//        dd($e->getMessage());
//    }
//
//    $userNode = $response->getGraphUser();
//    dd($userNode);
//
//});
//fim testes com facebook -----------------
