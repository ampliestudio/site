<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class SlideShowPost
{
//    public $message;
    public $full_picture;
    public $nome;
    public $slug;
}

class Comercios extends Authenticatable
{

    protected $fillable = [
        'nome', 'endereco', 'entrega', 'horario', 'telefone',
        'status', 'cidade_id', 'slug', 'legal_info', 'user_id',
        'oficial', 'rate_count', 'lat', 'lon', 'site', 'tipo', 'import'
    ];

    protected $table = 'comercios';

    protected $hidden = [

    ];

    public static function comerciosDaCidade($cidade_id)
    {
        return Comercios::where('cidade_id', $cidade_id)
            ->where('status', 2)
            ->where('alias', 0)
            ->join('users', 'comercios.user_id', '=', 'users.id')
            ->select('comercios.*', 'users.name as user_name')
            ->orderBy('rate_count', 'desc')
            ->orderBy('nome', 'asc')
            ->get();
    }

    public static function criarAliasDeLocal($request)
    {
        $comercio = new Comercios();
        $nome = $request->local;
        $nome = str_replace('"', '\'\'', $nome);;
        $comercio->nome = $nome;
        $comercio->cidade_id = $request->cidade;
        $comercio->user_id = Auth::id();
        $comercio->alias = 1;
        $comercio->status = 2;
        $comercio->foto = "alias-image.png";
        $comercio->slug = $request->cidade . str_random(10) . Auth::id();
        $comercio->save();
        return $comercio;
    }

    public static function importaLocalDoFacebook($request, $foto_nome)
    {


        $json = file_get_contents("https://graph.facebook.com/v2.7/" . $request->site . "?fields=location%2Cphone%2Cplace_type%2Cname%2Cdescription%2Clink%2Cabout&access_token=" . Auth::user()->password);
        $objeto_evento = json_decode($json);

        $comercio = new Comercios();
        $comercio->nome = $objeto_evento->name;
        if (isset($objeto_evento->location->street)) {
            $comercio->endereco = $objeto_evento->location->street;
        }
        $comercio->entrega = $request->entrega;
        if (isset($objeto_evento->phone)) {
            $comercio->telefone = $objeto_evento->phone;
        }
        $comercio->cidade_id = $request->cidade;
        $comercio->site = '<a href="' . $objeto_evento->link . '" target="_blank">Facebook</a>';
        $comercio->user_id = Auth::id();
        $comercio->status = 1;
        $comercio->slug = urlencode($objeto_evento->name);
        $comercio->foto = $foto_nome;
        if (isset($objeto_evento->location->latitude)) {
            $comercio->lat = $objeto_evento->location->latitude;
            $comercio->lon = $objeto_evento->location->longitude;
        }
        $comercio->tipo = $request->tipo;
        $comercio->alias = 0;
        $comercio->import = 1;
        if (isset($objeto_evento->about)) {
            $comercio->descricao = $objeto_evento->about;
        }
        if (isset($objeto_evento->description)) {
            $comercio->descricao = $objeto_evento->description;
        }
        $comercio->save();


        $new = new FacebookPages();
        $new->comercio_id = $comercio->id;
        $new->link = $objeto_evento->id;
        $new->nome = urlencode($objeto_evento->name);
        $new->save();
    }

    public static function slideShowPosts($cidade)
    {

        $random_places = Comercios::where('cidade_id', $cidade)
            ->inRandomOrder()
            ->limit(11)
            ->get();
        $post_array = [];

        foreach ($random_places as $place) {


            $obj = new SlideShowPost();
            if ($place->capa != null) {
                $obj->full_picture = $place->capa;
                $obj->nome = $place->nome;
                $obj->slug = $place->slug;
            } else {
                continue;
            }


            $post_array[] = $obj;
        }
//        dd($post_array);
        return $post_array;
    }

    public static function retornaCapa($id)
    {
        $place = Comercios::find($id);

        if (isset($place->capa)) {
            return $place->capa;
        } else {
            return false;
        }

    }

    public static function updateCover($id)
    {
        $update = Comercios::find($id);

        $place = FacebookPages::where('comercio_id', $id)->first();
        if(isset($place) == null){
            return false;
        }
        $json = file_get_contents("https://graph.facebook.com/v2.7/" . $place->link . "?fields=cover&access_token=" . Auth::user()->password);
        $dados = json_decode($json);
        if (isset($dados->cover->source)) {
            $update->capa = $dados->cover->source;
        }
        $update->save();
        return true;
    }

}

