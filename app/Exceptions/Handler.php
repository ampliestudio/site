<?php

namespace CityTips\Exceptions;

use CityTips\Suporte;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

//        if($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
//            $a = new Suporte();
//            $a->info = '404';
//            $a->texto = $request.$e;
//            if(Auth::check()){
//                $a->nome = Auth::user()->name;
//                $a->email = Auth::user()->email;
//            }
//            $a->save();
//            return response()->view('errors.404');
//        }else{
//            $a = new Suporte();
//            $a->info = '500';
//            $a->texto = $request.$e;
//            if(Auth::check()){
//                $a->nome = Auth::user()->name;
//                $a->email = Auth::user()->eomail;
//            }
//            $a->save();
//            return response()->view('errors.500');
//        }

        return parent::render($request, $e);
    }

}
