<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UsersLoginLog extends Authenticatable
{

    protected $fillable = [
        'ip', 'user_id', 'html_request'
    ];

    protected $table = 'users_login_log';

    protected $hidden = [

    ];

//    public function loginLog($id){
//        //create log
//        $log = new UsersLoginLog();
//        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
//            $ip = $_SERVER['HTTP_CLIENT_IP'];
//        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
//            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
//        } else {
//            $ip = $_SERVER['REMOTE_ADDR'];
//        }
//        $log->ip = $ip;
//        $log->user_id = $id;
//        $log->save();
//    }
}
