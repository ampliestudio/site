<?php

namespace CityTips;

use Illuminate\Foundation\Auth\User as Authenticatable;

class FacebookPages extends Authenticatable
{

    protected $fillable = [
        'link', 'name', 'cidade_id','comercio_id'
    ];

    protected $table = 'facebook_pages';

    protected $hidden = [

    ];
}
