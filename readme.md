## CityTips Instalation
## Remover do config/services.php
`'facebook' => [`
        `'client_id' => env('FACEBOOK_APP_ID', null),`
        `'client_secret' => env('FACEBOOK_APP_SECRET', null),`
        `'redirect' => url('callback'),`
    `],`
## Instale as dependências do projeto
Instalação do framework

`composer install`

--
Instalação dos componentes auxiliares

`npm install bower gulp -g`

`npm install`

`bower install`

`gulp install`

## Configuração do framework
Gera uma chave de segurança para o projeto

`php artisan key:generate`

## Assets
Gera uma chave de segurança para o projeto

`gulp` ou `gulp watch`

## ENV Config (add this to .env)
FACEBOOK_APP_ID=1803108983236448
FACEBOOK_APP_SECRET=aa5e43c7d807c5152d2fb9e9414912f1

RECAPTCHA_PUBLIC_KEY=6Ler5SQTAAAAADFiHe5o9wxtrnPFj2zH97avqAGg
RECAPTCHA_PRIVATE_KEY=6Ler5SQTAAAAAESgmJrP_k1-TWUjD1dLIuJBDdb_

## Adicionar a config/services.php
`'facebook' => [`
        `'client_id' => env('FACEBOOK_APP_ID', null),`
        `'client_secret' => env('FACEBOOK_APP_SECRET', null),`
        `'redirect' => url('callback'),`
    `],`
# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
