/**
 * Created by Zafalon.com on 7/26/16.
 */
//script para recuperar cardáprio
function loadDoc(id) {
    /* Configura a requisição AJAX */
    $.ajax({
        url: '/tips/cardaprio', /* URL que será chamada */
        type: 'GET', /* Tipo da requisição */
        data: 'com_id=' + $('#com-id' + id).val(), /* dado que será enviado via GET */
        dataType: 'json', /* Tipo de transmissão */
        success: function (data) {
            if (data.sucesso == 1) {
                clearDiv(id);
                $('#value' + data.comercio).append(data.cardaprio);
            }
        }
    });
    return false;
}
function clearDiv(id) {
    $('#value' + id).html("");
}
//script para navegação GPS até o local
function myNavFunc(lat, lon) {
    // If it's an iPhone..
    if ((navigator.platform.indexOf("iPhone") != -1)
        || (navigator.platform.indexOf("iPod") != -1)
        || (navigator.platform.indexOf("iPad") != -1))
        window.open("maps://maps.google.com/maps?daddr=" + lat + "," + lon + "&amp;ll=");
    else
        window.open("http://maps.google.com/maps?daddr=" + lat + "," + lon + "&amp;ll=");
}


//tooltip
$(function () {
    $('[data-toggle="tooltip-lembrar"]').tooltip()
})
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})


//share
function myPopup(url) {
    window.open(url, "myWindow", "status = 1, height = 500, width = 360, resizable = 0")
}

//isotope
$(document).ready(function () {

// quick search regex
    var qsRegex;
    var buttonFilter;

// init Isotope
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        percentPosition: true,
        masonry: {
            columnWidth: '.grid-sizer'
        },
        filter: function() {
            var $this = $(this);
            var searchResult = qsRegex ? $this.text().match( qsRegex ) : true;
            var buttonResult = buttonFilter ? $this.is( buttonFilter ) : true;
            return searchResult && buttonResult;
        }
    });

    $('#filters').on( 'click', 'button', function() {
        buttonFilter = $( this ).attr('data-filter');
        $grid.isotope();
    });

// use value of search field to filter
    var $quicksearch = $('#quicksearch').keyup( debounce( function() {
        qsRegex = new RegExp( $quicksearch.val(), 'gi' );
        $grid.isotope();
    }) );


    // change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $( this ).addClass('is-checked');
        });
    });


// debounce so filtering doesn't happen every millisecond
    function debounce( fn, threshold ) {
        var timeout;
        return function debounced() {
            if ( timeout ) {
                clearTimeout( timeout );
            }
            function delayed() {
                fn();
                timeout = null;
            }
            setTimeout( delayed, threshold || 100 );
        };
    }



});

//up event
function upthis(id) {
    /* Configura a requisição AJAX */
    $.ajax({
        url: '/user/score-ajax', /* URL que será chamada */
        type: 'GET', /* Tipo da requisição */
        data: 'id=' + id, /* dado que será enviado via GET */
        dataType: 'json', /* Tipo de transmissão */
        success: function (data) {
            if (data.sucesso == 1) {
                if (data.up == 1) {
                    var badge = '#badge-' + id;
                    var count = parseInt($(badge).text(), 10);
                    $(badge).empty();
                    var valornovo = count + 1
                    $(badge).text(valornovo);

                    var star = '#star-' + id;
                    $(star).addClass('fa-chevron-down');
                    $(star).addClass('st-st-down');
                    $(star).removeClass('st-st-up');
                    $(star).removeClass('fa-chevron-up');

                    var marcar = '#marcar-' + id;
                    $(marcar).addClass('st-st-down');
                    $(marcar).removeClass('st-st-up');
                    //$(marcar).text('Marcado!');

                    var badge = '#badge-' + id;
                    $(badge).removeClass('badge-score');
                    $(badge).addClass('badge-score-up');


                } else {
                    var badge = '#badge-' + id;
                    var count = parseInt($(badge).text(), 10);
                    $(badge).empty();
                    var valornovo = count - 1
                    $(badge).text(valornovo);

                    var star = '#star-' + id;
                    $(star).removeClass('fa-chevron-down');
                    $(star).removeClass('st-st-down');
                    $(star).addClass('st-st-up');
                    $(star).addClass('fa-chevron-up');

                    var marcar = '#marcar-' + id;
                    $(marcar).removeClass('st-st-down');
                    $(marcar).addClass('st-st-up');

                    var badge = '#badge-' + id;
                    $(badge).removeClass('badge-score-up');
                    $(badge).addClass('badge-score');
                    //$(marcar).text('Marcar');
                }
            }
        }
    });
    return false;
}

//up place

function upplace(id) {
    /* Configura a requisição AJAX */
    $.ajax({
        url: '/user/rate-ajax', /* URL que será chamada */
        type: 'GET', /* Tipo da requisição */
        data: 'id=' + id, /* dado que será enviado via GET */
        dataType: 'json', /* Tipo de transmissão */
        success: function (data) {
            if (data.sucesso == 1) {
                if (data.up == 1) {

                    //alert('up')
                    var badge = '#badge-place-' + id;
                    var count = parseInt($(badge).text(), 10);
                    $(badge).empty();
                    var valornovo = count + 1
                    $(badge).text(valornovo);

                    var thumb = '#comercio-rate-thumb-' + id;
                    $(thumb).addClass('fa-thumbs-up');
                    $(thumb).addClass('up-up');
                    $(thumb).removeClass('fa-thumbs-up-o');
                    //
                    var recomenda = '#comercio-rate-text-' + id;
                    $(recomenda).addClass('up-up');
                    $(recomenda).text('Recomendado');


                } else {
                    //alert('down')
                    var badge = '#badge-place-' + id;
                    var count = parseInt($(badge).text(), 10);
                    $(badge).empty();
                    var valornovo = count - 1
                    $(badge).text(valornovo);

                    var thumb = '#comercio-rate-thumb-' + id;
                    $(thumb).removeClass('fa-thumbs-up');
                    $(thumb).removeClass('up-up');
                    $(thumb).addClass('fa-thumbs-up-o');

                    var recomenda = '#comercio-rate-text-' + id;
                    $(recomenda).removeClass('up-up');
                    $(recomenda).text('Recomendar');
                }
            }
        }
    });
    return false;
}

function upthiscomercio(id) {
    /* Configura a requisição AJAX */
    $.ajax({
        url: '/user/rate-ajax', /* URL que será chamada */
        type: 'GET', /* Tipo da requisição */
        data: 'id=' + id, /* dado que será enviado via GET */
        dataType: 'json', /* Tipo de transmissão */
        success: function (data) {
            if (data.sucesso == 1) {
                if (data.up == 1) {
                    var badge = '#badge-place-' + id;
                    var count = parseInt($(badge).text(), 10);
                    $(badge).empty();
                    var valornovo = count + 1
                    $(badge).text(valornovo);

                    var star = '#star-' + id;
                    $(star).addClass('fa-chevron-down');
                    $(star).addClass('st-st-down');
                    $(star).removeClass('st-st-up');
                    $(star).removeClass('fa-chevron-up');

                    var marcar = '#marcar-' + id;
                    $(marcar).addClass('st-st-down');
                    $(marcar).removeClass('st-st-up');
                    //$(marcar).text('Marcado!');

                    var badge = '#badge-place-' + id;
                    $(badge).removeClass('badge-score');
                    $(badge).addClass('badge-score-up');


                } else {
                    var badge = '#badge-place-' + id;
                    var count = parseInt($(badge).text(), 10);
                    $(badge).empty();
                    var valornovo = count - 1
                    $(badge).text(valornovo);

                    var star = '#star-' + id;
                    $(star).removeClass('fa-chevron-down');
                    $(star).removeClass('st-st-down');
                    $(star).addClass('st-st-up');
                    $(star).addClass('fa-chevron-up');

                    var marcar = '#marcar-' + id;
                    $(marcar).removeClass('st-st-down');
                    $(marcar).addClass('st-st-up');

                    var badge = '#badge-place-' + id;
                    $(badge).removeClass('badge-score-up');
                    $(badge).addClass('badge-score');
                    //$(marcar).text('Marcar');
                }
            }
        }
    });
    return false;
}

function filtrodetipo(tipo){
    if(tipo == 1){
        $('#filter-style').empty().append('<style>tr.tipo-2{display: none}tr.tipo-3{display: none}tr.tipo-4{display: none}tr.tipo-23{display: none}tr.tipo-24{display: none}tr.tipo-34{display: none}tr.tipo-234{display: none}</style>');
    }else if(tipo == 2){
        $('#filter-style').empty().append('<style>tr.tipo-1{display: none}tr.tipo-3{display: none}tr.tipo-4{display: none}tr.tipo-13{display: none}tr.tipo-14{display: none}tr.tipo-34{display: none}tr.tipo-134{display: none}</style>')
    }else if(tipo == 3){
        $('#filter-style').empty().append('<style>tr.tipo-2{display: none}tr.tipo-1{display: none}tr.tipo-4{display: none}tr.tipo-12{display: none}tr.tipo-24{display: none}tr.tipo-14{display: none}tr.tipo-124{display: none}</style>')
    }else if(tipo == 4){
        $('#filter-style').empty().append('<style>tr.tipo-2{display: none}tr.tipo-3{display: none}tr.tipo-1{display: none}tr.tipo-23{display: none}tr.tipo-12{display: none}tr.tipo-13{display: none}tr.tipo-123{display: none}</style>')
    }else if(tipo == 0){
        $('#filter-style').empty();
    }
}


$('#texto-falta').bind('focus', function () {
    $('#aparecer-focus').removeClass('sr-only');
});